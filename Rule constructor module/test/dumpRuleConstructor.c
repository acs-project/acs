/* 
 *
 *
 *
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "acse.h"

int ruleNum = 0;
int ruleFlag = 0; // 0 = no current rule, 1 = on current rule

void trimheading(char *string)
{
	int firstChar = 0;
	while(isspace(string[firstChar]))
		firstChar++;
	strcpy(&string[0],&string[firstChar]);
}
// return status
int parseRule (char *command)
{
	int retVal = 0;
	int typeIndex; 		// type num
	int subtypeIndex; 	// subt num
	int result;
	int loop = 0;
	char ruleName[64];
	char typeName[64];
	char subtypeName[64];
	//char attrName[16];
	//char val[64];
	char *attrName;
	char *val;
	RULE_T *rCurrent = NULL;

	memset(ruleName,0,sizeof(ruleName));
	memset(typeName,0,sizeof(typeName));
	memset(subtypeName,0,sizeof(subtypeName));
	trimheading(command);

	if (strncmp(command,"RULE",strlen("RULE")) == 0) // new rule
	{
		sscanf(command,"RULE:%[^\n]s",ruleName);
		if (strlen(ruleName) == 0)
		{
			printf("Error : Missing rule name.\n");
			retVal = 2; // 2 = missing rule name
		}
		else 
		{
			ruleNum++;
			rCurrent = (RULE_T*) calloc(1, sizeof(RULE_T));			// allocate new rule node 
			rCurrent->id = ruleNum;									// assign rule id 
			strcpy(rCurrent->name, ruleName);						// strcpy rule name
			insert(rCurrent);										// insert to the list
		}
	}
	else if (strncmp(command,"TYPE",strlen("TYPE")) == 0)
	{
		rCurrent = getCurrent();
		sscanf(command,"TYPE:%s",typeName);
		loop = 0;
		while(loop < strlen(typeName))
			toupper(typeName[loop++]);

		if(strcmp(typeName, "name") == 0)
		{
			rCurrent->type = NAME;
			NAME_T* tmp = (NAME_T*) calloc(1, sizeof(NAME_T));
			rCurrent->data = tmp;
		}
		else if (strcmp(typeName, "formatting") == 0)
		{
			rCurrent->type = FORMAT;
			FORMAT_T* tmp = (FORMAT_T*) calloc(1, sizeof(FORMAT_T));
			rCurrent->data = tmp;
		}
		else if (strcmp(typeName, "documentation") == 0)
		{
			rCurrent->type = DOCUMENT;
			DOCUMENT_T* tmp = (DOCUMENT_T*) calloc(1, sizeof(DOCUMENT_T));
			rCurrent->data = tmp;
		}
		else
		{
			printf("Error : Type \"%s\" not exist in the system");
			retVal = 3; // wrong type define.
		}
	}
	else if (strncmp(command,"SUBTYPE",strlen("SUBTYPE")) == 0)
	{
		rCurrent = getCurrent();
		sscanf(command,"SUBTYPE:%s",subtypeName);
		subtypeIndex = checkSubtype(rCurrent->type,subtypeName); // check subtype
		if (subtypeIndex > 0)
			rCurrent->subtype = subtypeIndex;
		else
			printf("Error Wrong subtype define.\n");		// wrong subtype define
	}
	else
	{
		rCurrent = getCurrent();
		//sscanf(command,"%s:%s",attrName,val);
		attrName = strtok(command,":");
		val = strtok(NULL,":");
		result = checkAttr(rCurrent->type,rCurrent->subtype,attrName,val);
		if (result > 0)
		{
			switch(rCurrent->type)
			{
				case NAME:
					//rCurrent->data = (NAME_T*) calloc(1, sizeof(NAME_T));
				case FORMAT:
		    		//rCurrent->data = (FORMAT_T*) calloc(1, sizeof(FORMAT_T));
				case DOCUMENT:
					//rCurrent->data = (DOCUMENT_T*) calloc(1, sizeof(DOCUMENT_T));
				default:
					printf("RULE: %d Attrname : %10s Val : %s\n", rCurrent->id, attrName, val);
			}
		}
	}
	return retVal;
}



/*
 * return 0: succesfully read file.
 *        1: open file error.
 *        2: command error.
 */
int readFile (char *filename)
{
	FILE *fp = NULL;
	int retVal = 0;
	char input[128];
	char command[128];

	fp = fopen(filename, "r");
	if (fp == NULL)
	{
		printf("Error: Cannot open file \"%s\".\n", filename);
		fclose(fp);
		retVal = 1; // 1 = open file error
	}
	else
	{
		while(fgets(input,sizeof(input),fp) != NULL)
		{
			sscanf(input,"%[^\n]s",command);
			// skip comment
			if ((strncmp(command,"/",strlen("/")) != 0) && (strncmp(command,"*",strlen("*")) != 0))
			{
				retVal = parseRule(command);
				if (retVal != 0)
					break;
			}
			
		}
	}

	return retVal;
}