#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "acse.h"

char *codes[MAXLINE];

char* mystrdup(const char* data)
{
    char* d=malloc(strlen(data)+1);
    if(d==NULL)
        return NULL;
    strcpy(d,data);
    return d;
}

int isdigits(char value[])
{
	int i = 0;
	int retVal = 1;

	for (i = 0; i < strlen(value); i++) 
	{
		if(!isdigit(value[i]) && (value[i] != '\0'))
		{
			//printf("Error index %d\n", i);
			retVal = -1;
		}
	}
	return retVal;
}

int initCodeList(FILE *fileinput)
{
	char inputBuffer[5000];
	int i = 0;
	int retVal = 1;
	char lineData[5000];

	while(fgets(inputBuffer,sizeof(inputBuffer),fileinput) != NULL)
	{
		sscanf(inputBuffer,"%[^\n]s",lineData);
		if(i < MAXLINE)
		{
		    codes[i] = mystrdup(lineData);
		    i++;
		}
		else
		{
		    printf("The input file's size is too large\n");
		    retVal = 0;
		}
	}
	rewind(fileinput);

	return retVal;
}

char* getCodeLine(int line)
{
	if(line < MAXLINE)
		return codes[line];
	else
		return NULL;
}

int checkFileType(char *filename, char *fileType)
{
	strtok(mystrdup(filename), "."); 					// flush filename
	char *inputFT = strtok(NULL, ".");
	if(inputFT != NULL)
	{
		if(strcmp(fileType,inputFT) == 0)
			return 1;
	}
	return 0;
}

void checkReturn(int returnVal, int bReturn)
{
	if(returnVal != bReturn)
		exit(-1);
}

int isMarks(int type)
{
	int i = 0;
	int marks[] = {SLASH,B_BSLASH,SEMICOMM,COMM,COL,FULL_STOP,Q_MARK,HASH,
		D_QUOTE,ARITH_OP_1,ARITH_OP_2,REL_OP,REL_OP_1,REL_OP_2,LOGIC_OP,
		BIT_OP,ASS_OP,OP_PAR_BRACKET,ED_PAR_BRACKET,OP_SQ_BRACKET,
		ED_SQ_BRACKET,OP_CUR_BRACKET,ED_CUR_BRACKET,OP_ANG_BRACKET,
		ED_ANG_BRACKET};

	for (i = 0; i < sizeof(marks)/sizeof(int); i++)
	{
		if(type == marks[i])
			return 1;
	}
	return 0;
}

int isStatement(int type, int incFuncDef)
{
	int i = 0;
	int stmt[] = {DEFINE_STMT,INCLUDE_STMT,FUNC_DEF_STMT,DECL_STMT,STA_ASS_DECL,
				  FOR_STMT,WHILE_STMT,DO_WHILE_STMT,SWITCH_STMT,IF_STMT,LABEL_STMT,
				  COMP_STMT,JUMP_STMT,EXPR_STMT,MEM_ACS_EXPR,FUNC_CALL_EXPR,
				  TERN_COND_EXPR,ARTH_EXPR,LOG_EXPR,COMP_EXPR,INC_DEC_EXPR,ASS_EXPR,
				  BIT_EXPR,PRI_EXPR,CAST_EXPR,UNARY_EXPR};
	
	for(i = 0; i < sizeof(stmt)/sizeof(int); i++)
	{
		if(type == stmt[i])
		{
			if(stmt[i] == FUNC_DEF_STMT)
			{
				if(incFuncDef)
					return 1;
				else
					return 0;
			}
			return 1;
		}
	}
	return 0;
}

int isSpace(int type)
{
	int i = 0;
	int space[] = {SPACE,TAB,NEWLINE};

	for (i = 0; i < sizeof(space)/sizeof(int); i++)
	{
		if(type == space[i])
			return 1;
	}
	return 0;
}

char *getFilename(char *path)
{
	char *pch;
	char *retString = mystrdup(path);

	if((pch = strchr(path, '/')) != NULL)
		strtok(path, "/");

	while(pch != NULL)
	{
		free(retString);
		retString = strtok(NULL, "/");
		pch = strchr(pch + 1, '/');
	}
	return retString;
}