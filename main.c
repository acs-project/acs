#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include "acse.h"

int debug = 0;

int main(int argc, char *argv[])
{
	FILE *codeInput = NULL;
	int retVal = 0;
	char datetime[100];
	time_t now = time(NULL);
	struct tm *today = localtime(&now);
	clock_t t;

	t = clock();
	switch(argc)
	{
		case 2:
			if(strcmp(argv[1],"-help") == 0)
			{
				printf("\nUsage:\n");
				printf("  acse [options] file...\n");
				printf("  acse <C source file> <rule file> <report file>\n");
				//printf("  ruleConstructor [rule filename]       Disable log, Use user's rule filename\n");
				printf("\nOptions :\n");
				printf("  -help                                 Display this information\n");
				//printf("  -log [rule filename] [log filename]   Enable log, optional use user's rule\n");
				//printf("                                        and log filename\n");
				printf("\nNote : [...] means optional\n");
				printf("Information Date : 20/4/2017\n");
				return 0;
			}
			break;
		case 4:
			if (checkFileType(argv[1], "c") && checkFileType(argv[2], "csdl") && ((codeInput = fopen(argv[1],"r")) != NULL))
			{
				// parser and tokenizer
				checkReturn(initCodeList(codeInput), 1);
				codeStackClear();
    			flagStackClear();
    			checkReturn(structListInit(), 1);
				checkReturn(typedefListInit(), 1);
				retVal = parse(codeInput);
			    if(retVal) // error occur
			    {
			        printf("Syntax error line %d\n",getLine());
			        printf("\t%s\n",getCodeLine(getLine()-1));
			        return -1;
			    }
			    //printStructList();
    			//printTypedefList();
			    pack();
			    //printProgram(getStackHeadData());
			    //printf("\n\n");

			    // rule constructor
			    ruleListInit();
				checkReturn(readFile(argv[2]), 0);
				//testPrint();

				// report generator
				checkReturn(reportListInit(argv[3]),0);

				// enforcement perform
				enforcement();

				//testPrintReport();
			}
			break;
		case 5:
			printf("NOT SUPPORT YET\n");
			break;
		default:
			printf("Error : Expect 2-5 arguments, found %d\n",argc-1);
			break;
	}
	
    t = clock() - t;
    strftime(datetime, sizeof(datetime)-1, "%d-%m-%Y %H:%M", today);
    printf("Finish in %f seconds (%d clicks)\n", ((float)t)/CLOCKS_PER_SEC, t);
    printf("Date: %s\n", datetime);
    printHeader(getFilename(argv[1]), ((float)t)/CLOCKS_PER_SEC, datetime);

    fclose(codeInput);
	codeStackClear();
    flagStackClear();
    structlistDestroy();
    typedeflistDestroy();
    ruleListDestroy();
    reportFinish();

	return 0;
}