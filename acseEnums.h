/**************************************************************
 * Enumeration in rule constructor module.
 */
typedef enum
{
	NAME,
	FORMAT,
	DOCUMENT
} TYPE;

typedef enum 
{
	ST_ALLVAR,
	ST_GLOBAL,
	ST_LOCAL,
	ST_STATIC,
	ST_EXTERN,
	ST_CONSTANT,
	ST_DEFINEDCONST,
	ST_ENUM,
	ST_FUNCN,
	ST_STRUCT,
	ST_TYPEDEF,
	ST_BRACEPOS,				//
	ST_IFELSE,
	ST_SWITCH,
	ST_FUNCF,
	ST_VARDECLAREPOS,
	ST_OPSPACE,
	ST_BLOCKCOMM,				//
	ST_SINGLELINECOMM,
	ST_TRAILCOMM
} SUB_TYPE;

typedef enum
{
	MAX_LENGTH,
	MIN_LENGTH,
	EXCEPTIONS,
	STYLE,
	PRERFIX,
	SUFFIX,
	REFERENCE,
	POSITION,
	MAX_LINE,
	ENABLE
} ATTR;

typedef enum
{
	UNDEFINED,
	INTEGER,
	STRING,
	STRINGLIST
} ATTR_VALUE;

/**************************************************************
 * Enumeration in parser and tokenizer module.
 */

typedef enum 
{
  define_declaration=1000, define_constant, include, primary_expression,
  constant, enumeration_constant, string, generic_selection,
  generic_assoc_list, generic_association, postfix_expression,
  argument_expression_list, unary_expression, unary_operator,
  cast_expression, multiplicative_expression, additive_expression,
  shift_expression, relational_expression, equality_expression,
  and_expression, exclusive_or_expression, inclusive_or_expression,
  logical_and_expression, logical_or_expression,
  conditional_expression, assignment_expression, assignment_operator,
  expression, constant_expression, declaration,
  declaration_specifiers, init_declarator_list, init_declarator,
  storage_class_specifier, type_specifier, typedef_specifier,
  struct_or_union_specifier, struct_or_union,
  struct_declaration_list, struct_declaration,
  specifier_qualifier_list, struct_declarator_list,
  struct_declarator, enum_specifier, enumerator_list, enumerator,
  atomic_type_specifier, type_qualifier, function_specifier,
  alignment_specifier, declarator, direct_declarator, pointer,
  type_qualifier_list, parameter_type_list, parameter_list,
  parameter_declaration, identifier_list, type_name,
  abstract_declarator, direct_abstract_declarator, initializer,
  initializer_list, designation, designator_list, designator,
  static_assert_declaration, statement, labeled_statement,
  compound_statement, block_item_list, block_item,
  expression_statement, selection_statement, iteration_statement,
  jump_statement, translation_unit, external_declaration,
  function_definition, declaration_list
} YACC_GRAMMAR;

typedef enum
{
	SLASH=974,		/* \ */
	B_BSLASH,  		// //
	SEMICOMM,   	// ;
	COMM,       	// ,
	COL,			// :
	FULL_STOP,		// .
	Q_MARK,			// ?
	HASH,			// #
	D_QUOTE			// "
}PUNC_MARKS;

typedef enum
{
	PROGRAM=935,
	B_COMMENT,			// Block comment
	SL_COMMENT,			// Single-line comment
	DESIGNATOR,
	ENUMERATOR,
	GENERIC_ASSOC,
	INITIALIZER,
	PARAMETER,
	ALIGNMENT,
	DEFINE_STMT,
	INCLUDE_STMT,
	DECL_STMT,
	STA_ASS_DECL, 		// Static assert declaration
	FUNC_DEF_STMT,		// 948
	FOR_STMT,
	WHILE_STMT,
	DO_WHILE_STMT,
	SWITCH_STMT,
	IF_STMT,
	LABEL_STMT,			// Labeled statement
	COMP_STMT,			// Compound statement
	JUMP_STMT,			// Jump statement i.e. RETURN, BREAK
	EXPR_STMT,			// Expression statement
	FUNC_PROTO,			// Function prototype
	MEM_ACS_EXPR,		// Member-Access
	FUNC_CALL_EXPR,		// Function-call Expression
	TERN_COND_EXPR,		// Ternary condition expression
	ARTH_EXPR,			// Arithematic Expression
	LOG_EXPR,			// Logical Expression
	COMP_EXPR,			// Comparison Expression
	INC_DEC_EXPR,		// Increment Decrement Expression
	ASS_EXPR,			// Assignment statement
	BIT_EXPR,			// Bit expression
	PRI_EXPR,			// Primary expression
	CAST_EXPR,			// Cast expression
	UNARY_EXPR,
	TYPE_SPEC,			// Type specifier
	TYPE_QUAL,   		// Type qualifier
	INIT_DECL=973
}BLOCKS;

typedef enum 
{
	ARITH_OP_1 = 994,	// + - * /
	ARITH_OP_2,			// ++ --
	REL_OP,				// == != <= >=
	REL_OP_1 = 992,		// <
	REL_OP_2 = 993,		// >
	LOGIC_OP = 997,		// && || !
	BIT_OP,				// & | ^ ~ << >>
	ASS_OP				// = += -= *= /= %= <<= >>= &= ^= |=
}OPERATORS;

typedef enum 
{
	OP_PAR_BRACKET = 986,
	ED_PAR_BRACKET,
	OP_SQ_BRACKET,
	ED_SQ_BRACKET,
	OP_CUR_BRACKET,
	ED_CUR_BRACKET,
	OP_ANG_BRACKET = 992,
	ED_ANG_BRACKET = 993
}BRACKETS;

typedef enum 
{
	SPACE=983,
	TAB,
	NEWLINE
}WHITESPACES;