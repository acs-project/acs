#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "acse.h"

int isdigits(char value[])
{
	int i = 0;
	int retVal = 1;

	for (i = 0; i < strlen(value); i++) 
	{
		if(!isdigit(value[i]) && (value[i] != '\0'))
		{
			//printf("Error index %d\n", i);
			retVal = -1;
		}
	}
	return retVal;
}

int checkVar(char var[], int mode)
{
	int i = 0;
	char *reservedWord[] = {"auto", "default", "float", "register", "struct", "volatile",
	"break", "do", "far", "return", "switch", "while", "case", "double", "goto", "short",
	"typedef", "char", "else", "if", "signed", "union", "const", "enum", "int", "sizeof",
	"unsigned", "continue", "extern", "long", "static", "void"};
	int count = sizeof(reservedWord)/sizeof(char*);
	char dumpvar[strlen(var)];
	
	if(mode > 0)
	{
		// check first char
		strcpy(dumpvar, var);
		if ((isalpha(dumpvar[0]) == 0) && (dumpvar[0] != '_'))
			return -1;
	
		//printf("check 2\n");
		// check space and nonalphanum in variable
		for (i = 1; i < strlen(var); i++)
		{
			if ((isalnum(dumpvar[i]) == 0) && (dumpvar[i] != '_'))
				return -1;
		}
	
		if(mode > 1)
		{
			// check reserved word
			for(i = 0; i < count; i++)
			{
				if(strcmp(reservedWord[i], var) == 0)
					return -1;
			}
		}
	}
	
	return 0;
}

VALUE_T maxLengthValidate(int subtype, int valType, char value[])
{
	VALUE_T retVal = {UNDEFINED};

	retVal.type = isdigits(value);
	if(retVal.type != -1) 
	{
		retVal.type = valType;
		retVal.intVal[0] = atoi(value);
	}
	
	return retVal;
}

VALUE_T minLengthValidate(int subtype, int valType, char value[])
{
	VALUE_T retVal = {UNDEFINED};

	retVal.type = isdigits(value);
	if(retVal.type != -1) 
	{
		retVal.type = valType;
		retVal.intVal[0] = atoi(value);
	}
	
	return retVal;
}

VALUE_T exceptionsValidate(int subtype, int valType, char value[])
{
	int i = 0;
	int maxCount = 10;
	char *varName;
	VALUE_T retVal = {UNDEFINED};

	//printf("\"%s\"\n", value);
	if((value[0] == '[') && (value[strlen(value)-1] == ']'))
	{
		retVal.type = valType;
		value[strlen(value)-1] = '\0';
		for(i = 0; i < 10; i++)
		{
			if(i == 0)
				varName = strtok(value, "[,");
			else
				varName = strtok(NULL, ",");

			if(varName == NULL)
				break;
			else
			{
				if(checkVar(varName, 2) == 0)
					strcpy(retVal.stringList[i], varName);
				else
				{
					//printf("Invalid Name : %s\n", varName);
					retVal.type = -2; // Invalid variable name
					break;
				}
			}
		}
		retVal.intVal[0] = i;
	}
	else
		retVal.type = -1;  // Invalid 

	return retVal;
}

VALUE_T styleValidate(int subtype, int valType, char value[])
{
	int i = 0;
	char *list[] = {"camel", "pascal", "snake", "macro"};
	VALUE_T retVal = {UNDEFINED};
	
	while(i < strlen(value))
		tolower(value[i++]);

	for(i = 0; i < sizeof(list)/sizeof(char*); i++)
	{
		
		if(strcmp(list[i], value) == 0)
		{
			retVal.type = valType;
			retVal.intVal[0] = i;
		}
	}
	return retVal;
}

const int MAXPREFIX = 5;
const int MAXSUFFIX = 5;

VALUE_T prefixValidate(int subtype, int valType, char value[])
{
	VALUE_T retVal = {UNDEFINED};

	if((checkVar(value, 1) == 0) && (strlen(value) <= MAXPREFIX))
	{
		retVal.type = valType;
		strcpy(retVal.string, value);
	}
	
	return retVal;
}

VALUE_T suffixValidate(int subtype, int valType, char value[])
{
	int i = 0;
	VALUE_T retVal = {UNDEFINED};

	for (i = 0; i < strlen(value); i++)
	{
		if ((isalnum(value[i]) == 0) && (value[i] != '_'))
			retVal.type = -1;
	}

	if((retVal.type != -1) && (strlen(value) <= MAXSUFFIX))
	{
		retVal.type = valType;
		strcpy(retVal.string, value);
	}
	
	return retVal;
}

VALUE_T referenceValidate(int subtype, int valType, char value[])
{
	VALUE_T retVal = {UNDEFINED};
	int i = 0;
	char *refList[] = {"all-variable", "global", "enum", "structure"};
	int refVal[] = {ALLVAR, GLOBAL, ENUM, STRUCT};

	if(subtype == TYPEDEF)
	{
		for(i = 0; i < sizeof(refList)/sizeof(char*); i++)
		{
			if(strcmp(refList[i], value) == 0)
			{
				retVal.type = valType;
				retVal.intVal[0] = refVal[i];
			}

		}
	}

	return retVal;
}

VALUE_T positionValidate(int subtype, int valType, char value[])
{
	int i = 0;
	int index = 0;
	int list[6][3] = {{BRACEPOS, IFELSE, TRAILCOMM}, {BRACEPOS, IFELSE, SWITCH}, {BRACEPOS, SWITCH}, 
					{BLOCKCOMM}, {BLOCKCOMM}, {SINGLELINECOMM}};
	char *posVal[] = {"inline", "below", "below-one-step", "above-function", "header", "above-statement"};
	VALUE_T retVal = {UNDEFINED};
	
	while(i < strlen(value))
		tolower(value[i++]);

	for (index = 0; index < sizeof(posVal)/sizeof(char*); index++)
	{
		if(strcmp(posVal[index], value) == 0)
		{
			for (i = 0; i < sizeof(list[index])/sizeof(int); i++)
			{
				if(subtype == list[index][i])
				{
					retVal.intVal[0] = index;
					retVal.type = valType;
				}
			}
					
		}
	}
	return retVal;
}

VALUE_T maxLineValidate(int subtype, int valType, char value[])
{
	VALUE_T retVal = {UNDEFINED};

	retVal.type = isdigits(value);
	if(retVal.type != -1) 
	{
		retVal.type = valType;
		retVal.intVal[0] = atoi(value);
	}

	return retVal;
}

VALUE_T enableValidate(int subtype, int valType, char value[])
{
	VALUE_T retVal = {UNDEFINED};
	int i = 0, j = 0;
	int subtypeList[] = {VARDECLAREPOS, OPSPACE};
	char *validValue[] = {"false", "true"};

	if(!isdigits(value))
	{
		while(i < strlen(value))
			tolower(value[i++]);

		for (i = 0; i < sizeof(validValue)/sizeof(char*); i++)
		{
			if(strcmp(validValue[i], value) == 0)
			{
				for (j = 0; i < sizeof(subtypeList)/sizeof(int); i++)
				{
					if(subtype == subtypeList[j])
					{
						retVal.type = valType;
						retVal.intVal[0] = i;
					}
				}
			}
		}
	}
	
	return retVal;
}
