/* 
 *
 *
 *
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "acse.h"

int indentSpace = 4;
int ruleNum = 0;
int ruleFlag = 0; // 0 = no current rule, 1 = on current rule
int lineNum = 0;

void trimString(char *string)
{
	int lastChar = 0;
	char *tmpString;

	if(isspace(string[0]))
	{
		tmpString = strtok(string, " \t");
		strcpy(string, tmpString);
	}

	/*while(((isalnum(string[lastChar])) || (isalnum(string[lastChar+1])) || 
		(string[lastChar] == ':') || (string[lastChar] == ']') || 
		(string[lastChar+1] == '_')) && 
		((int)string[lastChar] != 0))*/
	while(isprint(string[lastChar]))
		lastChar++;

	string[lastChar] = '\0';
}

// return status
int parseRule (char *command)
{
	int retVal = 0;
	int typeIndex; 		// type num
	int subtypeIndex; 	// subt num
	int attrIndex;
	int loop = 0;
	char ruleName[64];
	char typeName[64];
	char subtypeName[64];
	char *attrName;
	char *val;
	RULE_T *rCurrent = NULL;
	VALUE_T result = {0};

	trimString(command);
	//printf("CMD : %s|\n", command);

	if (strncmp(command,"INDENT_SPACE",strlen("INDENT_SPACE")) == 0)
	{
		char *tmpString = NULL;
		strtok(command, ": ");
		if((tmpString = strtok(NULL, ": ")) != NULL)
		{
			if(isdigits(tmpString))
				indentSpace = atoi(tmpString);
		}
	}
	else if (strncmp(command,"RULE",strlen("RULE")) == 0) // new rule
	{
		memset(ruleName,0,sizeof(ruleName));
		memset(typeName,0,sizeof(typeName));
		memset(subtypeName,0,sizeof(subtypeName));

		sscanf(command,"RULE:%[^\n]s",ruleName);
		if (strlen(ruleName) == 0)
		{
			printf("Error : Missing rule #%d name.\n", ruleName+1);
			retVal = 2; // 2 = missing rule name
		}
		else 
		{
			ruleNum++;
			rCurrent = (RULE_T*) calloc(1, sizeof(RULE_T));			// allocate new rule node 
			rCurrent->id = ruleNum;									// assign rule id 
			strcpy(rCurrent->name, ruleName);						// strcpy rule name
			insertRule(rCurrent);									// insert to the list
		}
	}
	else if (strncmp(command,"TYPE",strlen("TYPE")) == 0)
	{
		rCurrent = getRuleCurrent();
		sscanf(command,"TYPE:%s",typeName);
		loop = 0;
		while(loop < strlen(typeName))
			toupper(typeName[loop++]);

		if(strcmp(typeName, "name") == 0)
			rCurrent->type = NAME;
		else if (strcmp(typeName, "formatting") == 0)
			rCurrent->type = FORMAT;
		else if (strcmp(typeName, "documentation") == 0)
			rCurrent->type = DOCUMENT;
		else
		{
			printf("Error : Line#%d Type \"%s\" not exist in the system", lineNum, typeName);
			retVal = 3; // wrong type define.
		}
	}
	else if (strncmp(command,"SUBTYPE",strlen("SUBTYPE")) == 0)
	{
		rCurrent = getRuleCurrent();
		sscanf(command,"SUBTYPE:%s",subtypeName);
		subtypeIndex = checkSubtype(rCurrent->type,subtypeName); // check subtype
		if (subtypeIndex >= 0)
			rCurrent->subtype = subtypeIndex;
		else
		{
			printf("Error: Line#%d Wrong subtype define [%s].\n", lineNum, subtypeName);		
			retVal = 4; // wrong subtype define
		}
	}
	else
	{
		rCurrent = getRuleCurrent();
		//sscanf(command,"%s:%s",attrName,val);
		attrName = strtok(command,":");
		val = strtok(NULL,":");
		attrIndex = checkAttr(rCurrent->subtype,attrName);
		if (attrIndex < 0)
		{
			if (attrIndex == -1)
				printf("Error: Line#%d Attribute \"%s\" not exist in the system.\n", lineNum, attrName);
			else
				printf("Error: Line#%d Attribute \"%s\" not exist in subtype \"%s\".\n", lineNum, attrName, subtypeName);
			retVal = 5; // wrong attrbute define
		}
		else
		{
			//printf("%s %s\n", attrName, val);
			result = checkValue(rCurrent->subtype, attrIndex, val);			
			if (result.type < 0)
			{
				printf("Error: Line#%d Invalid attribute value defined in %s.\n", lineNum, attrName);
				retVal = 6; // wrong attribute value defined
			}
			else
			{
				ATTR_T *attr = (ATTR_T*)calloc(1,sizeof(ATTR_T));
				*attr = getAttr(attrIndex);
				attr->value = result;
				if(attr != NULL)
				{
					if (rCurrent->attrHead == NULL)
						rCurrent->attrHead = attr;
					else
						rCurrent->attrTail->next = attr;
					rCurrent->attrTail = attr;
				}
			}
		}
		//printf("attr name:%s\n", rCurrent->attrList->name);
	}
	return retVal;
}



/*
 * return 0: succesfully read file.
 *        1: open file error.
 *        2: command error.
 */
int readFile (char *filename)
{
	FILE *fp = NULL;
	int retVal = 0;
	char input[128];
	char command[128];
	fp = fopen(filename, "r");
	if (fp == NULL)
	{
		printf("Error: Cannot open file \"%s\".\n", filename);
		fclose(fp);
		retVal = 1; // 1 = open file error
	}
	else
	{
		while(fgets(input,sizeof(input),fp) != NULL)
		{
			sscanf(input,"%[^\n]s",command);
			lineNum++;
			// skip comment
			if ((strchr(command,'/') != 0) && (strchr(command,'*') != 0))
			{
				do {
					if(fgets(input,sizeof(input),fp) != NULL){
						sscanf(input,"%[^\n]s",command);
						lineNum++;
					}
					else
						return 0;
				} while((strchr(command,'/') == 0) && (strchr(command,'*') == 0));

				// skip end block comment
				if(fgets(input,sizeof(input),fp) != NULL){
					sscanf(input,"%[^\n]s",command);
					lineNum++;
				}
				else
					return 0;
			}

			//(strncmp(command,"/",strlen("/")) != 0) && (strncmp(command,"*",strlen("*")) != 0)

			if (strncmp(command,"//",strlen("//")) != 0)
			{
				retVal = parseRule(command);
				if (retVal != 0)
				{
					retVal = 2;
					break;
				}
			}
			
		}
	}

	//printf("Return Value : %d\n", retVal);
	return retVal;
}