/*
 *
 *
 *
 */
#define ATTR_COUNT 10


// ENUMERATOR SECTION
typedef enum
{
	NAME,
	FORMAT,
	DOCUMENT
} TYPE;

typedef enum 
{
	ALLVAR,
	GLOBAL,
	LOCAL,
	STATIC,
	EXTERN,
	CONSTANT,
	DEFINEDCONST,
	ENUM,
	FUNCN,
	STRUCT,
	TYPEDEF,
	BRACEPOS,				//
	IFELSE,
	SWITCH,
	FUNCF,
	VARDECLAREPOS,
	OPSPACE,
	BLOCKCOMM,				//
	SINGLELINECOMM,
	TRAILCOMM
} SUB_TYPE;

typedef enum
{
	MAX_LENGTH,
	MIN_LENGTH,
	EXCEPTIONS,
	STYLE,
	PRERFIX,
	SUFFIX,
	REFERENCE,
	POSITION,
	MAX_LINE,
	ENABLE
} ATTR;

typedef enum
{
	UNDEFINED,
	INTEGER,
	STRING,
	STRINGLIST
} ATTR_VALUE;

// STRUCTURE SECTION
typedef struct _code
{
	int type;						/* type of node(token) or line/block */
	char *value;					/* token value/func name/line type */
	int fLine;						/* first line / for each line and token this is its line */
	int lLine;						/* last line */
	int count;						/* line count*/
	struct _code *parent;			/* pointer to parent*/
	struct _code *firstChild;		/* pointer to first child*/
	struct _code *lastChild;		/* pointer to last child*/
	struct _code *next;				/* pointer to next tolen/line/block */
} CODE_T;

typedef struct
{
	int type;						// attr value type
	int intVal[3];					// attr integer value
	char string[5];					// attr string value -> prefix, suffix
	char stringList[10][64];		// attr string list -> exceptions
} VALUE_T;

typedef struct _attr
{
	char *name;											// attr name
	VALUE_T value;										// attr value
	VALUE_T (*validate)(int subtype, int valType, char value[]);		// attr's value validate function
	int (*check)(CODE_T *code, VALUE_T checkValue);		// attr's enforcement function
	struct _attr *next;
} ATTR_T;

typedef struct _rule
{
	int id;												// rule's id
	char name[64];										// rule's name
	int type;											// rule's type
	int subtype;										// rule's subtype
	ATTR_T *attrHead;									// rule's attribute list head
	ATTR_T *attrTail;									// rule's attribute list tail
	struct _rule *next; 								// ptr to next rule in the rule list
} RULE_T;

typedef struct
{
	int type;
	char *subtype;
	int attrList[ATTR_COUNT];
} CSDL_LIST_T;

typedef struct
{
	RULE_T *rHead;
	RULE_T *rTail;
	RULE_T *rCurrent;
} RULE_LIST_T;

// public functions section

/*
 * ruleController.c
 */
int ruleListInit();

void ruleListDestroy();

int insertRule(RULE_T *newRule);

RULE_T *getRuleHead();

RULE_T *getRuleCurrent();

int checkSubtype(int type, char* subtype);

int checkAttr(int subtype, char* attrName);

int* getEnforcementFunction(int attrIndex);

ATTR_T getAttr(int attrIndex);

VALUE_T checkValue(int subtype, int attrIndex, char* checkVal);

//int insertAttr(ATTR_T* aHead, ATTR_T* aTail, ATTR_T* aNew);

VALUE_T testPrint();
/*
 * ruleReader.c
 */
int readFile (char *filename);

/*
 * validateFunction.c
 */
VALUE_T maxLengthValidate(int subtype, int valType, char* checkVal);
VALUE_T minLengthValidate(int subtype, int valType, char* checkVal);
VALUE_T exceptionsValidate(int subtype, int valType, char* checkVal);
VALUE_T styleValidate(int subtype, int valType, char* checkVal);
VALUE_T prefixValidate(int subtype, int valType, char* checkVal);
VALUE_T suffixValidate(int subtype, int valType, char* checkVal);
VALUE_T referenceValidate(int subtype, int valType, char* checkVal);
VALUE_T positionValidate(int subtype, int valType, char* checkVal);
VALUE_T maxLineValidate(int subtype, int valType, char* checkVal);
VALUE_T enableValidate(int subtype, int valType, char* checkVal);
int isdigits(char value[]);

/*
 * enforcementFunction.c
 */
int maxLength(CODE_T *code, VALUE_T checkValue);
int minLength(CODE_T *code, VALUE_T checkValue);
int exceptions(CODE_T *code, VALUE_T checkValue);
int style(CODE_T *code, VALUE_T checkValue);
int prefix(CODE_T *code, VALUE_T checkValue);
int suffix(CODE_T *code, VALUE_T checkValue);
int reference(CODE_T *code, VALUE_T checkValue);
int position(CODE_T *code, VALUE_T checkValue);
int maxLine(CODE_T *code, VALUE_T checkValue);
int enable(CODE_T *code, VALUE_T checkValue);