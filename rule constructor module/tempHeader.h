typedef struct _attr
{
	char *name;						// attr name
	void *value;					// attr value
	int (*validate)(char *attr);	// attr validate function
	int (*check)(CODE_T *data);		// attr enforcement function
	struct _attr *next;				// next attr in the list (all attr list) use for search
}ATTR_T;

typedef struct _subtype
{
	char *name;						// subtype name
	ATTR_T *attrList;				// list of attr belongs to that subtype
	struct _subtype *next;			// next subtype in the list (all subtype list) use for search
}SUBTYPE_T;

typedef struct _rulex
{
	int id;							// rule id
	char name[64];					// rule name
	int type;						// rule type
	SUBTYPE_T subtype;				// rule's subtype + their attr list
	struct _rulex *next; 			// next rule in the rule list
} RULEX_T;

//=========================================================================================================

typedef enum
{
	NAME,
	FORMAT,
	DOCUMENT
} TYPE;

typedef enum 
{
	ALLVAR,
	GLOBAL,
	LOCAL,
	STATIC,
	EXTERN,
	CONSTANT,
	DEFINEDCONST,
	ENUM,
	FUNCN,
	STRUCT,
	TYPEDEF,
	BRACEPOS,				//
	IFELSE,
	SWITCH,
	FUNCF,
	VARDECLAREPOS,
	OPSPACE,
	BLOCKCOMM,				//
	SINGLELINECOMM,
	TRAILCOMM
} SUB_TYPE;

typedef enum
{
	MAX_LENGTH,
	MIN_LENGTH,
	EXCEPTIONS,
	STYLE,
	PRERFIX,
	SUFFIX,
	REFERENCE,
	POSITION,
	MAX_LINE,
	ENABLE
} ATTR;

typedef enum
{
	INTEGER,
	STRING,
	STRINGLIST,
	TWOINTVAL,
	THREEVAL 						// 1 int for tell typedef, 1 int for ref, 1 string for pf/sf
} ATTR_VALUE;

typedef struct
{
	int type;						// attr value type
	int intVal[2];					// attr integer value min-max, ref use 2 int value -> may three for select prefix or sufffix
	char string[5];					// attr string value -> prefix, suffix
	char stringList[10][64];		// string list -> exceptions
}VALUE_T;

typedef struct _attr
{
	char *name;						// attr name
	VALUE_T value;
	int (*validate)(char *value);	// attr's value validate function
	int (*check)(CODE_T *data)
}ATTR_T;

typedef struct _subtype  			// bcz char array can't do this, string not same length
{
	int type;
	char *name;						// subtype name
	ATTR_T *attrList;				// list of attr belongs to that subtype
}SUBTYPE_T;

typedef struct _rule
{
	int id;							// rule id
	char name[64];					// rule name
	int type;						// rule type
	int subtype;					// rule's subtype + their attr list
	VALUE_T value;
	int (*check)(CODE_T *data, VALUE_T checkValue)
	struct _rulex *next; 			// next rule in the rule list
} RULE_T;

#define ATTR_COUNT 10

typedef struct
{
	int type;
	char subtype[];
	int attrList[ATTR_COUNT];
}CSDL_LIST_T;


CSDL_LIST_T csdlList[] =
{
	{NAME, "all-variable", {1,1,1,1,1,1,0,0,0,0}},
	{NAME, "global", {1,1,1,1,1,1,0,0,0,0}},
	{NAME, "local", {1,1,1,1,1,1,0,0,0,0}},
	{NAME, "static", {1,1,1,1,1,1,0,0,0,0}},
	{NAME, "extern", {1,1,1,1,1,1,0,0,0,0}},
	{NAME, "constant", {1,1,1,1,1,1,0,0,0,0}},
	{NAME, "defined-constant", {1,1,1,1,1,1,0,0,0,0}},
	{NAME, "enum", {1,1,1,1,1,1,0,0,0,0}},
	{NAME, "function", {1,1,1,1,1,1,0,0,0,0}},
	{NAME, "structure", {1,1,1,1,1,1,0,0,0,0}},
	{NAME, "typedef", {1,1,1,1,1,1,1,0,0,0}},
	{FORMAT, "brace-placement", {0,0,0,0,0,0,0,1,0,0}},				//
	{FORMAT, "if-else", {0,0,0,0,0,0,0,1,0,0}},
	{FORMAT, "switch-case", {0,0,0,0,0,0,0,1,0,0}},
	{FORMAT, "variable-declaration-placement", {0,0,0,0,0,0,0,0,0,1}},
	{FORMAT, "operation-spacing", {0,0,0,0,0,0,0,0,0,1}},
	{FORMAT, "function", {0,0,0,0,0,0,0,0,1,0}},				//
	{DOCUMENT, "block-comment", {0,0,0,0,0,0,0,1,0,0}},
	{DOCUMENT, "single-line-comment", {0,0,0,0,0,0,0,1,0,0}},
	{DOCUMENT, "trailing-comment", {0,0,0,0,0,0,0,1,0,0}}
}

ATTR_T attributeList[] = 
{
	{"MAX_LENGTH", TWOINTVAL, maxLengthValidate, maxLength},
	{"MIN_LENGTH", TWOINTVAL, minLengthValidate, minLength},
	{"EXCEPTIONS", STRINGLIST, exceptionsValidate, exceptions},
	{"STYLE", INTEGER, styleValidate, style},
	{"PRERFIX", THREEVAL, prefixValidate, prefix},
	{"SUFFIX", THREEVAL, suffixValidate, suffix},
	{"REFERENCE", INTVAL, referenceValidate, reference},
	{"POSITION", INTVAL, positionValidate, position},
	{"MAX_LINE", INTVAL, maxLineValidate, maxLine},
	{"ENABLE", INTVAL, enableValidate, enable}
};


typedef struct _code
{
	int number;
	int type;
	int count;
	char *value;
	struct _code *data;
	struct _code *next;
} CODE_T;


