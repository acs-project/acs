/* 
 * ruleController.c
 * 
 * Control initialize of each rule type.
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "acse.h"

CSDL_LIST_T csdlList[] =
{
	{NAME, "all-variable", {1,1,1,1,1,1,0,0,0,0}},
	{NAME, "global", {1,1,1,1,1,1,0,0,0,0}},
	{NAME, "local", {1,1,1,1,1,1,0,0,0,0}},
	{NAME, "static", {1,1,1,1,1,1,0,0,0,0}},
	{NAME, "extern", {1,1,1,1,1,1,0,0,0,0}},
	{NAME, "constant", {1,1,1,1,1,1,0,0,0,0}},
	{NAME, "defined-constant", {1,1,1,1,1,1,0,0,0,0}},
	{NAME, "enum", {1,1,1,1,1,1,0,0,0,0}},
	{NAME, "function", {1,1,1,1,1,1,0,0,0,0}},
	{NAME, "structure", {1,1,1,1,1,1,0,0,0,0}},
	{NAME, "typedef", {1,1,1,1,1,1,1,0,0,0}},
	{FORMAT, "brace-placement", {0,0,0,0,0,0,0,1,0,0}},				//
	{FORMAT, "if-else", {0,0,0,0,0,0,0,1,0,0}},
	{FORMAT, "switch-case", {0,0,0,0,0,0,0,1,0,0}},
	{FORMAT, "variable-declaration-placement", {0,0,0,0,0,0,0,0,0,1}},
	{FORMAT, "operation-spacing", {0,0,0,0,0,0,0,0,0,1}},
	{FORMAT, "function", {0,0,0,0,0,0,0,0,1,0}},				//
	{DOCUMENT, "block-comment", {0,0,0,0,0,0,0,1,0,0}},
	{DOCUMENT, "single-line-comment", {0,0,0,0,0,0,0,1,0,0}},
	{DOCUMENT, "trailing-comment", {0,0,0,0,0,0,0,1,0,0}}
};

ATTR_T attributeList[] = 
{
	{"MAX_LENGTH", {INTEGER}, maxLengthValidate, maxLength},
	{"MIN_LENGTH", {INTEGER}, minLengthValidate, minLength},
	{"EXCEPTIONS", {STRINGLIST}, exceptionsValidate, exceptions},
	{"STYLE", {INTEGER}, styleValidate, style},
	{"PRERFIX", {STRING}, prefixValidate, prefix},
	{"SUFFIX", {STRING}, suffixValidate, suffix},
	{"REFERENCE", {INTEGER}, referenceValidate, reference},
	{"POSITION", {INTEGER}, positionValidate, position},
	{"MAX_LINE", {INTEGER}, maxLineValidate, maxLine},
	{"ENABLE", {INTEGER}, enableValidate, enable}
};

RULE_LIST_T *ruleList = NULL;

int ruleListInit()
{
	int retVal = 0;
	if(ruleList != NULL)
		ruleListDestroy();
	ruleList = (RULE_LIST_T*) calloc(1, sizeof(RULE_LIST_T));
	if(ruleList == NULL)
		retVal = -1;
	return retVal;
}

void ruleListDestroy()
{
	if (ruleList != NULL)
	{
		RULE_T *item = ruleList->rHead;
		while(item != NULL)
		{
			RULE_T *next = item->next;
			free(item);
			item = next;
		}
		free(ruleList);
		ruleList = NULL;
	}
}

int insertRule(RULE_T *newRule)
{
	int retVal = 0;
	if(ruleList == NULL)
		retVal = -1;
	else
	{
		RULE_T *pTail = ruleList->rTail;
		//RULE_LIST_T *pNew = *rule;
		if (newRule == NULL)
			retVal = -2;
		else
		{
			if (pTail == NULL)
				ruleList->rHead = newRule;
			else
				pTail->next = newRule;
			ruleList->rTail = newRule;
			ruleList->rCurrent = newRule;
		}
	}
	return retVal;
}

RULE_T *getRuleHead()
{
	return ruleList->rHead;
}

RULE_T *getRuleCurrent()
{
	return ruleList->rCurrent;
}

int checkSubtype(int type, char* subtype)
{
	int i = 0;

	for (i = 0; i < sizeof(csdlList)/sizeof(CSDL_LIST_T); i++)
	{
		if (csdlList[i].type == type)
		{
			if (strcmp(csdlList[i].subtype, subtype) == 0)
				return i;
		}
	}

	return -1;
}

//return index of attr
// -1 : not exist
// -2 : incorrect subtype
int checkAttr(int subtype, char* attrName)
{
	int i = 0;

	for (i = 0; i < ATTR_COUNT; i++)
	{
		// check if attr exist or not
		if (strcmp(attributeList[i].name, attrName) == 0){
			// check if attr belong to subtype or not
			if (csdlList[subtype].attrList[i] == 1)
				return i;
			else
				return -2;
		}
	}

	return -1;
}

// return pointer to function
int* getEnforcementFunction(int attrIndex)
{
	return attributeList[attrIndex].check;
}

// retun attr
ATTR_T getAttr(int attrIndex)
{
	return attributeList[attrIndex];
}

// return VAUE_T 
VALUE_T checkValue(int subtype, int attrIndex, char* checkVal)
{
	VALUE_T value = attributeList[attrIndex].value;
	return attributeList[attrIndex].validate(subtype, value.type, checkVal);
}

/*int insertAttr(ATTR_T* aHead, ATTR_T* aTail, ATTR_T* aNew)
{
	int retVal = 0;
	if(aNew == NULL)
		retVal = -1;					// corrupt insert data
	else
	{
		if (aHead == NULL)
		{
			aHead = aNew;
			retVal = 1;
		}
		else
			aTail->next = aNew;
		aTail = aNew;
	}
	return retVal;
}*/

VALUE_T testPrint()
{
	RULE_T *currentRule = ruleList->rHead;
	int i = 0;

	printf("\nTEST PRINT\n");
	while(currentRule != NULL)
	{
		printf("==============\n");
		printf("RULE ID : %d\n", currentRule->id);
		printf("RULE NAME : \"%s\"\n", currentRule->name);
		printf("RULE TYPE : %d\n", currentRule->type);
		printf("RULE SUBTYPE : %d\n", currentRule->subtype);
		printf("RULE ATTRIBUTE\n");

		ATTR_T *currentAttr = currentRule->attrHead;
		while(currentAttr != NULL)
		{
			printf("	ATTR NAME : \"%s\"\n", currentAttr->name);
			VALUE_T currentValue = currentAttr->value;
			printf("	ATTR VALUE TYPE : %d\n", currentValue.type);
			switch(currentValue.type)
			{
				case INTEGER:
					printf("	- INT VAL : %d\n", currentValue.intVal[0]);
					break;
				case STRING:
					printf("	- STRING VAL : %s\n", currentValue.string);
					break;
				case STRINGLIST:
					printf("	- STRING LIST\n");
					for(i = 0; i < currentValue.intVal[0]; i++)
					{
						printf("	  - \"%s\"\n", currentValue.stringList[i]);
					}
					break;
				default:
					printf("eiei\n");
			}

			printf("\n");
			currentAttr = currentAttr->next;
		}
		printf("==============\n\n");

		currentRule = currentRule->next;
	}
}
