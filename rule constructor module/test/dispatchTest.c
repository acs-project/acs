#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char list[5][64] = {"hello", "world", "test", "dispatch", "eiei"};

typedef struct _node
{
    char *name;
    int index;
    int (*check)(struct _node node);
} NODE_T;

int checkName(NODE_T node)
{
    int i = 0;
    while(strcmp(list[i], node.name) != 0)
        i++;
    return i;
}

int main()
{
    NODE_T node;
    
    node.check = checkName;
    //strcpy(node.name, "test");
    node.name = strdup("test");
    node.index = node.check(node);
    printf("Node name : %s | Index : %d\n", node.name, node.index);
    

    return 0;
}

