#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef enum
{
	INTEGER,
	STRING,
	STRINGLIST,
	TWOINTVAL,
	THREEVAL 						// 1 int for tell typedef, 1 int for ref, 1 string for pf/sf
} ATTR_VALUE;

typedef struct
{
	int type;						// attr value type
	int intVal[2];					// attr integer value min-max, ref use 2 int value -> may three for select prefix or sufffix
	char string[5];					// attr string value -> prefix, suffix
	char stringList[10][64];		// string list -> exceptions
}VALUE_T;

typedef struct _attr
{
	char *name;						// attr name
	VALUE_T value;
	char *attrList[];
	//int (*validate)(char *value);	// attr's value validate function
	//int (*check)(CODE_T *data)
}ATTR_T;

ATTR_T attributeList[] = 
{
	{"MAX_LENGTH", (VALUE_T){TWOINTVAL}, {"test","eiei"}},
	{"MIN_LENGTH", (VALUE_T){TWOINTVAL}, {"test","eiei"}},
	{"EXCEPTIONS", (VALUE_T){STRINGLIST}, {"test","eiei"}},
	{"STYLE", (VALUE_T){INTEGER}, {"test","eiei"}},
	{"PRERFIX", (VALUE_T){THREEVAL}, {"test","eiei"}},
	{"SUFFIX", (VALUE_T){THREEVAL}, {"test","eiei"}},
	{"REFERENCE", (VALUE_T){INTEGER}, {"test","eiei"}},
	{"POSITION", (VALUE_T){INTEGER}, {"test","eiei"}},
	{"MAX_LINE", (VALUE_T){INTEGER}, {"test","eiei"}}
};

int main()
{
    //attributeList[0].attrList = (char**) calloc(3, sizeof(char*));
    //attributeList[0].attrList[0] = strdup("eiei");
    
    
    printf("Hello, World! %s %s\n", attributeList[0].name, attributeList[0].attrList[0]);

    return 0;
}

