// test module file
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "acse.h"

int main(int argc, char const *argv[])
{
	int retVal = 0;
	char *filename = "sampleRule.csdl";
	
	ruleListInit();
	retVal = readFile(filename);
	testPrint();
	ruleListDestroy();

	return retVal;
}