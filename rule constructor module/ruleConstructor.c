/*
 * ruleConstructor.c
 *
 * Rule Constructor Main Module. In able to run this module independently
 *
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "acse.h"

char* mystrdup(const char* data)
{
    char* d=malloc(strlen(data)+1);
    if(d==NULL)
        return NULL;
    strcpy(d,data);
    return d;
}

int checkFileType(char *filename, char *fileType)
{
	strtok(mystrdup(filename), "."); 					// flush filename
	char *inputFT = strtok(NULL, ".");
	if(inputFT != NULL)
	{
		if(strcmp(fileType,inputFT) == 0)
			return 1;
	}
	return 0;
}

int main(int argc, char const *argv[])
{
	int retVal = 0;
	int choice = 0;
	char *ruleFilename = "rule.csdl";
	char *logFilename = "log.txt";
	char *input[3] = {NULL};

	if(argc >= 1)
	{
		ruleListInit();
		switch(argc)
		{
			case 1:
				printf("Note : Use \"-help\" for help.\n");
				printf("Note : No specify user rule file. Using default rule file \"%s\".\n", ruleFilename);
				if(readFile(ruleFilename) == 0)
				{
					testPrint();
				}
				break;
			case 2:
				input[0] = mystrdup(argv[1]);
				if(checkFileType(input[0],"csdl"))
				{
					if(readFile(input[0]) == 0)
					{
						testPrint();
					}
				}
				else if(strcmp("-log",input[0]) == 0)
					printf("Note : Use default log \"%s\".\n",logFilename);
				else if(strcmp("-help",input[0]) == 0)
				{
					// printf("\nUsage:\n");
					// printf("  ruleConstructor [options] file...\n");
					// printf("  ruleConstructor [rule filename]       Disable log, Use user's rule filename\n");
					// printf("\nOptions :\n");
					// printf("  -help                                 Display this information\n");
					// printf("  -log [rule filename] [log filename]   Enable log, optional use user's rule\n");
					// printf("                                        and log filename\n");
					// printf("\nNote : [...] means optional\n");
					// printf("Information Date : 17/4/2017 04:39 GMT+7\n");
					printf("\nUsage:\n");
					printf("  ruleConstructor [options] file...\n");
					printf("  ruleConstructor                       Use default rule file \"rule.csdl\"\n");
					printf("  ruleConstructor [rule filename]       Use user's rule filename\n");
					printf("\nOptions :\n");
					printf("  -help                                 Display this information\n");
					printf("\nNote : [...] means optional\n");
					printf("Information Date : 17/4/2017\n");
				}
				free(input[0]);
				break;
			case 3:
				input[0] = mystrdup(argv[1]);
				input[1] = mystrdup(argv[2]);
				if((strcmp("-log",input[0]) == 0) && (checkFileType(input[1],"csdl")))
				{
					if(readFile(input[1]) == 0)
					{
						testPrint();
					}
				}
				else if((strcmp("-log",input[0]) == 0) && (checkFileType(input[1],"csdl") == 0))
				{
					printf("Note : No specify user rule file. Using default rule file \"%s\".\n", ruleFilename);
					printf("Note : Use input log file \"%s\".\n",input[1]);
					if(readFile(ruleFilename) == 0)
					{
						testPrint();
					}
				}
				free(input[0]);
				free(input[1]);
				break;
			case 4:
				input[0] = mystrdup(argv[1]);
				input[1] = mystrdup(argv[2]);
				input[2] = mystrdup(argv[3]);
				if((strcmp("-log",input[0]) == 0) && (checkFileType(input[1],"csdl")))
				{
					printf("Note : Use input log file \"%s\".\n",input[2]);
					if(readFile(input[1]) == 0)
					{
						testPrint();
					}
				}
				else
				{
					printf("Error : Wrong arguments input.\n");
					retVal = -1;
				}
				free(input[0]);
				free(input[1]);
				free(input[2]);
				break;
			default:
				printf("Error : Expect less than or equal 3 arguments, found %d.\n",argc-1);
				retVal = -1;
				break;
		}
		ruleListDestroy();
	}
	return retVal;
}