#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "acse.h"

REPORT_LIST_T *reportList = NULL;
FILE *reportOutput = NULL;
int reportCount = 0;

int reportListInit(char *fileName)
{
	int retVal = 0;

	reportOutput=fopen(fileName,"w");
	if(reportOutput==NULL)
		retVal= -1;
	else
	{
		if(reportList != NULL)
			reportListDestroy();
		reportList = (REPORT_LIST_T*) calloc(1, sizeof(REPORT_LIST_T));
		if(reportList == NULL)
			retVal = -1;
	}
	return retVal;
}

void reportListDestroy()
{
	if (reportList != NULL)
	{
		REPORT_T *item = reportList->reportHead;
		while(item != NULL)
		{
			REPORT_T *next = item->next;
			free(item);
			item = next;
		}
		free(reportList);
		reportList = NULL;
	}
}
void printHeader(char *sourceName, double timeUsed, char *datetime)
{
	fprintf(reportOutput,"=======================================================\n");
	fprintf(reportOutput,"DATE     : %s\n", datetime);
	fprintf(reportOutput,"FILENAME : %s\n", sourceName);
	fprintf(reportOutput,"TIMEUSE  : %f s\n", timeUsed);
	fprintf(reportOutput,"RULE VIOLATION : %d\n", reportCount);
	fprintf(reportOutput,"=======================================================\n");
}

void printReport()
{
	REPORT_T* current=reportList->reportHead;
	while(current != NULL)
	{
		fprintf(reportOutput, "Line#%3d: Non-compliance Rule#%2d -[%s]\n", current->lineNum, current->violatedNum, current->violatedName);
		fprintf(reportOutput, "  %s", current->code);
		fprintf(reportOutput, "\n\n");
		current=current->next;
	}
}

void reportFinish()
	{
	if(reportList != NULL)
	{
		printReport();
		reportListDestroy();
	}
	fclose(reportOutput);
	}

int report(int lineNum,char *code,int violatedNum,char *violatedName)
{
	int retVal = 0;
	if(reportList == NULL)
		retVal = -1;
	else
	{
		REPORT_T *newReport = (REPORT_T*) calloc(1,sizeof(REPORT_T));
		if(newReport==NULL)
		{
			retVal = -1;
		}
		else
		{
			reportCount++;
			newReport->lineNum=lineNum;
			newReport->code = mystrdup(code);
			newReport->violatedNum=violatedNum;
			newReport->violatedName = mystrdup(violatedName);
			
			REPORT_T *pTail = reportList->reportTail;
			if (pTail == NULL)
				reportList->reportHead = newReport;
			else
				pTail->next = newReport;
			reportList->reportTail = newReport;
			reportList->reportCurrent = newReport;
		}
	}
	return retVal;
}

void testPrintReport()
{
	REPORT_T* current=reportList->reportHead;
	while(current != NULL)
	{
		printf("lineNum: %d\n",current->lineNum);
		printf("code: %s\n",current->code);
		printf("violatedNum: %d\n",current->violatedNum);
		printf("violatedName: %s\n",current->violatedName);
		current=current->next;
	}
}
