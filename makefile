EXECUTABLES=acse

all : $(EXECUTABLES)

y.tab.c y.tab.h: project.y
	yacc -d project.y

y.tab.o: y.tab.c 
	gcc -c y.tab.c

lex.yy.o: y.tab.h lex.yy.c 
	gcc -c lex.yy.c

lex.yy.c: project.l lexyaccMechanism.c acse.h
	flex project.l lexyaccMechanism.c

ruleReader.o : ruleReader.c acse.h
	gcc -c ruleReader.c

ruleController.o : ruleController.c acse.h
	gcc -c ruleController.c

validateFunction.o : validateFunction.c acse.h
	gcc -c validateFunction.c

enforceFunction.o : enforceFunction.c acse.h y.tab.h
	gcc -c enforceFunction.c

enforcement.o : enforcement.c acse.h y.tab.h
	gcc -c enforcement.c

reportGenerator.o : reportGenerator.c acse.h
	gcc -c reportGenerator.c

acseUtil.o : acseUtil.c acse.h
	gcc -c acseUtil.c

main.o : main.c acse.h
	gcc -c main.c

acse : main.o acseUtil.o ruleController.o ruleReader.o validateFunction.o y.tab.o lex.yy.o enforceFunction.o enforcement.o reportGenerator.o
	gcc -o acse main.o acseUtil.o ruleController.o ruleReader.o validateFunction.o y.tab.o lex.yy.o enforceFunction.o enforcement.o reportGenerator.o

clean : 
	-rm *.o
	-rm acse
	-rm lex.yy.c
	-rm y.tab.c
	-rm y.tab.h

clear :
	-rm *.o
	-rm *.c
	-rm *.h
	-rm acse