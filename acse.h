#include "acseStructures.h"
#include "acseEnums.h"

/*
 * acseUtil.c
 */
int isdigits(char value[]);
char* mystrdup(const char* data);
int initCodeList(FILE *fileinput);
char* getCodeLine(int line);
int checkFileType(char *filename, char *fileType);
void checkReturn(int returnVal, int bReturn);
int isMarks(int type);
int isStatement(int type, int incFuncDef);
int isSpace(int type);
char *getFilename(char *path);

/*
 * ruleController.c
 */
int ruleListInit();
void ruleListDestroy();
int insertRule(RULE_T *newRule);
RULE_T *getRuleHead();
RULE_T *getRuleCurrent();
int checkSubtype(int type, char* subtype);
int checkAttr(int subtype, char* attrName);
int* getEnforcementFunction(int attrIndex);
ATTR_T getAttr(int attrIndex);
VALUE_T checkValue(int subtype, int attrIndex, char* checkVal);
VALUE_T testPrint();

/*
 * ruleReader.c
 */
int readFile (char *filename);

/*
 * project.l
 */
int getLine();

/*
 * lexyaccMechanism.c
 */
void typedefAndStructHandle(int choice,char* data);
int searchList(LIST_T* listHead,char* searchData);
int structListInit();
int typedefListInit();
int structlistDestroy();
int typedeflistDestroy();
int structListInsert(char* insertData);
int typedefListInsert(char* insertData);
LIST_T* getTypedefHead();
LIST_T* getStructHead();
void printStructList();
void printTypedefList();
int initCodeTree();
void codeTreeDestroy();
int insertToken(int flag, char *token);
void testPrintCode();
void traversal(CODE_T *node, void (*nodeFunc)(CODE_T *pCode));

int getIdentifierFlag();
int isException(int flag);
int pushChild(int line,int type,int flag,char* value);
int pushParent(CODE_NODE_T *parent);
int pushFlag(int flag);
CODE_NODE_T *popCode();
int popFlag();
int combine(int acseFlag, int yaccFlag);
int flagTransform(int oldFlag, int newFlag);
void codeStackClear();
void flagStackClear();
void printStack();
void printProgram(CODE_T *node);
CODE_T *getStackHeadData();
int pack();

/*
 * validateFunction.c
 */
VALUE_T maxLengthValidate(int subtype, int valType, char* checkVal);
VALUE_T minLengthValidate(int subtype, int valType, char* checkVal);
VALUE_T exceptionsValidate(int subtype, int valType, char* checkVal);
VALUE_T styleValidate(int subtype, int valType, char* checkVal);
VALUE_T prefixValidate(int subtype, int valType, char* checkVal);
VALUE_T suffixValidate(int subtype, int valType, char* checkVal);
VALUE_T referenceValidate(int subtype, int valType, char* checkVal);
VALUE_T positionValidate(int subtype, int valType, char* checkVal);
VALUE_T maxLineValidate(int subtype, int valType, char* checkVal);
VALUE_T enableValidate(int subtype, int valType, char* checkVal);

/*
 * enforcementFunction.c
 */
void maxLength(CODE_T *code, VALUE_T checkValue, RULE_T *rule);
void minLength(CODE_T *code, VALUE_T checkValue, RULE_T *rule);
void exceptions(CODE_T *code, VALUE_T checkValue, RULE_T *rule);
void style(CODE_T *code, VALUE_T checkValue, RULE_T *rule);
void prefix(CODE_T *code, VALUE_T checkValue, RULE_T *rule);
void suffix(CODE_T *code, VALUE_T checkValue, RULE_T *rule);
void reference(CODE_T *code, VALUE_T checkValue, RULE_T *rule);
void position(CODE_T *code, VALUE_T checkValue, RULE_T *rule);
void maxLine(CODE_T *code, VALUE_T checkValue, RULE_T *rule);
void enable(CODE_T *code, VALUE_T checkValue, RULE_T *rule);

/*
 * enforcement.c
 */
int enforcement();
NAME_LIST_T *nameListDestroy(NAME_LIST_T *list);
NAME_LIST_T *initNameList(NAME_LIST_T *list);
int insertNameList(NAME_LIST_T *list, char *data);

/*
 * reportGenerator.C
 */
int reportListInit(char *fileName);
void reportListDestroy();
int report(int lineNum,char *code,int violatedNum,char *violatedName);
void printHeader(char *sourceName, double timeUsed, char *datetime);
void reportFinish();
void testPrintReport();