var express = require('express');
var app = express();
var port = process.env.PORT || 5884;
var router = express.Router();
var viewPath = __dirname + '/views/';
var uploadDir = __dirname + '/directory/uploads/';
var resultDir = __dirname + '/directory/result/';
var formidable = require('formidable');
var fs = require('fs');
var path = require('path');
var exec = require('child_process').exec;
var ruleFile;
var count;

function getFilesName(dir) {
	return fs.readdirSync(dir);
}

router.use(function (req,res,next) {
  console.log("/" + req.method);
  next();
});

router.get("/", function(req, res) {
	res.sendFile(viewPath + "index.html");
});
router.get("/index", function (req, res) {
    res.send('<h1>This is index page</h1>');
});
router.post("/uploadFile", function (req, res) {
    var form = new formidable.IncomingForm();
    form.multiples = true;
    form.uploadDir = uploadDir;
    form.on('file', function(field, file) {
    	fs.rename(file.path, form.uploadDir + file.name);
    	console.log("fuck");
    });
    form.on('error', function(err) {
    	console.log('An error has occured: \n' + err);
    });
    form.on('fileBegin', function(name, file){
    	console.log("uploading " + file.name);
    	if(path.extname(file.name) === ".csdl"){
    		ruleFile = file.name;
    	}
    });
    form.parse(req);
    form.on('end', function() {
	    res.end("ok");
    });
});
router.post("/compile", function (req, res) {
	var strings = fs.readdirSync(uploadDir);
	var i = 0;
	var j = 0;
	console.log("uploaddir : "+strings);
	count = strings.length;
	for (i = 0; i < strings.length; i++) {
		if(path.extname(strings[i]) === ".c"){
			var command = "acse directory/uploads/"+strings[i]+" directory/uploads/"+ruleFile+" directory/result/report_"+strings[i].split('.')[0]+"_"+(new Date).getTime()%1000000+".txt";
			console.log("CMD : "+command);
			exec(command,function(error, stdout, stderr) {
			    if(error) throw error;
			    console.log(stdout);
			    if((j+1) == (strings.length-1))
			    	res.end("ok");
			    j++;
			});
		}
	}

	/*fs.readdirSync(uploadDir).foreach(file => {
		if(path.extname(file) === ".c"){
			var command = "acse directory/uploads/"+file+" directory/uploads/"+ruleFile+" directory/result/report_"+file.split('.')[0]+"_"+(new Date).getTime()%1000000+".txt";
			console.log("CMD : "+command);
			exec(command,function(error, stdout, stderr) {
			    if(error) throw error;
			    console.log(stdout);
			});
		}
	});*/
		
});

router.post("/clear", function(req, res) {
	clear();
});

function check (count, max, res) {
	if((count+1) == (max-1))
		res.end("ok");
}

function clear () {
	exec("ls directory/uploads/",function(error, stdout, stderr) {
    	if(error) throw error;
    	if(stdout) {
    		exec("rm directory/uploads/*",function(error, stdout, stderr) {
		    	if(error) throw error;
		    });
    	}
    });
    exec("ls directory/result/",function(error, stdout, stderr) {
    	if(error) throw error;
    	if(stdout) {
		    exec("rm directory/result/*",function(error, stdout, stderr) {
		    	if(error) throw error;
		    });
		}
	});
}

router.post("/getResult", function (req, res) {
	var strings = getFilesName(resultDir);
	console.log(strings);
	var json = JSON.stringify(strings);
	res.end(json);
});


app.use(express.static(__dirname + '/directory/result'));
app.use(express.static(__dirname + '/directory/uploads'));
app.use("/", router);

app.listen(port, function() {
    console.log('Starting node.js on port ' + port);
    clear();
    exec("extract-zip ACSE.zip",function(error, stdout, stderr) {
    	if(error) throw error;
    	exec("make",function(error, stdout, stderr) {
	    	if(error) throw error;
	    	exec("make clear",function(error, stdout, stderr) {
		    	if(error) throw error;
		    	console.log("Compile Done");
		    	exec("rm makefile",function(error, stdout, stderr) {
			    	if(error) throw error;
			    	/*var name = "onlineBookstore.c";
			    	exec("acse directory/uploads/onlineBookstore.c directory/uploads/rule.csdl directory/result/report_onlineBookstore_33141.txt",function(error, stdout, stderr) {
				    	if(error) throw error;
				    	console.log("Test Done");
			    	});*/
			    });
			    
		    });
	    });
    });
});