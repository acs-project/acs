#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "acse.h"
#include "y.tab.h"

extern int indentSpace;

typedef enum
{
	CAMEL,
	PASCAL,
	SNAKE,
	MACRO
}IDENT_STYLE;

typedef enum
{
	POS_INLINE,
	POS_BELOW,
	POS_BELOW_ONE_STEP,
	POS_ABOVE_FUNC,
	POS_HEADER,
	POS_ABOVE_STMT
}POS;

int lineCount = 0;
int foundLine[MAXLINE];

int bIdentifier = 0;
int bFuncName = 0;
int bGlobal = 1;
int bConst = 0;
int styleMode = 0;
NAME_LIST_T *identifierList = NULL;
void getIdentifier(CODE_T *pCode);

NAME_LIST_T *exceptionList = NULL;
int exceptionType = -1;

void maxLength(CODE_T *code, VALUE_T checkValue, RULE_T *rule)
{
	int i = 0;
	int index = 0;
	int hasException = 0;
	int isException = 0;
	LIST_T *exceptName = NULL;

	switch(rule->subtype)
	{
		case ST_ALLVAR:
			styleMode = ST_ALLVAR;
			break;
		case ST_FUNCN:
			styleMode = ST_FUNCN;
			break;
		case ST_GLOBAL:
			styleMode = ST_GLOBAL;
			break;
		case ST_LOCAL:
			styleMode = ST_LOCAL;
			break;
		case ST_CONSTANT:
			styleMode = ST_CONSTANT;
			break;
		default:
			printf("MAXLENGTH: Sorry, Your subtype not support yet.\n");
			return;
	}

	// clear old exception list
	ATTR_T *tmpAttr = rule->attrHead;
	while(tmpAttr != NULL)
	{
		if(strcmp("EXCEPTIONS", tmpAttr->name) == 0)
			hasException = 1;
		tmpAttr = tmpAttr->next;
	}

	lineCount = bIdentifier = bFuncName = bConst = 0;
	bGlobal = 1;
	memset(foundLine,0,sizeof(foundLine));
	identifierList = initNameList(identifierList);
	traversal(code,&getIdentifier);

	LIST_T *checkName = identifierList->listHead;
	while(checkName != NULL)
	{
		isException = 0;
		if(strlen(checkName->string) > checkValue.intVal[0])
		{
			if(hasException)
			{
				LIST_T *exceptName = exceptionList->listHead;
				while(exceptName != NULL)
				{
					if(strcmp(exceptName->string, checkName->string) == 0)
						isException = 1;
					exceptName = exceptName->next;
				}
			}
			
			if(!isException)
			{
				//printf("TEST : %s\n", checkName->string);
				report(foundLine[index],getCodeLine(foundLine[index]-1),rule->id,rule->name);
			}
		}
		index++;
		checkName = checkName->next;
	}
}

void minLength(CODE_T *code, VALUE_T checkValue, RULE_T *rule)
{
	int i = 0;
	int index = 0;
	int hasException = 0;
	int isException = 0;
	LIST_T *exceptName = NULL;

	switch(rule->subtype)
	{
		case ST_ALLVAR:
			styleMode = ST_ALLVAR;
			break;
		case ST_FUNCN:
			styleMode = ST_FUNCN;
			break;
		case ST_GLOBAL:
			styleMode = ST_GLOBAL;
			break;
		case ST_LOCAL:
			styleMode = ST_LOCAL;
			break;
		case ST_CONSTANT:
			styleMode = ST_CONSTANT;
			break;
		default:
			printf("MAXLENGTH: Sorry, Your subtype not support yet.\n");
			return;
	}

	// clear old exception list
	ATTR_T *tmpAttr = rule->attrHead;
	while(tmpAttr != NULL)
	{
		if(strcmp("EXCEPTIONS", tmpAttr->name) == 0)
			hasException = 1;
		tmpAttr = tmpAttr->next;
	}

	lineCount = bIdentifier = bFuncName = bConst = 0;
	bGlobal = 1;
	memset(foundLine,0,sizeof(foundLine));
	identifierList = initNameList(identifierList);
	traversal(code,&getIdentifier);

	LIST_T *checkName = identifierList->listHead;
	while(checkName != NULL)
	{
		isException = 0;
		if(strlen(checkName->string) < checkValue.intVal[0])
		{
			if(hasException)
			{
				LIST_T *exceptName = exceptionList->listHead;
				while(exceptName != NULL)
				{
					if(strcmp(exceptName->string, checkName->string) == 0)
						isException = 1;
					exceptName = exceptName->next;
				}
			}
			
			if(!isException)
				report(foundLine[index],getCodeLine(foundLine[index]-1),rule->id,rule->name);
		}
		index++;
		checkName = checkName->next;
	}
	
}

/***************************************************************************
 * EXCEPTIONS ENFORCEMENT
 */

void exceptions(CODE_T *code, VALUE_T checkValue, RULE_T *rule)
{
	int i = 0;
	exceptionList = initNameList(exceptionList);

	exceptionType = rule->subtype;
	for(i = 0; i < checkValue.intVal[0]; i++)
	{
		insertNameList(exceptionList, checkValue.stringList[i]);
	}
}

/***************************************************************************
 * STYLE ENFORCEMENT
 */

void getIdentifier(CODE_T *pCode)
{
	//printf("FIND %d %d\n",TYPE_SPEC,pCode->type);
	if(isMarks(pCode->type))
	{
		if((pCode->type == OP_PAR_BRACKET) || (pCode->type == OP_CUR_BRACKET))
		{
			bFuncName = 0;
			bGlobal = 0;
		}
		else if(pCode->type == ED_CUR_BRACKET)
		{
			//printf("Found global line:%d\n", pCode->fLine);
			bGlobal = 1;
		}
		if(pCode->type == COMM)
		{
			bIdentifier = 1;
		}
		else
		{
			bIdentifier = 0;
			bConst = 0;
		}
	}

	if(pCode->type == TYPE_SPEC)
	{
		if(pCode->parent->type == FUNC_DEF_STMT)
		{
			bFuncName = 1;
			bGlobal = 0;
		}
		bIdentifier = 1;
		
		CODE_T *tmp = pCode->parent->firstChild;
		while(tmp != pCode)
		{
			if(tmp->type == TYPE_QUAL)
			{
				if(strcmp("const",tmp->value) == 0)
					bConst = 1;
				else
					bConst = 0;
			}

			tmp = tmp->next;
		}
	}
	else if((bIdentifier && !bFuncName && (pCode->type == IDENTIFIER)) && (styleMode == ST_ALLVAR))
	{	// all variable name
		//printf("test: %s\n", pCode->value);
		insertNameList(identifierList, pCode->value);
		foundLine[lineCount] = pCode->fLine;
		lineCount++;
	}
	else if((bFuncName && (pCode->type == IDENTIFIER)) && (styleMode == ST_FUNCN))
	{	// function name
		insertNameList(identifierList, pCode->value);
		foundLine[lineCount] = pCode->fLine;
		lineCount++;
		bFuncName = 0;
	}
	else if((bIdentifier && !bFuncName && bConst && (pCode->type == IDENTIFIER)) && (styleMode == ST_CONSTANT))
	{	// constant name
		insertNameList(identifierList, pCode->value);
		foundLine[lineCount] = pCode->fLine;
		lineCount++;
	}
	else if((bIdentifier && !bFuncName && !bGlobal && (pCode->type == IDENTIFIER)) && (styleMode == ST_LOCAL))
	{	// local name
		insertNameList(identifierList, pCode->value);
		foundLine[lineCount] = pCode->fLine;
		lineCount++;
	}
	else if((bIdentifier && !bFuncName && bGlobal && (pCode->type == IDENTIFIER)) && (styleMode == ST_GLOBAL))
	{	// global name
		insertNameList(identifierList, pCode->value);
		foundLine[lineCount] = pCode->fLine;
		lineCount++;
	}

}

void style(CODE_T *code, VALUE_T checkValue, RULE_T *rule)
{
	int index = 0;
	int i = 0;
	int hasException = 0;
	int isException = 0;
	LIST_T *exceptName = NULL;
	char c;

	switch(rule->subtype)
	{
		case ST_ALLVAR:
			styleMode = ST_ALLVAR;
			break;
		case ST_FUNCN:
			styleMode = ST_FUNCN;
			break;
		case ST_GLOBAL:
			styleMode = ST_GLOBAL;
			break;
		case ST_LOCAL:
			styleMode = ST_LOCAL;
			break;
		case ST_CONSTANT:
			styleMode = ST_CONSTANT;
			break;
		default:
			printf("STYLE: Sorry, Your subtype not support yet.\n");
			return;
	}

	// clear old exception list
	ATTR_T *tmpAttr = rule->attrHead;
	while(tmpAttr != NULL)
	{
		if(strcmp("EXCEPTIONS", tmpAttr->name) == 0)
			hasException = 1;
		tmpAttr = tmpAttr->next;
	}

	lineCount = bIdentifier = bFuncName = bConst = 0;
	bGlobal = 1;
	memset(foundLine,0,sizeof(foundLine));
	identifierList = initNameList(identifierList);
	traversal(code,&getIdentifier);

	/*LIST_T *dummy = identifierList->listHead;
	printf("> %s\n", rule->name);
	while(dummy != NULL)
	{
		printf("TEST : %s\n", dummy->string);
		dummy = dummy->next;
	}*/

	LIST_T *foundIdent = identifierList->listHead;
	switch(checkValue.intVal[0])
	{
		case CAMEL:
			while(foundIdent != NULL)
			{
				isException = 0;
				for(i = 0; i < strlen(foundIdent->string); i++)
				{
					c = foundIdent->string[i];
					if((i == 0) && (isupper(c)))
					{
						if(hasException)
						{
							exceptName = exceptionList->listHead;
							while(exceptName != NULL)
							{
								if(strcmp(exceptName->string, foundIdent->string) == 0)
									isException = 1;
								exceptName = exceptName->next;
							}
						}
						
						if(!isException)
							report(foundLine[index],getCodeLine(foundLine[index]-1),rule->id,rule->name);
					}
					else if(c == '_')
					{
						if(hasException)
						{
							exceptName = exceptionList->listHead;
							while(exceptName != NULL)
							{
								if(strcmp(exceptName->string, foundIdent->string) == 0)
									isException = 1;
								exceptName = exceptName->next;
							}
						}
						
						if(!isException)
							report(foundLine[index],getCodeLine(foundLine[index]-1),rule->id,rule->name);
					}
				}
				index++;
				foundIdent = foundIdent->next;
			}
			break;
		case PASCAL:
			while(foundIdent != NULL)
			{
				isException = 0;
				for(i = 0; i < strlen(foundIdent->string); i++)
				{
					c = foundIdent->string[i];
					if((i == 0) && (islower(c)))
					{
						if(hasException)
						{
							exceptName = exceptionList->listHead;
							while(exceptName != NULL)
							{
								if(strcmp(exceptName->string, foundIdent->string) == 0)
									isException = 1;
								exceptName = exceptName->next;
							}
						}
						
						if(!isException)
							report(foundLine[index],getCodeLine(foundLine[index]-1),rule->id,rule->name);
					}
					else if(c == '_')
					{
						if(hasException)
						{
							exceptName = exceptionList->listHead;
							while(exceptName != NULL)
							{
								if(strcmp(exceptName->string, foundIdent->string) == 0)
									isException = 1;
								exceptName = exceptName->next;
							}
						}
						
						if(!isException)
							report(foundLine[index],getCodeLine(foundLine[index]-1),rule->id,rule->name);
					}
				}
				index++;
				foundIdent = foundIdent->next;
			}
			break;
		case SNAKE:
			while(foundIdent != NULL)
			{
				isException = 0;
				for(i = 0; i < strlen(foundIdent->string); i++)
				{
					c = foundIdent->string[i];
					if(isupper(c))
					{
						if(hasException)
						{
							exceptName = exceptionList->listHead;
							while(exceptName != NULL)
							{
								if(strcmp(exceptName->string, foundIdent->string) == 0)
									isException = 1;
								exceptName = exceptName->next;
							}
						}
						
						if(!isException)
							report(foundLine[index],getCodeLine(foundLine[index]-1),rule->id,rule->name);
					}
				}
				index++;
				foundIdent = foundIdent->next;
			}
			break;
		case MACRO:
			while(foundIdent != NULL)
			{
				isException = 0;
				for(i = 0; i < strlen(foundIdent->string); i++)
				{
					c = foundIdent->string[i];
					if(isupper(c))
					{
						if(hasException)
						{
							exceptName = exceptionList->listHead;
							while(exceptName != NULL)
							{
								if(strcmp(exceptName->string, foundIdent->string) == 0)
									isException = 1;
								exceptName = exceptName->next;
							}
						}
						
						if(!isException)
							report(foundLine[index],getCodeLine(foundLine[index]-1),rule->id,rule->name);
					}
				}
				index++;
				foundIdent = foundIdent->next;
			}
			break;
		default:
			printf("Error : Specify case does not exist\n");
			return;
	}
	identifierList = nameListDestroy(identifierList);
	
	
}

/***************************************************************************
 * PREFIX & SUFFIX ENFORCEMENT
 */
void prefix(CODE_T *code, VALUE_T checkValue, RULE_T *rule)
{
	int retVal = 0;

	if(rule->subtype == ST_TYPEDEF)
	{
		
	}
	
	
}

void suffix(CODE_T *code, VALUE_T checkValue, RULE_T *rule)
{
	int retVal = 0;
	
}

void reference(CODE_T *code, VALUE_T checkValue, RULE_T *rule)
{
	int retVal = 0;
	
}

/***************************************************************************
 * POSITION ENFORCEMENT
 */
int positionMode = 0;
int sFuncLine[MAXLINE];
int oBraceLine[MAXLINE];
int cBraceLine[MAXLINE];
int funcIndent[MAXLINE];
int obIndentCount[MAXLINE];
int cbIndentCount[MAXLINE];
int lastStmt[MAXLINE];
int switchIndent[MAXLINE];
int switchLine[MAXLINE];
int caseIndent[MAXLINE];
int caseLine[MAXLINE];
int ifLine[MAXLINE];
int ifIndent[MAXLINE];
int elseIndent[MAXLINE];
int elseLine[MAXLINE];
int tmpStmtCount = 0;
int indentCount = 0;
int stmtCount = 0;
int endStmt = 0;
int foundIfElse = 0; // Fix bug if statement else compound statement -> compound belong to if instead of else
void getBrace(CODE_T *pCode)
{
	int i = 0;
	if(pCode->type == NEWLINE)
		indentCount = 0;
	else if(pCode->type == SPACE)
		indentCount += strlen(pCode->value)/indentSpace;
	else if(pCode->type == TAB)
		indentCount += strlen(pCode->value);
	else if(pCode->parent != NULL)
	{
		if(((pCode->parent->type >= FUNC_DEF_STMT) && (pCode->parent->type <= IF_STMT)) || (pCode->parent->type == LABEL_STMT))
		{
			/*if (pCode->value != NULL)
			{
				printf("%s %d\n", pCode->value, pCode->fLine);
			}*/
			if(pCode->type == ELSE)
			{
				elseIndent[stmtCount] = indentCount;
				elseLine[stmtCount] = pCode->fLine;

				for(i = 0; i < stmtCount; i++)
				{
					if(elseLine[stmtCount] == cBraceLine[i])
						elseIndent[stmtCount] = cbIndentCount[i];
				}
			}
			if(pCode->type == IF)
			{
				ifIndent[stmtCount] = indentCount;
				ifLine[stmtCount] = pCode->fLine;
			}
			if(pCode->type == SWITCH)
			{
				switchIndent[stmtCount] = indentCount;
				switchLine[stmtCount] = pCode->fLine;
			}
			if((pCode->type == CASE) || (pCode->type == DEFAULT))
			{
				caseIndent[stmtCount] = indentCount;
				caseLine[stmtCount] = pCode->fLine;
				//printf("eiei %d %d %d\n", caseLine[stmtCount], caseIndent[stmtCount], stmtCount);
				stmtCount++;
			}
			if((pCode == pCode->parent->firstChild) || (pCode->type == ELSE))
			{
				sFuncLine[stmtCount] = pCode->fLine;
				funcIndent[stmtCount] = indentCount;
				//printf("FUNC : %d %d %s\n", funcIndent[stmtCount], sFuncLine[stmtCount], pCode->value);
				indentCount = 0;
				endStmt = 0;
				CODE_T *tmp = pCode;
				foundIfElse = 0;
				while((tmp != NULL) && !endStmt)
				{
					if(tmp->type == NEWLINE)
						indentCount = 0;
					else if(tmp->type == SPACE)
						indentCount += strlen(tmp->value)/indentSpace;
					else if(tmp->type == TAB)
						indentCount += strlen(tmp->value);
					else if((tmp->type == ELSE) || (tmp->type == IF))
						foundIfElse++;
					else if((tmp->type == COMP_STMT) && (foundIfElse == 1))
					{
						CODE_T *tmp2 = tmp->firstChild;
						while(tmp2 != NULL)
						{
							if(tmp2->type == OP_CUR_BRACKET)
							{
								oBraceLine[stmtCount] = tmp2->fLine;
								obIndentCount[stmtCount] = indentCount;
							}
							else if(tmp2->type == ED_CUR_BRACKET)
							{
								cbIndentCount[stmtCount] = indentCount;
								cBraceLine[stmtCount] = tmp2->fLine;
								stmtCount++;
								endStmt = 1;
								break;
							}
							else if(tmp2->type == NEWLINE)
								indentCount = 0;
							else if(tmp2->type == SPACE)
								indentCount += strlen(tmp2->value)/indentSpace;
							else if(tmp2->type == TAB)
								indentCount += strlen(tmp2->value);
							else
								lastStmt[stmtCount] = tmp2->lLine;
							tmp2 = tmp2->next;
						}
					}
					tmp = tmp->next;
				}
				//stmtCount++;
			}
		}
	}
}

int hasHeader = 0;			// attr are seperated
int isEndline[MAXLINE];
int isAboveSTMT[MAXLINE];
int isInline[MAXLINE];
CODE_T *prev = NULL;

int bcFirstLine[MAXLINE];
int bcLastLine[MAXLINE];

NAME_LIST_T *commentList = NULL;
void getComment(CODE_T *pCode)
{
	int bBefore = 1;
	int bOK = 0;
	CODE_T *tmp = NULL;

	switch(positionMode)
	{
		case ST_BLOCKCOMM:
			if((pCode->type == B_COMMENT) && (pCode->count != 1))
			{
				insertNameList(commentList, pCode->value);
				bcFirstLine[lineCount] = pCode->fLine;
				bcLastLine[lineCount] = pCode->lLine;
				//printf("%d %d %d\n", lineCount, bcFirstLine[lineCount], bcLastLine[lineCount]);
				lineCount++;
			}

			if(pCode->parent != NULL) // not root node
			{
				if(((pCode->parent->type == FUNC_DEF_STMT) || (pCode->parent->type == FUNC_PROTO)) && (pCode != prev))
				{
					prev = pCode->parent;
					foundLine[lineCount] = pCode->fLine;
				}
			}
			break;
		case ST_TRAILCOMM:
			if((pCode->type == B_COMMENT) && (pCode->count == 1))
			{
				insertNameList(commentList, pCode->value);
				foundLine[lineCount] = pCode->fLine;

				tmp = pCode->parent->firstChild;
				while(tmp != NULL)
				{
					if(tmp == pCode)
						bBefore = 0;
					else if(!isSpace(tmp->type) && bBefore && (tmp->lLine == pCode->fLine))
						bOK = 1;
					else if(!isSpace(tmp->type) && !bBefore && (tmp->lLine == pCode->fLine))
						bOK = 0;
					tmp = tmp->next;
				}
				if (bOK)
					isInline[lineCount] = 1;
				bOK = 0;
				bBefore = 1;
				lineCount++;
			}
			break;
		case ST_SINGLELINECOMM:
			if(pCode->type == SL_COMMENT)
			{
				insertNameList(commentList, pCode->value);
				foundLine[lineCount] = pCode->fLine;

				tmp = pCode->parent->firstChild;
				while(tmp != NULL)
				{
					if(tmp == pCode)
						bBefore = 0;
					else if(!isSpace(tmp->type) && bBefore && (tmp->lLine == pCode->fLine))
						bOK = 1;
					else if(!isSpace(tmp->type) && !bBefore && (tmp->lLine == pCode->fLine))
						bOK = 0;
					tmp = tmp->next;
				}
				if (bOK)
					isInline[lineCount] = 1;
				bOK = 0;
				bBefore = 1;

				tmp = pCode->next;
				while(tmp != NULL)
				{
					if(isStatement(tmp->type, 0))
					{
						if(pCode->lLine < tmp->fLine)
							isAboveSTMT[lineCount] = 1;
						break;
					}
					else if(!isSpace(tmp->type))
						break;
					tmp = tmp->next;
				}
				lineCount++;
			}
			break;
		default:
			printf("POSITION: Sorry, Your subtype not support yet.\n");
			return;
	}
}

void position(CODE_T *code, VALUE_T checkValue, RULE_T *rule)
{
	int i = 0;
	int j = 0;

	// assign mode
	if((rule->subtype < ST_BRACEPOS) || (rule->subtype == ST_VARDECLAREPOS) || (rule->subtype == ST_OPSPACE))
	{
		printf("POSITION: Sorry, Your subtype not support yet. %d\n",rule->subtype);
		return;
	}
	else
	{
		positionMode = rule->subtype;
		if(positionMode >= ST_BLOCKCOMM)
		{
			// initnamelist
			lineCount = 0;
			//memset(isEndline,0,sizeof(isEndline));
			memset(bcFirstLine,0,sizeof(bcFirstLine));
			memset(bcLastLine,0,sizeof(bcLastLine));
			memset(isAboveSTMT,0,sizeof(isAboveSTMT));
			memset(isInline,0,sizeof(isInline));
			memset(foundLine,0,sizeof(foundLine));
			commentList = initNameList(commentList);
			traversal(code,&getComment);
		}
		else
		{
			//initbracelist
			memset(sFuncLine,0,sizeof(sFuncLine));
			memset(oBraceLine,0,sizeof(oBraceLine));
			memset(cBraceLine,0,sizeof(cBraceLine));
			memset(funcIndent,0,sizeof(funcIndent));
			memset(obIndentCount,0,sizeof(obIndentCount));
			memset(cbIndentCount,0,sizeof(cbIndentCount));
			memset(lastStmt,0,sizeof(lastStmt));
			memset(caseIndent,0,sizeof(caseIndent));
			memset(switchIndent,0,sizeof(switchIndent));
			memset(switchLine,0,sizeof(switchLine));
			memset(ifLine,0,sizeof(ifLine));
			memset(ifIndent,0,sizeof(ifIndent));
			memset(elseIndent,0,sizeof(elseIndent));
			memset(elseLine,0,sizeof(elseLine));
			indentCount = 0;
			stmtCount = 0;
			traversal(code,&getBrace);
		}
	}

	

	// check
	switch(checkValue.intVal[0])
	{
		case POS_HEADER:
			hasHeader = 1;
			if(bcFirstLine[0] != 1)
			{
				report(1,getCodeLine(0),rule->id,rule->name);
				hasHeader = 0;
			}
			break;
		case POS_ABOVE_FUNC:
			for (i = 0; i < lineCount; i++)
			{
				if((foundLine[i] != (bcLastLine[i] + 1)) && !hasHeader)
				{
						printf("test2\n");
						report(foundLine[i],getCodeLine(foundLine[i]-1),rule->id,rule->name);
				}
			}
			break;
		case POS_ABOVE_STMT:
			for(i = 0; i < lineCount; i++)
			{
				if(isAboveSTMT[i] != 1)
					report(foundLine[i],getCodeLine(foundLine[i]-1),rule->id,rule->name);
			}
			break;
		case POS_INLINE:
			if (positionMode >= ST_BLOCKCOMM)	// 
			{
				for(i = 0; i < lineCount; i++)
				{
					if(isInline[i] != 1)
						report(foundLine[i],getCodeLine(foundLine[i]-1),rule->id,rule->name);
				}
			}
			else if (positionMode == ST_BRACEPOS)
			{
				for(i = 0; i < stmtCount; i++)
				{
					if(oBraceLine[i] + cBraceLine[i] != 0)
					{
						if((oBraceLine[i] - sFuncLine[i]) != 0)
							report(oBraceLine[i],getCodeLine(oBraceLine[i]-1),rule->id,rule->name);

						if((funcIndent[i] != cbIndentCount[i]) && ((elseLine != 0) && (elseIndent[i] != cbIndentCount[i])))
							report(cBraceLine[i],getCodeLine(cBraceLine[i]-1),rule->id,rule->name);
					}
				}
			}
			else if (positionMode == ST_IFELSE)
			{
				for(i = 0; i < stmtCount; i++)
				{
					if(elseLine[i] != 0)
					{
						for(j = 0; j < i; j++)
						{
							if(elseLine[i] == (cBraceLine[j] + 1))
								report(elseLine[i],getCodeLine(elseLine[i]-1),rule->id,rule->name);
						}
					}
				}
			}
			break;
		case POS_BELOW:
			if (positionMode == ST_BRACEPOS)
			{
				for(i = 0; i < stmtCount; i++)
				{
					if(oBraceLine[i] + cBraceLine[i] != 0)
					{
						if((oBraceLine[i] - sFuncLine[i]) != 1)
							report(oBraceLine[i],getCodeLine(oBraceLine[i]-1),rule->id,rule->name);

						if((funcIndent[i] != obIndentCount[i]) && ((elseLine[i] != 0) && (elseIndent[i] != obIndentCount[i])))
							report(oBraceLine[i],getCodeLine(oBraceLine[i]-1),rule->id,rule->name);

						if((funcIndent[i] != cbIndentCount[i]) && ((elseLine[i] != 0) && (elseIndent[i] != cbIndentCount[i])))
							report(cBraceLine[i],getCodeLine(cBraceLine[i]-1),rule->id,rule->name);
					}
				}
			}
			else if (positionMode == ST_IFELSE)
			{
				for(i = 0; i < stmtCount; i++)
				{
					if(elseLine[i] != 0)
					{
						for(j = 0; j < i; j++)
						{
							if(elseLine[i] == cBraceLine[j])
								report(elseLine[i],getCodeLine(elseLine[i]-1),rule->id,rule->name);
						}
					}
				}
			}
			else if (positionMode == ST_SWITCH)
			{
				int reported = 0;
				for(i = 0; i < stmtCount; i++)
				{
					if(caseLine[i] != 0)
					{
						for(j = 0; j < i; j++)
						{
							if(caseLine[i] == oBraceLine[j])
							{
								if(caseLine[i] == switchLine[j])
									reported = 1;
								report(caseLine[i],getCodeLine(caseLine[i]-1),rule->id,rule->name);
							}

							if(switchLine[j] != 0)
							{
								if((caseLine[i] >= switchLine[j]) && (caseLine[i] <= cBraceLine[j]))
								{
									if(caseLine[i] == switchLine[j])
									{
										if(reported)
											reported = 0;
										else
											report(caseLine[i],getCodeLine(caseLine[i]-1),rule->id,rule->name);
									}

									if(caseIndent[i] != switchIndent[j])
										report(caseLine[i],getCodeLine(caseLine[i]-1),rule->id,rule->name);
								}
							}
						}
					}
				}
			}
			break;
		case POS_BELOW_ONE_STEP:
			if (positionMode == ST_BRACEPOS)
			{
				for(i = 0; i < stmtCount; i++)
				{
					if(oBraceLine[i] + cBraceLine[i] != 0)
					{
						if((oBraceLine[i] - sFuncLine[i]) != 1)
						{
							// printf("test1 %d %d %d\n", oBraceLine[i], sFuncLine[i], elseLine[i]);
							report(oBraceLine[i],getCodeLine(oBraceLine[i]-1),rule->id,rule->name);
						}

						if((funcIndent[i] != (obIndentCount[i] - 1)) && ((elseLine[i] != 0) && (elseIndent[i] != (obIndentCount[i] - 1))))
						{
							// printf("test2 %d %d %d\n", oBraceLine[i], sFuncLine[i], elseLine[i]);
							// printf("    > %d %d %d\n", obIndentCount[i], funcIndent[i], elseIndent[i]);
							report(oBraceLine[i],getCodeLine(oBraceLine[i]-1),rule->id,rule->name);
						}

						if((funcIndent[i] != (cbIndentCount[i] - 1)) && ((elseLine[i] != 0) && (elseIndent[i] != (cbIndentCount[i] - 1))))
						{
							// printf("test3 %d %d %d\n", oBraceLine[i], sFuncLine[i], elseLine[i]);
							// printf("    > %d %d %d\n", obIndentCount[i], funcIndent[i], elseIndent[i]);
							report(cBraceLine[i],getCodeLine(cBraceLine[i]-1),rule->id,rule->name);
						}
					}
				}
			}
			else if (positionMode == ST_SWITCH)
			{
				int reported = 0;
				for(i = 0; i < stmtCount; i++)
				{
					if(caseLine[i] != 0)
					{
						for(j = 0; j < i; j++)
						{
							if(caseLine[i] == oBraceLine[j])
							{
								if(caseLine[i] == switchLine[j])
									reported = 1;
								report(caseLine[i],getCodeLine(caseLine[i]-1),rule->id,rule->name);
							}

							if(switchLine[j] != 0)
							{
								if((caseLine[i] >= switchLine[j]) && (caseLine[i] <= cBraceLine[j]))
								{
									if(caseLine[i] == switchLine[j])
									{
										if(reported)
											reported = 0;
										else
											report(caseLine[i],getCodeLine(caseLine[i]-1),rule->id,rule->name);
									}

									if(caseIndent[i] != (switchIndent[j]+1))
										report(caseLine[i],getCodeLine(caseLine[i]-1),rule->id,rule->name);
								}
							}
						}
					}
				}
			}
			break;
		default:
			printf("POSITION: Sorry, Your subtype not support yet.\n");
			return;
	}
}

int totalLine[MAXLINE];

void getFunction(CODE_T *pCode)
{
	if(pCode->type == FUNC_DEF_STMT)
	{
		foundLine[lineCount] = pCode->fLine;
		totalLine[lineCount] = pCode->count;
		lineCount++;
	}
}

void maxLine(CODE_T *code, VALUE_T checkValue, RULE_T *rule)
{
	int i = 0;
	lineCount = 0;
	memset(foundLine,0,sizeof(foundLine));
	memset(totalLine,0,sizeof(totalLine));

	traversal(code, &getFunction);

	for(i = 0; i < lineCount; i++)
	{
		if(totalLine[i] > checkValue.intVal[0])
			report(foundLine[i],getCodeLine(foundLine[i]-1),rule->id,rule->name);
	}
}

void enable(CODE_T *code, VALUE_T checkValue, RULE_T *rule)
{
	int retVal = 0;
	
}
