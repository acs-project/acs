/*****************************************************************************************************************************
 *
 *      onlineBookstore.c
 *
 *      This program start with asking for the user's login name (6 to 10 alphabetic or numeric characters starting with
 * an alphabetic character for registered users, or "guest"). A user name of "END" indicates the end of the day.
 *      For each customer, it will repeatedly ask for the book title (“END” to quit), the book format (ebook, paperback, or
 * hardcover) and the number of copies. Ebooks cost 120 baht, paperbacks cost 300 baht, and hardcover books cost 600 baht.
 *      Registered users receive a 10% discount if they buy up to 5 books (of any format) in one order and a 15%
 * discount if they buy 6 or more books in one order.
 *      When a customer is finished entering book titles, it will print an invoice, showing the user
 * login name, plus a record of each transaction in order: title, format, number of copies, per copy price and
 * extended price (number of copies times per copy price), plus the invoice total before discount, and the
 * invoice total after discount.
 *      At the end of the day print the total number of each book format sold, the number of registered users served,
 * the number of guests served, and the total amount received.
 *
 *  Created by Tanyagorn Benjaprompadung (Gring) ID 58070503413
 *  27 September 2015
 *
 *****************************************************************************************************************************
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>

#define MAXCUSTOMERS 20 /* the maximum number of customers */
#define LIMITBOOK 10    /* the maximum number of different title books name */
#define MAXLEN 128      /* the maximum length of strings */

/* Print welcome message
 * No argument and No return value
 */ 
void printWelcome()
    {
    printf("\n\t\t===========================================\n");
    printf("\t\t||                WELCOME                ||\n");
    printf("\t\t||                   TO                  ||\n");
    printf("\t\t||     Tanyagorn's Online Bookstore      ||\n");
    printf("\t\t===========================================\n");
    }

/* Check login name, allow user to enter guest,END and login name with
 * 6 to 10 alphabetic or numeric characters starting with an alphabetic character for registered users
 * Arguments: loginName - input string from user
 * Return: no return value
 */
void checkLoginName(char loginName[])
    {
    char inputline[MAXLEN];    /* buffer for inputting chars from user */

    memset(inputline,0,sizeof(inputline)); /* set buffer to all zeros */

    /* loop until user enters a valid login name */
    while(1)
        {
        if(loginName[strlen(loginName)-1] =='\n')
            loginName[strlen(loginName)-1] = '\0';
        if(strcmp(loginName,"guest")==0)
            break;
        else if(strcmp(loginName,"END")==0)
            break;
        /* if the first character is not an alphabet or the length of login name is less than 6 or more than 10 characters,
         * print a message and ask for the login name again until receive a valid login name.
         */
        else if((isdigit(loginName[0])!=0) || ((strlen(loginName)<6) || (strlen(loginName)>10)))
            {
            printf("   Invalid user name - Your login name must contain 6 to 10 alphabetic\n");
            printf("   or numeric characters starting with an alphabetic character\n");
            printf("Enter your login name (type 'guest' for unregistered users): ");
            fgets(inputline,sizeof(inputline),stdin);
            sscanf(inputline,"%s",loginName);
            }
        else
            break;
        }
    }

/* Get title of the book
 * if user enter a blank or empty strings, keep asking until user did type something
 * Arguments: titleOfBooks - output argument for storing the title of the book
 *            titleBooksCount - number of title book count for each customer
 * Return: no return value
 */
void getBookTitle(char titleOfBooks[LIMITBOOK][MAXLEN],int titleBooksCount)
    {
    char inputline[MAXLEN]; /* buffer for inputting chars from user */

    /* loop until user enters a valid title of the book (can be anything but empty strings) */
    while(1)
        {
        printf("\n\tEnter title of the book (type 'END' to quit): ");
        fgets(inputline,sizeof(inputline),stdin);
        if(strlen(inputline) == 1) /* user didn't type anything, give a message and ask again */
            printf("\t   You didn't type anything!");
        else
            {
            /* Remove the '\n' from the string but retain any embedded spaces */
            if(inputline[strlen(inputline)-1]=='\n')
                inputline[strlen(inputline)-1]='\0';
            strcpy(titleOfBooks[titleBooksCount],inputline);
            break;
            }
        }
    }

/* Use menus to get book format from the user and check that the user
 * enters a valid option (an integer number from 1 through 3)
 * Arguments: no arguments
 * Return: string of format of the book
 */
char* getBookFormat()
    {
    char inputline[MAXLEN];     /* buffer for inputting chars from user */
    char stringOption[MAXLEN];  /* string for holding temporary option (use this to check that user enters a valid option or not) */
    int integerNumber=0;        /* a value for checking that user enters a valid option or not */
    int option=0;               /* to hold the value of option (an integer number from 1 through 3) */
    int i=0;                    /* the constant number for loops */

    printf("\tWhat is the type of book?\n");
    printf("\t\t1 - ebook\n");
    printf("\t\t2 - paperback\n");
    printf("\t\t3 - hardcover\n");

    /* loop until the user enters a valid option */
    while(1)
        {
        integerNumber=0;

        printf("\tYour book format is: ");
        fgets(inputline,sizeof(inputline),stdin);
        sscanf(inputline,"%s",stringOption);
        /* to check that user enters a positive integer
         * if not, print an error message and keep asking until user enters a valid number
         */
        for(i=0; i<strlen(stringOption); i++)
            {
            if(isdigit(stringOption[i])==0)
               integerNumber=1;
            }

        if(integerNumber==1)
            printf("\t   Invalid option - please enter an integer number from 1 through 3\n");
        else
            {
            option=atoi(stringOption);  /* convert string to integer */

            if(option==1)
                return "ebook";
            if(option==2)
                return "paperback";
            if(option==3)
                return "hardcover";
            else
                printf("\t   Invalid option - please enter an integer number from 1 through 3\n");
            }
        }
    }

/* Get number of copies for each title of the book and check that the user
 * enters a valid number (an integer number that greater than 0)
 * Arguments: no arguments
 * Return: the number of copies for each title of the book
 */
int getNumberOfCopies()
    {
    char inputline[MAXLEN];             /* buffer for inputting chars from user */
    char stringNumberOfCopies[MAXLEN];  /* string for holding temporary number of copies (for checking valid number) */
    int integerNumber=0;                /* a value for checking that user enters a positive integer or not */
    int integerNumberOfCopies=0;        /* to hold the value of number of copies (an integer number that greater than 0) */
    int i=0;                            /* the constant number for loops */

    /* loop until user enters a valid number */
    while(1)
        {
        integerNumber=0;

        printf("\tHow many copies do you want? ");
        fgets(inputline,sizeof(inputline),stdin);

        if(strlen(inputline)==1) /* user didn't type anything */
            printf("\t    Please enter an integer number that greater than 0\n");

        /* to check that user enters a positive integer and that number is not 0
         * if not, print an error message and keep asking until user enters a valid number
         */
        else
            {
            if(inputline[strlen(inputline)-1]=='\n')
                inputline[strlen(inputline)-1]='\0';

            strcpy(stringNumberOfCopies,inputline);
            for(i=0; i<strlen(stringNumberOfCopies); i++)
                {
                if(isdigit(stringNumberOfCopies[i])==0)
                    integerNumber=1;
                }
            if(strcmp(stringNumberOfCopies,"0") == 0)
                integerNumber=1;
            if(integerNumber==1)
                printf("\t   Please enter an integer number that greater than 0\n");
            if(integerNumber==0)
                {
                integerNumberOfCopies=atoi(stringNumberOfCopies); /* convert string to integer */
                return integerNumberOfCopies;
                }
            }
        }
    }

/* calculate total price before discount for each customer
 * and print an invoice (everything except total price after discount)
 * Arguments: titles - a string of title of each book
 *            titleBooksCount - a number of title book in the array
 *            format - a string of format of each title of the book
 *            loginName - a string of login name of each customer (including guest)
 *            customerCount - a number of customer in the array
 *            number of copies - a positive integer number of number of copies of each title of the book
 * Return: total price before discount for each customer
 */
int printInvoice1AndGetBeforePrice(char titles[LIMITBOOK][MAXLEN], int titleBooksCount, char format[LIMITBOOK][MAXLEN],char loginName[MAXCUSTOMERS][MAXLEN], int customerCount, int numberOfCopies[])
    {
    int i=0;                           /* the constant number for loops */
    int perCopyPrice=0;                /* to hold the value of price per copy */
    int extendedPrice[LIMITBOOK];      /* to hold the value of number of copies times per copy price */
    int totalPriceBeforeDiscount=0;    /* to hold the value of total price before discount */

    printf("\n\t======================================================");
    printf("\n\tInvoice\n");
    printf("\tuser: %s\n",loginName[customerCount]);

    /* loop for printing an invoice until there is no title of the book left */
    for(i=0; i<titleBooksCount; i++)
        {
        printf("\t------------------------------------------------------\n");
        printf("\tOrder: %d\n",i+1);
        printf("\tTitle: %s\n",titles[i]);
        printf("\tFormat: %s\n",format[i]);
        printf("\tNumber of copies: %d\n",numberOfCopies[i]);

        if(strcmp(format[i],"ebook")==0)
            perCopyPrice=120;
        if(strcmp(format[i],"paperback")==0)
            perCopyPrice=300;
        if(strcmp(format[i],"hardcover")==0)
            perCopyPrice=600;

        printf("\tPrice per copy: %d baht\n",perCopyPrice);
        extendedPrice[i]=perCopyPrice*numberOfCopies[i];
        printf("\tExtended price: %d baht\n",extendedPrice[i]);
        totalPriceBeforeDiscount=extendedPrice[i]+totalPriceBeforeDiscount;
        }

    printf("\t------------------------------------------------------\n");
    printf("\tTotal price before discount: %d baht\n",totalPriceBeforeDiscount);
    return totalPriceBeforeDiscount;
    }

/* calculate total price after discount for each customer
 * and print an invoice (only total price after discount)
 * Arguments: customerCount - a number of customer in the array
 *            numberOfBooks - a total number of books that user want to buy of each customer
 *            loginName - a string of login name of each customer (including guest)
 *            totalPriceBeforeDiscount - the total price before discount of each customer
 * Return: total price after discount for each customer
 */
int printInvoice2AndGetAfterPrice(int customerCount,int numberOfBooks[],char loginName[MAXCUSTOMERS][MAXLEN],int totalPriceBeforeDiscount[])
    {
    int totalPriceAfterDiscount=0;  /* to hold the value of total price after discount */

    if(strcmp(loginName[customerCount],"guest")==0)    /* quests receive no discount */
        {
        totalPriceAfterDiscount=totalPriceBeforeDiscount[customerCount];
        printf("\tTotal price after discount: %d baht\n",totalPriceAfterDiscount);
        printf("\t======================================================\n");
        return totalPriceAfterDiscount;
        }
    else    /* registered users; receiving discount depends on the total number of books */
        {
        if(numberOfBooks[customerCount]==5)
            {
            totalPriceAfterDiscount=(totalPriceBeforeDiscount[customerCount]*90)/100;
            printf("\tYou receive a 10 percent discount (buy up to 5 books)\n");
            printf("\tTotal price after discount: %d baht\n",totalPriceAfterDiscount);
            printf("\t======================================================\n");
            return totalPriceAfterDiscount;
            }
        if(numberOfBooks[customerCount]>=6)
            {
            totalPriceAfterDiscount=(totalPriceBeforeDiscount[customerCount]*85)/100;
            printf("\tYou receive a 15 percent discount (buy up to 6 books)\n");
            printf("\tTotal price after discount: %d baht\n",totalPriceAfterDiscount);
            printf("\t======================================================\n");
            return totalPriceAfterDiscount;
            }
        else
            {
            totalPriceAfterDiscount=totalPriceBeforeDiscount[customerCount];
            printf("\tNo discount (buy less than 5 books)\n");
            printf("\tTotal price after discount: %d baht\n",totalPriceAfterDiscount);
            printf("\t======================================================\n");
            return totalPriceAfterDiscount;
            }
        }
    }

/* Calculate total amount received
 * Arguments: totalPriceAfterDiscount - array where the total price after discount of each customer be stored
 *            customerCount - a number of customer in the array
 * Return: total amount received
 */
int getTotalAmountReceived(int totalPriceAfterDiscount[], int customerCount)
    {
    int i=0;                    /* the constant number for loops */
    int totalAmountReceived=0;  /* to hold the value of total amount received */

    for(i=0; i<customerCount; i++)
        {
        totalAmountReceived=totalAmountReceived+totalPriceAfterDiscount[i];
        }
    return totalAmountReceived;
    }

/* Print the summary of today when the user enters 'END' for login name
 * Arguments: ebook, paperback and hardcover - the total number of each book format sold
 *            numberOfRegisteredUsers - the total number of registered users that using this program
 *            numberOfGuests - the total number of guests that using this program
 *            totalAmountReceived - the value of total amount received
 * Return: no return value
 */
void printSummaryOfTheDay(int ebook,int paperback,int hardcover,int numberOfRegisteredUsers,int numberOfGuests,int totalAmountReceived)
    {
    printf("\n\t========================================\n");
    printf("\tSummary of today\n");
    printf("\t----------------------------------------\n");
    printf("\tTotal number of each book format sold\n");
    printf("\tEbook: %d\n",ebook);
    printf("\tPaperback: %d\n",paperback);
    printf("\thardcover: %d\n",hardcover);
    printf("\t----------------------------------------\n");
    printf("\tNumber of registered users served: %d\n",numberOfRegisteredUsers);
    printf("\tNumber of guests served: %d\n",numberOfGuests);
    printf("\t----------------------------------------\n");
    printf("\tTotal amount received: %d baht\n",totalAmountReceived);
    printf("\t========================================\n\n");
    }

int main()
    {
    char inputline[MAXLEN];            	    	/* buffer for inputting chars from user */
    char loginName[MAXCUSTOMERS][MAXLEN];   	/* string for holding login name of each customer */
    char titleOfBooks[LIMITBOOK][MAXLEN];   	/* string for holding titles of the book */
    char formatOfBooks[LIMITBOOK][MAXLEN];  	/* string for holding format of each title book */
    int numberOfCopies[LIMITBOOK];          	/* to hold the value of number of copies of each title book */
    int totalPriceBeforeDiscount[MAXCUSTOMERS]; /* to hold the value of total price before discount of each customer */
    int totalPriceAfterDiscount[MAXCUSTOMERS];  /* to hold the value of total price after discount of each customer */
    int numberOfGuests=0;                   	/* to hold the value of total number of guests served */
    int numberOfRegisteredUsers=0;          	/* to hold the value of total number of registered users served */
    int numberOfBooks[MAXCUSTOMERS];        	/* to hold the value of total number of books that user want to buy of each customer */
    int customerCount=0;                    	/* the constant number for loops (MAXCUSTOMERS) */
    int titleBooksCount=0;                  	/* the constant number for loops (LTMITBOOK)    */
    int ebook=0;                            	/* to hold the value of total number of ebook sold */
    int paperback=0;                        	/* to hold the value of total number of paperback sold */
    int hardcover=0;                        	/* to hold the value of total number of hardcover sold */
    int totalAmountReceived=0;              	/* to hold the value of total amount received */

    printWelcome();

    /* loop for each customer, maximum number of customer is 20 */
    for(customerCount=0; customerCount<MAXCUSTOMERS; customerCount++)
        {
        printf("\nEnter your login name (type 'guest' for unregistered users): ");
        fgets(inputline,sizeof(inputline),stdin);
        sscanf(inputline,"%s",loginName[customerCount]);

        checkLoginName(loginName[customerCount]);

        if(strcmp(loginName[customerCount],"guest")==0)
            numberOfGuests=numberOfGuests+1;
        else if(strcmp(loginName[customerCount],"END")==0)
            break;
        else
            numberOfRegisteredUsers=numberOfRegisteredUsers+1;

        /* loop for getting transaction info of each title of the book.
         * LIMITBOOK=10 so the program can hold 10 different title of the books per customer.
         */
        for(titleBooksCount=0; titleBooksCount<LIMITBOOK; titleBooksCount++)
            {
            printf("\n\tOrder: %d",titleBooksCount+1);
            getBookTitle(titleOfBooks,titleBooksCount);

            if(strcmp(titleOfBooks[titleBooksCount],"END")==0) /* break the loop to print an invoice */
                break;

            strcpy(formatOfBooks[titleBooksCount],getBookFormat());
            numberOfCopies[titleBooksCount]=getNumberOfCopies();
            numberOfBooks[customerCount]=numberOfBooks[customerCount]+numberOfCopies[titleBooksCount];

            /* count the number of total of each book format sold,
             * we will use this information when user had entered END to indicate the end of the day
             */
            if(strcmp(formatOfBooks[titleBooksCount],"ebook")==0)
                ebook=numberOfCopies[titleBooksCount]+ebook;
            if(strcmp(formatOfBooks[titleBooksCount],"paperback")==0)
                paperback=numberOfCopies[titleBooksCount]+paperback;
            if(strcmp(formatOfBooks[titleBooksCount],"hardcover")==0)
                hardcover=numberOfCopies[titleBooksCount]+hardcover;
            }
        /* if the customer buy more than 10 different titles of the book,
         * print a message and act as if the customer had entered END for title of the book.
         */
        if(titleBooksCount==10)
            printf("\n\t   Sorry, we allow you to buy only 10 different titles of the book per customer\n");

        totalPriceBeforeDiscount[customerCount]=printInvoice1AndGetBeforePrice(titleOfBooks,titleBooksCount,formatOfBooks,loginName,customerCount,numberOfCopies);
        totalPriceAfterDiscount[customerCount]=printInvoice2AndGetAfterPrice(customerCount,numberOfBooks,loginName,totalPriceBeforeDiscount);
        }
    /* if we have more than 20 customers (limit number of customers in this program),
     * print a message and act as if the user had entered END for login name (indicates the end of the day)
     */
    if(customerCount==20)
        printf("\n\t   We already served 20 customers today. It's time to close!");

    totalAmountReceived=getTotalAmountReceived(totalPriceAfterDiscount,customerCount);
    printSummaryOfTheDay(ebook,paperback,hardcover,numberOfRegisteredUsers,numberOfGuests,totalAmountReceived);
    }
