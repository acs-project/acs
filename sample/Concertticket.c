/******************************************************************
* CPE 100 Project                                                 *
* Concertticket.c                                                 *
*                                                                 *
* Created by Nawaphon Boonnitikorn                                *
* 3416 Section C                                                  *
*******************************************************************/
#include<stdio.h>
#include<string.h>
#include<ctype.h>

/*This function check that this string is a digit or not */
int checkDigit(char stringinput[100])
{
    int i;
    for (i=0; i< strlen(stringinput) ; i++)
    {
        if(!isdigit(stringinput[i]))
            return 0;
    }
    return 1;
}
/*This function check that this string is a alphabet or not */
int checkAlpha(char stringinput[100])
{
    int count;
    for(count=0 ; count < strlen(stringinput); count++)
    {
        if(!isalpha(stringinput[count]))
            return 0;
    }
    return 1;
}
/*function : give customer choices to select the artist */
int askForArtist()
{
    char stringinput[100];
    int artist =0; /*choice of the artist */

    while(artist < 1 || artist > 4)
    {
        printf("\t \t --------------------------------------------------\n");
        printf("\t \t **************************************************\n");
        printf("\t \t * PLEASE SELECT THE FOLLOWING NUMBER FOR ARTIST. *\n");
        printf("\t \t **************************************************\n");
        printf("\t \t --------------------------------------------------\n");
        printf("\t\t\t ENTER 1: Big Ass      (1700 BAHT/TICKET)\n" );
        printf("\t\t\t ENTER 2: Bodyslam     (2000 BAHT/TICKET)\n" );
        printf("\t\t\t ENTER 3: Taylor Swift (3000 BAHT/TICKET)\n" );
        printf("\t\t\t ENTER 4: Lady Gaga    (3500 BAHT/TICKET)\n" );
        printf("\t \t --------------------------------------------------\n");
        printf("ENTER THE NUMBER FOR SELECT THE ARTIST : ");
        fgets(stringinput,sizeof(stringinput),stdin);
        sscanf(stringinput,"%d",&artist);
        rewind(stdin);
        printf("==================================================\n");
        if(artist < 1 || artist > 4)
        {
            printf("=======___===__====__====__====__===========\n");
            printf("======|___  |__|  |__|  |  |  |__|  |=======\n");
            printf("      |___  |  %c  |  %c  |__|  |  %c  |       \n",92,92,92);
            printf("          PLEASE RE ENTER AGAIN !           \n");
            printf("              (Between 1-4)                 \n");
            printf("=======   == === = === ==  === === =========\n");
        }
    }
    return artist;
}
/*function : get the number ticket*/
int askForNumberTicket()
{
    char stringinput[100];
    int number_of_Ticket = -1;/*how much ticket that customer want */
    while (number_of_Ticket < 0)
    {
        printf("Enter the number of ticket (Enter 0 print to invoice) : ");
        fgets(stringinput,sizeof(stringinput),stdin);
        sscanf(stringinput,"%d",&number_of_Ticket);
        rewind(stdin);
        printf("---------------------------------\n");
        if(number_of_Ticket < 0)
        {
            printf("=======___===__====__====__====__===========\n");
            printf("======|___  |__|  |__|  |  |  |__|  |=======\n");
            printf("      |___  |  %c  |  %c  |__|  |  %c  |       \n",92,92,92);
            printf("          PLEASE RE ENTER AGAIN !           \n");
            printf("           (Enter only integers)            \n");
            printf("=======   == === = === ==  === === =========\n");
        }
    }
    return number_of_Ticket;
}

/*function : ask the customer which day that*/
void askForDate(char date[])
{
    int checkDay[]= {0,31,28,31,30,31,30,31,31,30,31,30,31}; /* array to check day in each month */
    int day = 0;
    int month = 0;
    int year = 0;

    while(1)
    {
        printf("Enter the date of concert (DD/MM/YYYY) :  ");
        fgets(date,100,stdin);
        if (date[strlen(date)-1] == '\n')
            date[strlen(date)-1] = 0;
        sscanf(date,"%d/%d/%d",&day,&month,&year);
        printf("==================================================\n");
        /*check leap year */
        if(year%4 == 0)
        {
            checkDay[2] = 29;
        }
        /*check day in each month*/
        if(day <= 0 || day > checkDay[month])
        {
            printf("=======___===__====__====__====__===========\n");
            printf("======|___  |__|  |__|  |  |  |__|  |=======\n");
            printf("      |___  |  %c  |  %c  |__|  |  %c  |       \n",92,92,92);
            printf("          PLEASE RE ENTER AGAIN !           \n");
            printf("                                            \n");
            printf("=======   == === = === ==  === === =========\n");
            continue;
        }
        /*check month have to between 1-12 */
        if(month <= 0 || month > 12)
        {
            printf("=======___===__====__====__====__===========\n");
            printf("======|___  |__|  |__|  |  |  |__|  |=======\n");
            printf("      |___  |  %c  |  %c  |__|  |  %c  |       \n",92,92,92);
            printf("          PLEASE RE ENTER AGAIN !           \n");
            printf("                                            \n");
            printf("=======   == === = === ==  === === =========\n");
            continue;
        }
        /*check year have to be more than 2014 */
        if(year < 2015)
        {
            printf("=======___===__====__====__====__===========\n");
            printf("======|___  |__|  |__|  |  |  |__|  |=======\n");
            printf("      |___  |  %c  |  %c  |__|  |  %c  |       \n",92,92,92);
            printf("          PLEASE RE ENTER AGAIN !           \n");
            printf("                                            \n");
            printf("=======   == === = === ==  === === =========\n");
            continue;
        }
        break;
    }
}
/* function : how much is the ticket due to each artist */
int getPricePerTicket(int artist)
{
    int pricePerTicket = 0;
    switch(artist)
    {
    case 1: /*Big Ass concert : 1700 baht */
        pricePerTicket = 1700;
        break;
    case 2: /*Bodyslam concert : 2000 baht */
        pricePerTicket = 2000;
        break;
    case 3: /*Taylorswift concert : 3000 baht */
        pricePerTicket = 3000;
        break;
    case 4: /*Lady Gaga concert : 3500 baht */
        pricePerTicket = 3500;
        break;
    }
    return pricePerTicket;
}
/*function : calculate service fee due to number of ticket */
int calculateServiceFee(int number_of_Ticket)
{
    int serviceFee = 0;
    /*if the customer buy less than 5 ticket the service fee per ticket is 20 baht */
    if(number_of_Ticket <= 6)
    {
        serviceFee = 30;
    }
    /*if the customer buy more than 5 ticket the service fee per ticket is 10 baht */
    else
    {
        serviceFee = 40;
    }
    return serviceFee*number_of_Ticket;
}
/* function : print invoice for the customer including artist, number of ticket, date, price per ticket, service fees, total price, total amount */
void printInvoice(char name[] , char telnum[] , int number_of_Ticket[] , int artist[] , int whatTime , int day[] , int month[] , int year[])
{
    int i = 0;
    int j = whatTime;
    char listArtist[5][32] = {"Big Ass","Bodyslam","Taylor swift","Lady Gaga"};/*the artists' name*/
    int pricePerTicket = 0;/*how much each ticket due to artist */
    int total_Service_Fee = 0;/*total service fee due to the number of ticket*/
    int totalPrice = 0;/*total price of each concert that customer have to pay*/
    int totalAmount = 0; /*total amount money of every concert that customer have to pay*/

    printf("\t \t *                    INVOICE                     *\n");
    printf("\t \t Customer name : %s\n",name);
    printf("\t \t Customer telephone number : %s\n",telnum);
    for(whatTime = 0 ; whatTime < j ; whatTime++)
    {
        printf("\t \t **************************************************\n");
        printf("\t \t *                    CONCERT                     *\n");
        printf("\t \t **************************************************\n\n",i+1);
        printf("\t \t Artist              :   %s\n",listArtist[artist[whatTime]-1]);
        printf("\t \t Number of tickets   :   %d\n",number_of_Ticket[whatTime]);
        printf("\t \t Date                :   %d/%d/%d\n",day[whatTime],month[whatTime],year[whatTime]);
        pricePerTicket = getPricePerTicket(artist[whatTime]);
        printf("\t \t Price Per Ticket    :   %d BAHT\n",pricePerTicket);
        total_Service_Fee = calculateServiceFee(number_of_Ticket[whatTime]);
        printf("\t \t Service fees        :   %d BAHT\n",total_Service_Fee);
        totalPrice = (pricePerTicket * number_of_Ticket[whatTime]) + total_Service_Fee;
        printf("\t \t Total price         :   %d BAHT\n",totalPrice);
        totalAmount = totalAmount + totalPrice;
        i++;
    }
    printf("\t\t\t You have to pay :%d BAHT\n\n",totalAmount);
    printf("\t \t **************************************************\n");
    printf("\t \t *                 !! THANK YOU !!                *\n");
    printf("\t \t **************************************************\n");
}
/*This function will print the total income at the end of the day*/
void EndOfTheDay(int bigassTicketWholeDay, int bodyslamTicketWholeDay, int TaylorswiftTicketWholeDay , int ladygagaTicketWholeday , int total_Service_Fee)
{
    int grandTotalMoney = 0;
    int totalTicketMoneyEveryArtist = 0;
    printf("\t \t **************************************************\n");
    printf("\t \t *              ++  END OF THE DAY  ++            *\n");
    printf("\t \t **************************************************\n");
    printf("\t\t\t Total number of ticket sold for each artist\n");
    printf("\t\t Big Ass CONCERT        :  %d Tickets\n",bigassTicketWholeDay);
    printf("\t\t BODYSLAM CONCERT       :  %d Tickets\n",bodyslamTicketWholeDay);
    printf("\t\t Taylor Swift CONCERT   :  %d Tickets\n",TaylorswiftTicketWholeDay);
    printf("\t\t Lady Gaga CONCERT      :  %d Tickets\n",ladygagaTicketWholeday);

    printf("\t\t\t TOTAL TICKET INCOME FOR EACH ARTIST \n");
    printf("\t \t --------------NOT INCLUDING SERVICE FEE-----------\n");
    printf("\t\t Big Ass CONCERT            : %d Baht\n",bigassTicketWholeDay*1700);
    printf("\t\t Bodyslam CONCERT           : %d Baht\n",bodyslamTicketWholeDay*2000);
    printf("\t\t TaylorswiftCONCERT         : %d Baht\n",TaylorswiftTicketWholeDay*3000);
    printf("\t\t Lady Gaga CONCERT          : %d Baht\n",ladygagaTicketWholeday*3500);
    totalTicketMoneyEveryArtist = (bigassTicketWholeDay*1700)+(bodyslamTicketWholeDay*2000)+(TaylorswiftTicketWholeDay*3000)+(ladygagaTicketWholeday*3500);
    printf("\t\t TOTAL OF ALL SERVICE FEES  : %d BAHT\n",total_Service_Fee);
    grandTotalMoney = total_Service_Fee + totalTicketMoneyEveryArtist;
    printf("\t\t The total money were received : %d BAHT\n",grandTotalMoney);
}

int main()
{
    char stringinput[128];
    char Name[128];/*customer's name*/
    char telnum[36];/*customer's telephone number*/
    int artist[64];
    int number_of_Ticket[100];
    char date[100];
    int day[32],month[32],year[32];
    int whatTime = 0;/*count how many times that customer have buy the ticket*/
    int bodyslam_Ticket = 0;
    int taylorswift_Ticket = 0;
    int ladygaga_Ticket = 0;
    int bigass_Ticket = 0;
    int each_Service_Fee = 0;
    int total_Service_Fee = 0;
    while(strcmp(Name,"END") != 0)
    {
        memset(Name,0,sizeof(Name));
        /*welcome message */
        printf(" ------------------------------------------- \n");
        printf("|  Welcome to Concert ticket seller machine |                 \n");
        printf(" ------------------------------------------- \n");
        /*check name length cannot more than 40 characters and have to be alphabet*/

        while(strlen(Name)<=0 || strlen(Name) > 40 || !checkAlpha(Name))
        {
            printf("Please enter your Name :");
            fgets(stringinput,sizeof(stringinput),stdin);
            sscanf(stringinput,"%s",Name);
            rewind(stdin);
            if(strcmp(Name,"END")!=0)
            {
                if(strlen(Name)<=0 || strlen(Name) > 40 || !checkAlpha(Name))
                {
                    printf("=======___===__====__====__====__===========\n");
                    printf("======|___  |__|  |__|  |  |  |__|  |=======\n");
                    printf("      |___  |  %c  |  %c  |__|  |  %c  |       \n",92,92,92);
                    printf("          PLEASE RE ENTER AGAIN !           \n");
                    printf("                                            \n");
                    printf("=======   == === = === ==  === === =========\n");
                }
                whatTime=0;
            }
            else
            {
                EndOfTheDay(bigass_Ticket,bodyslam_Ticket,taylorswift_Ticket ,ladygaga_Ticket,total_Service_Fee);
                return 0;
            }
        }

        memset(telnum,0,sizeof(telnum));

        /*if customer enter 'END' the program will print the end of the day*/

        while(strlen(telnum)<8 || strlen(telnum)>14 || !checkDigit(telnum))
            /*phone number length must between 8-14*/
        {
            printf("Please enter your phone number :");
            fgets(stringinput,sizeof(stringinput),stdin);
            sscanf(stringinput,"%s",telnum);
            rewind(stdin);
            if(strlen(telnum)<8 || strlen(telnum)>14 || !checkDigit(telnum))
            {
                printf("=======___===__====__====__====__===========\n");
                printf("======|___  |__|  |__|  |  |  |__|  |=======\n");
                printf("      |___  |  %c  |  %c  |__|  |  %c  |       \n",92,92,92);
                printf("          PLEASE RE ENTER AGAIN !           \n");
                printf("                                            \n");
                printf("=======   == === = === ==  === === =========\n");
            }
        }

        while(1)
        {
            number_of_Ticket[whatTime] = askForNumberTicket();
            if(number_of_Ticket[whatTime] == 0)
            {
                printInvoice(Name ,telnum ,number_of_Ticket ,artist ,whatTime ,day ,month,year);
                break;
            }

            each_Service_Fee = calculateServiceFee(number_of_Ticket[whatTime]);
            total_Service_Fee = total_Service_Fee + each_Service_Fee;
            if(number_of_Ticket[whatTime] !=0)
            {
                artist[whatTime] = askForArtist();
                if(artist[whatTime] == 1)
                {
                    bigass_Ticket+=number_of_Ticket[whatTime];
                }
                else if(artist[whatTime] == 2)
                {
                    bodyslam_Ticket+=number_of_Ticket[whatTime];
                }
                else if(artist[whatTime] == 3)
                {
                    taylorswift_Ticket+=number_of_Ticket[whatTime];
                }
                else if(artist[whatTime] == 4)
                {
                    ladygaga_Ticket+=number_of_Ticket[whatTime];
                }
                askForDate(date);
                sscanf(date,"%d/%d/%d/",&day[whatTime],&month[whatTime],&year[whatTime]);
                whatTime++;
            }

        }

    }




}
