/****************************************************
 This is the program that call 'Concert Ticket Service'.
 The program start by ask for name and number.
 You can type END when you reach the end of the day.
 And then ask for number of ticket,concert and the date.
 When you want to print the invoice per person just type 0.

 Created by Pharewa  Phusuwankul Section C ID 58070503421

****************************************************/
#include <stdio.h>
#include <string.h>
#define MAXLEN 64

/** create function to get number of ticket  **/
int getNumberofticket()
    {
    char input[64];
    int numberTicket=-1;

    while(numberTicket<0)
         {
         printf("\n");
         printf("Type your amount ticket (type 0 when you want to stop) : ");
         fgets(input,sizeof(input),stdin);
         sscanf(input,"%d",&numberTicket);

         if(numberTicket<0)
           {
           printf("\n");
           printf("error! try again\n");
           }
         }
    return numberTicket;
    }

/** create function to get what concert do the user want **/
int getNumberofconcert()
    {
    char input[64];
    int artist=0;

    while(artist < 1 || artist > 4)
         {
         printf("Type the number of concert : \n");
         fgets(input,sizeof(input),stdin);
         sscanf(input,"%d",&artist);

         if(artist < 1 || artist > 4)
            {
            printf("\n");
            printf("error! it can be 1-4 only \n");
            }
         }
    return artist;
    }

/** create function to get the date from user **/
void getDate(char date[])
    {
    int checkday[]={0,31,28,31,30,31,30,31,31,30,31,30,31}; /** create array to keep the day in each month **/
    int day =0 ;
    int month =0;
    int year =0;
    int artist;

    while(1)
         {
         printf("What date is concert perform? (dd/mm/yy) : ");
         fgets(date,100,stdin);

         if(date[strlen(date)-1 == '\n'])
            date[strlen(date)-1] =0;
            sscanf(date,"%d/%d/%d",&day,&month,&year);

        /**if in that year , february has 29 days **/
        if(year%4 == 0)
          {
          checkday[2] = 29;
          }

        if(day <= 0 || day > checkday[month])
          {
          printf("ERROR\n");
          }
          else if(month <=0 || month >12)
                 {
                 printf("ERROR\n");
                 }
                 else if(year < 2015 || year > 2020)
                        {
                        printf("ERROR\n");
                        }
                        else
                           break;

        }
    }

/** create function to check number **/
int checkDigit(char input[64])
    {
    int p;

    for(p = 0 ; p < strlen(input) ; p++)
       {
       if(!isdigit(input[p]))
       return 0;
       }
    return 1;
    }

/** create function to check name **/
int checkAlphabet(char input[64])
    {
    int t;
    for(t = 0 ; t < strlen(input) ; t++)
        {
        if(!isalpha(input[t]))
        return 0;
        }
    return 1;
    }

int main()
    {
    char inputName [MAXLEN];
    char input[64];
    int i=0;
    char inputNumber [MAXLEN];
    int numberTicket[64];
    int artist[64];
    char artistName[4][20];
    int day[64];
    int month[64];
    int year[64];
    char date[64];
    int personTicket;
    int j=0;
    int priceofeachTicket[4];
    int totalFee=0;                     /** input totalfee data **/
    int totaleachpriceConcert=0;        /** input totaleachpriceconcert data**/
    int totalpricePerson=0;             /** input total price per person data**/
    int alldayBigass=0;                 /** input total of big ass ticket number each day data**/
    int alldayBodyslam=0;               /** input total of bodyslam ticket number each day data **/
    int alldayTaylor=0;                 /** input total of Taylor Swift ticket number each day data**/
    int alldayGaga=0;                   /** input total of Lady Gaga ticket number each day data**/
    int grandFee=0;                     /** input grandfee data**/
    int totalincomeforAll=0;            /** input totalincomeforAll data**/
    int grandTotal=0;                   /** input grandtotal data **/

    /** set the name of concert into number **/
    strcpy(artistName[0],"Big Ass");
    strcpy(artistName[1],"Bodyslam");
    strcpy(artistName[2],"Taylor Swift");
    strcpy(artistName[3],"Lady Gaga");

    for(i=0 ; ;i++)
       {
       memset(inputName,0,sizeof(inputName));
       while(strlen(inputName) <= 0 || strlen(inputName) > 40 || !checkAlphabet(inputName))
            {
            memset(inputName,0,sizeof(inputName));
            printf("\n");
            printf("=======================================================================\n");
            printf("                   Welcome to concert ticket service                   \n");
            printf("=======================================================================\n");
            printf("Enter your name (type END when you reach end of the day) : ");
            fgets(inputName,sizeof(inputName),stdin);
            sscanf(inputName,"%s",inputName);

            if(strlen(inputName) <= 0 || strlen(inputName) > 40 || !checkAlphabet(inputName))
              {
              printf("=====\n");
              printf("ERROR\n");
              printf("=====\n");
              }

            if(strncmp(inputName,"END",3) ==0)
              {
              break;
              }

            memset(inputNumber,0,sizeof(inputNumber));

            while(strlen(inputNumber) <5 || strlen(inputNumber) > 15 || !checkDigit(inputNumber))
                 {
                 printf("Enter your number : ");
                 fgets(inputNumber,sizeof(inputNumber),stdin);
                 sscanf(inputNumber,"%s",inputNumber);

                 if(strlen(inputNumber) <5 || strlen(inputNumber) > 15 || !checkDigit(inputNumber))
                   {
                   printf("=====\n");
                   printf("ERROR\n");
                   printf("=====\n");
                   }
                 }

            for(personTicket=0 ; ; personTicket++)
               {
               /** call the function to get number of ticket **/
               numberTicket[personTicket] = getNumberofticket();

               if( numberTicket[personTicket] == 0 )
                 {
                 break;
                 }

                 printf("=======================================\n");
                 printf("     What concert do you want ?        \n");
                 printf("1 - Big Ass        (1700 baht/ticket)\n");
                 printf("2 - Bodyslam       (2000 baht/ticket)\n");
                 printf("3 - Taylor Swift   (3000 baht/ticket)\n");
                 printf("4 - Lady Gaga      (3500 baht/ticket)\n");
                 printf("=======================================\n");

                 /** call the function to get waht concert **/
                 artist[personTicket] = getNumberofconcert();

               if (artist[personTicket] == 1)
                  {
                  alldayBigass += numberTicket[personTicket];
                  }
                  else if (artist[personTicket] == 2)
                          {
                          alldayBodyslam += numberTicket[personTicket];
                          }
                         else if (artist[personTicket] == 3)
                                 {
                                 alldayTaylor += numberTicket[personTicket];
                                 }
                                 else if (artist[personTicket] == 4)
                                         {
                                         alldayGaga += numberTicket[personTicket];
                                         }

              /** call the function to get the date **/
              getDate(date);
              sscanf(date,"%d/%d/%d",&day[personTicket],&month[personTicket],&year[personTicket]);
              }

            /** print invoice per person **/
            printf("=============================================\n");
            printf("                 THE INVOICE                 \n");
            printf("=============================================\n");
            printf("Name : %s\n",inputName);
            printf("Number : %s\n",inputNumber);
            printf("\n");

            for(j=0;j<personTicket;j++)
               {
               printf("\n");
               printf("Concert : %s\n",artistName[artist[j]-1]);
               printf("Number of ticket : %d\n",numberTicket[j]);
               printf("Date : %d/%d/%d\n",day[j],month[j],year[j]);

               if (artist[j]==1)
                  {
                  printf("Price per ticket : %d baht\n",1700);
                  }
                  else if (artist[j]==2)
                          {
                          printf("Price per ticket : %d baht\n",2000);
                          }
                          else if (artist[j]==3)
                                  {
                                  printf("Price per ticket : %d baht\n",3000);
                                  }
                                  else
                                     {
                                     printf("Price per ticket : %d baht\n",3500);
                                     }

               /** calculate for totalfee **/
               if (numberTicket[j]<=6)
                  {
                  totalFee = 30 * numberTicket[j];
                  }
                  else
                     {
                     totalFee = 40 * numberTicket[j];
                     }
               printf("\n");
               printf("service fee for this concert is : %d baht\n",totalFee);
               grandFee=grandFee+totalFee;

               if(artist[j]==1)
                 {
                 totaleachpriceConcert = (numberTicket[j]*1700) + totalFee;
                 }
                 else if (artist[j]==2)
                         {
                         totaleachpriceConcert = (numberTicket[j]*2000) + totalFee;
                         }
                         else if (artist[j]==3)
                                 {
                                 totaleachpriceConcert = (numberTicket[j]*3000) + totalFee;
                                 }
                                 else if (artist[j]==4)
                                         {
                                         totaleachpriceConcert = (numberTicket[j]*3500) + totalFee;
                                         }

                 printf("total price of this concert is : %d\n",totaleachpriceConcert);

                 totalpricePerson = totalpricePerson + totaleachpriceConcert;
                 totalincomeforAll = ((alldayBigass*1700)+(alldayBodyslam*2000)+(alldayTaylor*3000)+(alldayGaga*3500));
                 grandTotal = grandFee+totalincomeforAll;
                 }

                 printf("==========================================\n");
                 printf("Total price amount is : %d baht\n",totalpricePerson);
                 printf("==========================================\n");
                 totalpricePerson=0;
            }

                 if(strncmp(inputName,"END",3) ==0)
                    break;
            }

            /** print end of day report **/
            printf("\n");
            printf("******************************************************\n");
            printf("                    END OF DAY                        \n");
            printf("******************************************************\n");

            printf("\n");
            printf("Total ticket for each artist\n");
            printf("Big Ass : %d\n",alldayBigass);
            printf("Bodyslam : %d\n",alldayBodyslam);
            printf("Taylor Swift : %d\n",alldayTaylor);
            printf("Lady Gaga : %d\n",alldayGaga);

            printf("\n");
            printf("Total income for each artist (not including fees)\n");
            printf("Big Ass : %d\n",alldayBigass*1700);
            printf("Bodyslam : %d\n",alldayBodyslam*2000);
            printf("Taylor Swift : %d\n",alldayTaylor*3000);
            printf("Lady Gaga : %d\n",alldayGaga*3500);

            totalincomeforAll = ((alldayBigass*1700)+(alldayBodyslam*2000)+(alldayTaylor*3000)+(alldayGaga*3500));

            printf("\n");
            printf("grand fee : %d\n",grandFee);
            printf("grand total : %d\n",grandTotal);
            return 0;





    }





