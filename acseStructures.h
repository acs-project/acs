#define ATTR_COUNT 10
#define MAXLINE 5000
/**************************************************************
 * Structures using in acse project
 */
typedef struct _code
{
	int type;
	char *value;
	int fLine;
	int lLine;
	int count;
	struct _code *parent;
	struct _code *firstChild;
	struct _code *lastChild;
	struct _code *next;
} CODE_T;

typedef struct
{
	int type;						// attr value type
	int intVal[3];					// attr integer value
	char string[5];					// attr string value -> prefix, suffix
	char stringList[10][64];		// attr string list -> exceptions
} VALUE_T;

struct _rule;

typedef struct _attr
{
	char *name;															// attr name
	VALUE_T value;														// attr value
	VALUE_T (*validate)(int subtype, int valType, char value[]);		// attr's value validate function
	void (*check)(CODE_T *code, VALUE_T checkValue, struct _rule *rule);		// attr's enforcement function
	struct _attr *next;
} ATTR_T;

typedef struct _rule
{
	int id;												// rule's id
	char name[64];										// rule's name
	int type;											// rule's type
	int subtype;										// rule's subtype
	ATTR_T *attrHead;									// rule's attribute list head
	ATTR_T *attrTail;									// rule's attribute list tail
	struct _rule *next; 								// ptr to next rule in the rule list
} RULE_T;

typedef struct
{
	int type;
	char *subtype;
	int attrList[ATTR_COUNT];
} CSDL_LIST_T;

typedef struct
{
	RULE_T *rHead;
	RULE_T *rTail;
	RULE_T *rCurrent;
} RULE_LIST_T;

typedef struct _codeNode
{
	int flag;						// flag represent node 
	CODE_T *data;					// 
	struct _codeNode *next;
}CODE_NODE_T;

typedef struct _flag
{
    int flag;
    struct _flag *next;
}FLAG_T;

typedef struct _list
{
    char *string;
    struct _list *next;
}LIST_T;

typedef struct
{
	LIST_T *listHead;
	LIST_T *listCurrent;
	LIST_T *listTail;
} NAME_LIST_T;

typedef struct _report
{
	int lineNum;
	char *code;
	int violatedNum;
	char *violatedName;
	struct _report *next;
}REPORT_T;

typedef struct
{
	REPORT_T *reportHead;
	REPORT_T *reportTail;
	REPORT_T *reportCurrent;
}REPORT_LIST_T;