typedef enum 
{
  define_declaration=1000, define_constant, include, primary_expression,
  constant, enumeration_constant, string, generic_selection,
  generic_assoc_list, generic_association, postfix_expression,
  argument_expression_list, unary_expression, unary_operator,
  cast_expression, multiplicative_expression, additive_expression,
  shift_expression, relational_expression, equality_expression,
  and_expression, exclusive_or_expression, inclusive_or_expression,
  logical_and_expression, logical_or_expression,
  conditional_expression, assignment_expression, assignment_operator,
  expression, constant_expression, declaration,
  declaration_specifiers, init_declarator_list, init_declarator,
  storage_class_specifier, type_specifier, typedef_specifier,
  struct_or_union_specifier, struct_or_union,
  struct_declaration_list, struct_declaration,
  specifier_qualifier_list, struct_declarator_list,
  struct_declarator, enum_specifier, enumerator_list, enumerator,
  atomic_type_specifier, type_qualifier, function_specifier,
  alignment_specifier, declarator, direct_declarator, pointer,
  type_qualifier_list, parameter_type_list, parameter_list,
  parameter_declaration, identifier_list, type_name,
  abstract_declarator, direct_abstract_declarator, initializer,
  initializer_list, designation, designator_list, designator,
  static_assert_declaration, statement, labeled_statement,
  compound_statement, block_item_list, block_item,
  expression_statement, selection_statement, iteration_statement,
  jump_statement, translation_unit, external_declaration,
  function_definition, declaration_list
} YACC_GRAMMAR;

typedef enum
{
	SLASH=974,		/* \ */
	B_BSLASH,  		// //
	SEMICOMM,   	// ;
	COMM,       	// ,
	COL,			// :
	FULL_STOP,		// .
	Q_MARK,			// ?
	HASH,			// #
	D_QUOTE			// "
}PUNC_MARKS;

typedef enum
{
	PROGRAM=935,
	B_COMMENT,			// Block comment
	SL_COMMENT,			// Single-line comment
	DESIGNATOR,
	ENUMERATOR,
	GENERIC_ASSOC,
	INITIALIZER,
	PARAMETER,
	ALIGNMENT,
	DEFINE_STMT,
	INCLUDE_STMT,
	FUNC_DEF_STMT,
	DECL_STMT,
	STA_ASS_DECL, 		// Static assert declaration
	FOR_STMT,
	WHILE_STMT,
	DO_WHILE_STMT,
	SWITCH_STMT,
	IF_STMT,
	LABEL_STMT,			// Labeled statement
	COMP_STMT,			// Compound statement
	JUMP_STMT,			// Jump statement i.e. RETURN, BREAK
	EXPR_STMT,			// Expression statement
	FUNC_PROTO,			// Function prototype
	MEM_ACS_EXPR,		// Member-Access
	FUNC_CALL_EXPR,		// Function-call Expression
	TERN_COND_EXPR,		// Ternary condition expression
	ARTH_EXPR,			// Arithematic Expression
	LOG_EXPR,			// Logical Expression
	COMP_EXPR,			// Comparison Expression
	INC_DEC_EXPR,		// Increment Decrement Expression
	ASS_EXPR,			// Assignment statement
	BIT_EXPR,			// Bit expression
	PRI_EXPR,			// Primary expression
	CAST_EXPR,			// Cast expression
	UNARY_EXPR,
	TYPE_SPEC,			// Type specifier
	TYPE_QUAL,   		// Type qualifier
	INIT_DECL
}BLOCKS;

typedef enum 
{
	ARITH_OP_1 = 994,	// + - * /
	ARITH_OP_2,			// ++ --
	REL_OP,				// == != <= >=
	REL_OP_1 = 992,		// <
	REL_OP_2 = 993,		// >
	LOGIC_OP = 997,		// && || !
	BIT_OP,				// & | ^ ~ << >>
	ASS_OP				// = += -= *= /= %= <<= >>= &= ^= |=
}OPERATORS;

typedef enum 
{
	OP_PAR_BRACKET = 986,
	ED_PAR_BRACKET,
	OP_SQ_BRACKET,
	ED_SQ_BRACKET,
	OP_CUR_BRACKET,
	ED_CUR_BRACKET,
	OP_ANG_BRACKET = 992,
	ED_ANG_BRACKET = 993
}BRACKETS;

typedef enum 
{
	SPACE=983,
	TAB,
	NEWLINE
}WHITESPACES;

//List of typedef and struct variable name
typedef struct _list
    {
    char *string;
    struct _list *next;
    }LIST_T;

typedef struct
{
	LIST_T *listHead;
	LIST_T *listCurrent;
	LIST_T *listTail;
} NAME_LIST_T;

typedef struct _code
{
	int type;
	char *value;
	int fLine;
	int lLine;
	int count;
	struct _code *parent;
	struct _code *firstChild;
	struct _code *lastChild;
	struct _code *next;
} CODE_T;

typedef struct _codeTree
{
    CODE_T *head;
    CODE_T *current;
    CODE_T *tail;
} CODE_TREE;

typedef struct _flag
{
    int flag;
    struct _flag *next;
}FLAG_T;

typedef struct _codeNode
{
	int flag;						// flag represent node 
	CODE_T *data;
	struct _codeNode *next;
}CODE_NODE_T;

void typedefAndStructHandle(int choice,char* data);
int searchList(LIST_T* listHead,char* searchData);
int structListInit();
int typedefListInit();
int structlistDestroy();
int typedeflistDestroy();
int structListInsert(char* insertData);
int typedefListInsert(char* insertData);
LIST_T* getTypedefHead();
LIST_T* getStructHead();
void printStructList();
void printTypedefList();
int getLine();
char* mystrdup(const char* data);
int initCodeTree();
void codeTreeDestroy();
int insertToken(int flag, char *token);
void testPrintCode();

int pushChild(int line,int type,int flag,char* value);
int pushParent(CODE_NODE_T *parent);
int pushFlag(int flag);
CODE_NODE_T *popCode();
int popFlag();
int combine(int acseFlag, int yaccFlag);
int flagTransform(int oldFlag, int newFlag);
void codeStackClear();
void flagStackClear();
void printStack();
void printProgram(CODE_T *node);
CODE_T *getStackHeadData();
int pack();

int hasToken;
char *currentToken;
/*
int pushChild(int flag, char *value);
int pushParent(CODE_T *parent);
int pushFlag(int flag);
CODE_T *popCode();
int popFlag();
int combine(int flag);*/
