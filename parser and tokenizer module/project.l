%e  1019
%p  2807
%n  371
%k  284
%a  1213
%o  1117

SQ   \'
DQ   \"
O   [0-7] 
D   [0-9]
NZ  [1-9]
L   [a-zA-Z_]
A   [a-zA-Z_0-9]
H   [a-fA-F0-9]
SC  ~|`|!|@|\#|\$|%|\^|\&|\*|\(|\)|_|-|\+|=|\"|\?|\|\[|\]|\{|\}|\*|:|;
HP  (0[xX])
E   ([Ee][+-]?{D}+)
P   ([Pp][+-]?{D}+)
FS  (f|F|l|L)
IS  (((u|U)(l|L|ll|LL)?)|((l|L|ll|LL)(u|U)?))
CP  (u|U|L)
SP  (u8|u|U|L)
ES  (\\(['"\?\\abfnrtv]|[0-7]{1,3}|x[a-fA-F0-9]+))
WS  [ \v\f]
TAB [\t]

%{
    #include <stdio.h>
    #include <string.h>
    #include <ctype.h>
    #include "y.tab.h"
    #include "LexYacc.h"

    static int bInComment=0;
    static int bOfTypedef=0;
    static int bOfStruct=0;
    static int bOfType=0;
    static int bInStruct=0;
    int line = 1;


    int yywrap(void);
    char *comment(void);
    static int check_type(void);
    int sym_type(char* token);
    char* mystrdup(const char* data);
    
%}

%%


"/*"                    { 
							int blockFirstLine = line;
							char *blockMsg = comment();
							pushChild(blockFirstLine,B_COMMENT,B_COMMENT,blockMsg);
							CODE_T *tmp = getStackHeadData();
							if(tmp->type != B_COMMENT)
								printf("NOT BLOCK COMMENT\n");
							else
								tmp->lLine = line;
						}
"//".*                  { 
							pushChild(line,SL_COMMENT,SL_COMMENT,yytext);
						  	//printf("yytext: %s\n",yytext);
						}

"include"               { 
							pushChild(line,INCLUDE,INCLUDE,yytext);
						  	return(INCLUDE);
						}
"define"                { 
							pushChild(line,DEFINE,DEFINE,yytext);
						  	return(DEFINE);
						}
"void"					{   //spec
                          	bOfType=1;
                          	pushChild(line,TYPE_SPEC,TYPE_SPEC,yytext);
                          	return(VOID);
                        }
"int"					{
							//spec
                          	bOfType=1;
                          	pushChild(line,TYPE_SPEC,TYPE_SPEC,yytext);
                          	return(INT);
                        } /* Data Type*/
"long"					{
							//spec
                          	bOfType=1;
                          	pushChild(line,TYPE_SPEC,TYPE_SPEC,yytext);
                          	return(LONG);
                        }
"char"					{
							//spec
                          	bOfType=1;
                          	pushChild(line,TYPE_SPEC,TYPE_SPEC,yytext);
                          	return(CHAR);
                        }
"short"					{
							//spec
                          	bOfType=1;
                          	pushChild(line,TYPE_SPEC,TYPE_SPEC,yytext);
                          	return(SHORT);
                        }
"signed"				{
							//spec
                          	bOfType=1;
                          	pushChild(line,TYPE_SPEC,TYPE_SPEC,yytext);
                          	return(SIGNED);
                        }
"double"				{
							//spec
                          	bOfType=1;
                          	pushChild(line,TYPE_SPEC,TYPE_SPEC,yytext);
                          	return(DOUBLE);
                        }
"unsigned"				{
							//spec
                          	bOfType=1;
                          	pushChild(line,TYPE_SPEC,TYPE_SPEC,yytext);
                          	return(UNSIGNED);
                        }
"float"					{
							//spec
                          	bOfType=1;
                          	pushChild(line,TYPE_SPEC,TYPE_SPEC,yytext);
                          	return(FLOAT);
                        }
"auto"					{ 
						  	pushChild(line,AUTO,AUTO,yytext);
						  	return(AUTO); 
						} 
"extern"				{ 
						  	pushChild(line,EXTERN,EXTERN,yytext);
						  	return(EXTERN); 
						}
"register"				{ 
						  	pushChild(line,REGISTER,REGISTER,yytext);
						  	return(REGISTER); 
						}
"static"				{ 
						  	pushChild(line,STATIC,STATIC,yytext);
						  	return(STATIC); 
						}

"const"					{ 
							// type Qualifier
						  	pushChild(line,TYPE_QUAL,TYPE_QUAL,yytext);
						  	return(CONST); 
						} 
"restrict"				{ 
							// type Qualifier
						  	pushChild(line,TYPE_QUAL,TYPE_QUAL,yytext);
						  	return(RESTRICT); 
						}
"volatile"				{ 
							// type Qualifier
						  	pushChild(line,TYPE_QUAL,TYPE_QUAL,yytext);
						  	return(VOLATILE); 
						}
"_Atomic"               { 

						  	pushChild(line,TYPE_SPEC,TYPE_SPEC,yytext);
						  	return ATOMIC; 
						}
"break"					{ 
						  	pushChild(line,BREAK,BREAK,yytext);
						  	return(BREAK); 
						}
"case"					{ 
						  	pushChild(line,CASE,CASE,yytext);
						  	return(CASE); 
						}
"continue"				{ 
						  	pushChild(line,CONTINUE,CONTINUE,yytext);
						  	return(CONTINUE); 
						}
"default"				{ 
						  	pushChild(line,DEFAULT,DEFAULT,yytext);
						  	return(DEFAULT); 
						}
"do"					{ 
						  	pushChild(line,DO,DO,yytext);
						  	return(DO); 
						}
"else"					{ 
						  	pushChild(line,ELSE,ELSE,yytext);
						  	return(ELSE); 
						}
"enum"					{ 
						  	pushChild(line,TYPE_SPEC,TYPE_SPEC,yytext);
						  	return(ENUM); 
						}
"for"					{ 
						  	pushChild(line,FOR,FOR,yytext);
						  	return(FOR); 
						}
"goto"					{ 
						  	pushChild(line,GOTO,GOTO,yytext);
						  	return(GOTO); 
						}
"if"					{ 
						  	pushChild(line,IF,IF,yytext);
						  	return(IF); 
						}
"inline"				{ 
						  	pushChild(line,INLINE,INLINE,yytext);
						  	return(INLINE); 
						}
"return"				{ 
						  	pushChild(line,RETURN,RETURN,yytext);
						  	return(RETURN); 
						}
"sizeof"				{
						  	pushChild(line,SIZEOF,SIZEOF,yytext);
						  	return(SIZEOF); 
						}
"struct"				{
                          	bOfStruct=1;
                          	bInStruct=1;
                          	pushChild(line,TYPE_SPEC,TYPE_SPEC,yytext);
                          	return(STRUCT);
                        }
"switch"				{ 
						  	pushChild(line,SWITCH,SWITCH,yytext);
						  	return(SWITCH); 
						}
"typedef"				{
                          	bOfTypedef=1;
                          	pushChild(line,TYPEDEF,TYPEDEF,yytext);
                          	return(TYPEDEF);
                        }
"union"					{ 
						  	pushChild(line,TYPE_SPEC,TYPE_SPEC,yytext);
						  	return(UNION); 
						}
"while"					{ 
						  	pushChild(line,WHILE,WHILE,yytext);
						  	return(WHILE); 
						}
"_Alignas"              { 
						  	pushChild(line,ALIGNAS,ALIGNAS,yytext);
						  	return ALIGNAS; 
						}
"_Alignof"              { 
						  	pushChild(line,ALIGNOF,ALIGNOF,yytext);
						  	return ALIGNOF; 
						}
"_Bool"                 { 
							//spec
						  	pushChild(line,TYPE_SPEC,TYPE_SPEC,yytext);
						  	return BOOL; 
						}
"_Complex"              { 
							//spec
						  	pushChild(line,TYPE_SPEC,TYPE_SPEC,yytext);
						  	return COMPLEX; 
						}
"_Generic"              { 
						  	pushChild(line,GENERIC,GENERIC,yytext);
						  	return GENERIC; 
						}
"_Imaginary"            { 
							//spec
						  	pushChild(line,TYPE_SPEC,TYPE_SPEC,yytext);
						  	return IMAGINARY; 
						}
"_Noreturn"             { 
						  	pushChild(line,NORETURN,NORETURN,yytext);
						  	return NORETURN; 
						}
"_Static_assert"        { 
						  	pushChild(line,STATIC_ASSERT,STATIC_ASSERT,yytext);
						  	return STATIC_ASSERT; 
						}
"_Thread_local"         { 
						  	pushChild(line,THREAD_LOCAL,THREAD_LOCAL,yytext);
						  	return THREAD_LOCAL; 
						}
"__func__"              { 
						  	pushChild(line,FUNC_NAME,FUNC_NAME,yytext);
						  	return FUNC_NAME; 
						}
"..."					{ 
							pushChild(line,ELLIPSIS,ELLIPSIS,yytext);
							return ELLIPSIS; 
						}
">>="					{ 
							pushChild(line,ASS_OP,RIGHT_ASSIGN,yytext);
						  	return RIGHT_ASSIGN; 
						}
"<<="					{ 
							pushChild(line,ASS_OP,LEFT_ASSIGN,yytext);
						  	return LEFT_ASSIGN; 
						}
"+="					{ 
							pushChild(line,ASS_OP,ADD_ASSIGN,yytext);
						  	return ADD_ASSIGN; 
						}
"-="					{ 
							pushChild(line,ASS_OP,SUB_ASSIGN,yytext);
						  	return SUB_ASSIGN; 
						}
"*="					{ 
							pushChild(line,ASS_OP,MUL_ASSIGN,yytext);
						  	return MUL_ASSIGN; 
						}
"/="					{ 
							pushChild(line,ASS_OP,DIV_ASSIGN,yytext);
						  	return DIV_ASSIGN; 
						}
"%="					{ 
							pushChild(line,ASS_OP,MOD_ASSIGN,yytext);
						  	return MOD_ASSIGN; 
						}
"&="					{ 
							pushChild(line,ASS_OP,AND_ASSIGN,yytext);
						  	return AND_ASSIGN; 
						}
"^="					{ 
							pushChild(line,ASS_OP,XOR_ASSIGN,yytext);
						  	return XOR_ASSIGN; 
						}
"|="					{ 
							pushChild(line,ASS_OP,OR_ASSIGN,yytext);
						  	return OR_ASSIGN; 
						}
">>"					{ 
							pushChild(line,BIT_OP,RIGHT_OP,yytext);
						  	return RIGHT_OP; 
						}
"<<"					{ 
							pushChild(line,BIT_OP,LEFT_OP,yytext); 
						  	return LEFT_OP; 
						}
"++"					{ 
							pushChild(line,ARITH_OP_2,INC_OP,yytext);
						 	return INC_OP; 
						}
"--"					{ 
							pushChild(line,ARITH_OP_2,DEC_OP,yytext);
						 	return DEC_OP; 
						}
"->"					{ 
							pushChild(line,PTR_OP,PTR_OP,yytext);
						 	return PTR_OP; 
						}
"&&"					{ 
							pushChild(line,LOGIC_OP,AND_OP,yytext);
						 	return AND_OP; 
						}
"||"					{ 
							pushChild(line,LOGIC_OP,OR_OP,yytext);
						 	return OR_OP; 
						}
"<="					{ 
							pushChild(line,REL_OP,LE_OP,yytext);
						  	return LE_OP; 
						}
">="					{
						  	pushChild(line,REL_OP,GE_OP,yytext);  
						  	return GE_OP; 
						}
"=="					{ 
						  	pushChild(line,REL_OP,EQ_OP,yytext);
						  	return EQ_OP; 
						}
"!="					{ 
						  	pushChild(line,REL_OP,NE_OP,yytext);
						  	return NE_OP; 
						}
";"                     { 
							pushChild(line,SEMICOMM,SEMICOMM,yytext);
						  	return ';'; 
						}
("{"|"<%")				{ 
							pushChild(line,OP_CUR_BRACKET,OP_CUR_BRACKET,yytext);
						  	return '{'; 
						}
("}"|"%>")				{
						  	pushChild(line,ED_CUR_BRACKET,ED_CUR_BRACKET,yytext);
                          	bInStruct=0;
                          	return '}';
                        }
","                     { 
							pushChild(line,COMM,COMM,yytext);
						  	return ','; 
						}
":"                     { 
							pushChild(line,COL,COL,yytext);
						  	return ':'; 
						}
"="                     { 
							pushChild(line,ASS_OP,ASS_OP,yytext);
						  	return '='; 
						}
"("                     { 
							pushChild(line,OP_PAR_BRACKET,OP_PAR_BRACKET,yytext);
						  	return '('; 
						}
")"                     { 
							pushChild(line,ED_PAR_BRACKET,ED_PAR_BRACKET,yytext);
						  	return ')'; 
						}
("["|"<:")				{ 
							pushChild(line,OP_SQ_BRACKET,OP_SQ_BRACKET,yytext);
						  	return '['; 
						}
("]"|":>")				{ 
							pushChild(line,ED_SQ_BRACKET,ED_SQ_BRACKET,yytext);
						  	return ']'; 
						}
"."                     { 
							pushChild(line,FULL_STOP,FULL_STOP,yytext);
						  	return '.'; 
						}
"&"                     { 
							pushChild(line,BIT_OP,BIT_OP,yytext);
						  	return '&'; 
						}
"!"                     { 
							pushChild(line,LOGIC_OP,LOGIC_OP,yytext);
						  	return '!'; 
						}
"~"                     { 
							pushChild(line,BIT_OP,BIT_OP,yytext);
						  	return '~'; 
						}
"-"                     { 
							pushChild(line,ARITH_OP_1,ARITH_OP_1,yytext);
						  	return '-'; 
						}
"+"                     { 
						  	pushChild(line,ARITH_OP_1,ARITH_OP_1,yytext); 
						  	return '+'; 
						}
"*"                     { 
						  	pushChild(line,ARITH_OP_1,ARITH_OP_1,yytext);
						  	return '*'; 
						}
"/"                     { 
						  	pushChild(line,ARITH_OP_1,ARITH_OP_1,yytext);
						  	return '/'; 
						}
"%"                     { 
						  	pushChild(line,ARITH_OP_1,ARITH_OP_1,yytext);
						  	return '%'; 
						}
"<"                     { 
							pushChild(line,REL_OP_1,REL_OP_1,yytext);
						  	return '<'; 
						}
">"                     { 
						  	pushChild(line,REL_OP_2,REL_OP_2,yytext);
						  	return '>'; 
						}
"^"                     { 
							pushChild(line,BIT_OP,BIT_OP,yytext);	
						  	return '^'; 
						}
"|"                     { 
							pushChild(line,BIT_OP,BIT_OP,yytext);
						  	return '|'; 
						}
"?"                     { 
							pushChild(line,Q_MARK,Q_MARK,yytext);
						  	return '?'; 
						}
"#"                     { 
							pushChild(line,HASH,HASH,yytext);
						  	return '#'; 
						}
{DQ}                    { 
							pushChild(line,D_QUOTE,D_QUOTE,yytext);
						  	return '"'; 
						}

{L}{A}+".h"             { 
						  	pushChild(line,LIBRARY,LIBRARY,yytext);
						  	return LIBRARY; 
						}
{L}{A}*					{ 	
							return check_type(); 
						}

({SP}?\"([^"\\\n]|{ES})*\"{WS}*)+	{ 
									  	pushChild(line,STRING_LITERAL,STRING_LITERAL,yytext);
									  	return STRING_LITERAL; 
									}

{HP}{H}+{IS}?					{	
							      	pushChild(line,I_CONSTANT,I_CONSTANT,yytext);
									return I_CONSTANT; 
								}
{NZ}{D}*{IS}?					{	
								  	pushChild(line,I_CONSTANT,I_CONSTANT,yytext);
								  	return I_CONSTANT; 
								}
"0"{O}*{IS}?					{	
								  	pushChild(line,I_CONSTANT,I_CONSTANT,yytext);
								  	return I_CONSTANT; 
								}

{CP}?"'"([^'\\\n]|{ES})"'"		{	
								  	pushChild(line,CHAR_LITERAL,CHAR_LITERAL,yytext);
								  	return CHAR_LITERAL; 
								}

{D}+{E}{FS}?				{ 
							  	pushChild(line,F_CONSTANT,F_CONSTANT,yytext); 
							  	return F_CONSTANT; 
							}
{D}*"."{D}+{E}?{FS}?		{ 
							  	pushChild(line,F_CONSTANT,F_CONSTANT,yytext); 
							  	return F_CONSTANT; 
							}
{D}+"."{E}?{FS}?			{ 
							  	pushChild(line,F_CONSTANT,F_CONSTANT,yytext); 
							  	return F_CONSTANT; 
							}
{HP}{H}+{P}{FS}?			{ 
							  	pushChild(line,F_CONSTANT,F_CONSTANT,yytext); 
							  	return F_CONSTANT; 
							}
{HP}{H}*"."{H}+{P}{FS}?		{ 
							  	pushChild(line,F_CONSTANT,F_CONSTANT,yytext);  
							  	return 	F_CONSTANT; 
							}
{HP}{H}+"."{P}{FS}?			{ 
							  	pushChild(line,F_CONSTANT,F_CONSTANT,yytext); 
							  	return F_CONSTANT; 
							}

\n                      { 
						  	printf("This is end of line\n"); 
						  	pushChild(line,NEWLINE,NEWLINE,yytext); 
						  	line++;  
						} /* Add the token directly to the structure */
{TAB}+                  { 
						  	printf("This is tab %d\n", strlen(yytext) );
						  	pushChild(line,TAB,TAB,yytext); 
						} /* Add the token directly to the structure */


{WS}+					{/* whitespace separates tokens */ 
						  	printf("SPACES : %d\n", strlen(yytext));
						  	pushChild(line,SPACE,SPACE,yytext); 
						} /* Add the token directly to the structure*/

.					{ /* discard bad characters */ }

%%

int yywrap(void)        /* called at end of input */
    {
    if (bInComment)
    yyerror("unterminated comment");
    return 1;           /* terminate now */
    }

char *comment(void)
{
	char *msg = NULL;
	int count = strlen(yytext) + 1;
	int c1 = 0;
	int c2 = input();

	msg = (char *) realloc (msg, count * sizeof(char));
	strcpy(msg,yytext);
	while(1)
	{
		if (c2 == '\n')
			line++;
		if(c2 == EOF)
			break;
		else if(c1 == '*' && c2 == '/')
		{
			count = count + 2;
			msg = (char *) realloc (msg, count * sizeof(char));
			msg[count-2] = '/';
			msg[count-3] = '*';
			break;
		}
		else
		{
			count++;
			msg = (char *) realloc (msg, count * sizeof(char));
			msg[count-2] = c2;
		}
		c1 = c2;
		c2 = input();
	}
	msg[count-1] = '\0';
	// printf("Here is our comment\n");
	// printf("%s\n\n", msg);
	return msg;
}


static int check_type(void)
    {
    switch (sym_type(yytext))
        {
        case TYPEDEF_NAME:                /* previously defined */
            {
            printf("TYPEDEF_NAME\n");
            pushChild(line,TYPEDEF_NAME,TYPEDEF_NAME,yytext); 
            return TYPEDEF_NAME;
            }
        case ENUMERATION_CONSTANT:        /* previously defined */
            {
            printf("ENUMERATION\n");
            pushChild(line,ENUMERATION_CONSTANT,ENUMERATION_CONSTANT,yytext); 
            return ENUMERATION_CONSTANT;
            }
        case STRUCT_NAME:
            {
            printf("STRUCT_NAME\n");
            yypush_buffer_state(yy_create_buffer( yyin, YY_BUF_SIZE ));
            pushChild(line,STRUCT_NAME,STRUCT_NAME,yytext); 
            return STRUCT_NAME;
            }
        default:    /* includes undefined */ /*This is when do normal declaration such as int a=3; in this case it return a*/
            {
            if(searchList(getTypedefHead(),yytext)==1)
                {
                pushChild(line,TYPEDEF_NAME,TYPEDEF_NAME,yytext); 
                return TYPEDEF_NAME;
                }
            else
                {
                pushChild(line,IDENTIFIER,IDENTIFIER,yytext); 
                return IDENTIFIER;
                }
            }
        }
    }

int sym_type(char* token)
    {
    if(bOfStruct)
        {
        bOfStruct=0;
        if(searchList(getStructHead(),yytext) != 1)
            {
            typedefAndStructHandle(1,yytext);
            }
        return IDENTIFIER;
        }
    else if(bOfType && bInStruct)
        {
        bOfType=0;
        return IDENTIFIER;
        }
    else if(bOfTypedef && (!bInStruct))
        {
        printf("eiei\n");
        bOfType=0;
        bOfTypedef=0;
        if(searchList(getTypedefHead(),yytext) != 1)
            {
            typedefAndStructHandle(2,yytext);
            }
        return TYPEDEF_NAME;
        }
    else if(bInStruct)
        {
        bOfType=0;
        return IDENTIFIER;
        }
    return -1;
    }


char* mystrdup(const char* data)
    {
    char* d=malloc(strlen(data)+1);
    if(d==NULL)
        return NULL;
    strcpy(d,data);
    return d;
    }

int getLine()
{
	return line;
}





