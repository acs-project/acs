//  listProject.c
//  Code
//  To compile the code do as the same when compiling lex and Yacc file
//  Finally compile with: gcc -o main main.c y.tab.c lex.yy.c -ly -ll
//  Created by Siva on 4/8/2557 BE.
//  Copyright (c) 2557 Siva. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

NAME_LIST_T* structList = NULL;
NAME_LIST_T* typedefList = NULL;
CODE_T *blockParent;
CODE_T *lineParent;
CODE_TREE *codeTree = NULL;
CODE_NODE_T *codeStackHead = NULL;
FLAG_T *flagStackHead = NULL;


void typedefAndStructHandle(int choice,char* data)
    {
    if(choice==1)
        {
        if(structListInsert(data)==-1)
            {
            printf("The StructList doesn't exist, the program will terminate immediately\n");
            exit(0);
            }
        }
    else if(choice==2)
        {
        if(typedefListInsert(data)==-1)
            {
            printf("The typedefList doesn't exist, the program will terminate immediately\n");
            exit(0);
            }
        }
    }

int searchList(LIST_T* listHead, char* searchData)
	{
	LIST_T *search = listHead;
    while(search != NULL)
    	{
        if(strcmp(search->string, searchData) == 0)
        	return 1;
        search = search->next;
        }
    printf("IDENTIFIER: \"%s\"\n", searchData);
    return 2;
	}

int structListInit()
    {
    if(structList != NULL)
        {
        structlistDestroy();
        }
    else
        {
        structList = (NAME_LIST_T*) calloc(1,sizeof(NAME_LIST_T));
        if(structList==NULL)
            {
            return -1;
            }
        else
            {
            return 1;
            }
        }
    }

int structlistDestroy()
    {
    LIST_T* destroy = structList->listHead;
    if(destroy == NULL)
        {
        return -1;
        }
    else
        {
        while(destroy != NULL)
            {
            LIST_T *next = destroy->next;
            free(destroy);
            destroy = next;
            }
        free(structList);
        structList = NULL;
        return 1;
        }
    }

int typedefListInit()
    {
    if(typedefList != NULL)
        {
        typedeflistDestroy();
        }
    else
        {
        typedefList = (NAME_LIST_T*) calloc(1,sizeof(NAME_LIST_T));
        if(typedefList == NULL)
            {
            return -1;
            }
        else
            {
            return 1;
            }
        }
    }

int typedeflistDestroy()
    {
    LIST_T* destroy = typedefList->listHead;
    if(destroy==NULL)
        {
        return -1;
        }
    else
        {
        while(destroy!=NULL)
            {
            LIST_T *next = destroy->next;
            free(destroy);
            destroy = next;
            }
        free(typedefList);
        typedefList = NULL;
        return 1;
        }
    }

int structListInsert(char* insertData)
    {
    if(structList == NULL)
        {
        return -1;
        }
    else
        {
        LIST_T *tail = structList->listTail;
        LIST_T *new = (LIST_T*) calloc(1, sizeof(LIST_T));
        if(new != NULL)
        {
            new->string = mystrdup(insertData);
            if(new->string != NULL)
            {
                if(tail == NULL)
                {
                    structList->listHead = new;
                }
                else
                {
                    tail->next = new;
                }
                structList->listTail = new;
                structList->listCurrent = new;
                return 1;
            }
        }
        return -1;
        }
    }

int typedefListInsert(char* insertData)
    {
    if(typedefList == NULL)
        {
        return -1;
        }
    else
        {
        LIST_T *tail = typedefList->listTail;
        LIST_T *new = (LIST_T*) calloc(1, sizeof(LIST_T));
        if(new != NULL)
        {
            new->string = mystrdup(insertData);
            if(new->string != NULL)
            {
                if(tail == NULL)
                {
                    typedefList->listHead = new;
                }
                else
                {
                    tail->next = new;
                }
                typedefList->listTail = new;
                typedefList->listCurrent = new;
                return 1;
            }
        }
        return -1;
        }
    }

LIST_T* getTypedefHead()
{
    return typedefList->listHead;
}

LIST_T* getStructHead()
{
    return structList->listHead;
}

void printStructList()
{
    LIST_T *current = structList->listHead;
    while(current != NULL)
    {
        printf("Struct Data : %s\n", current->string);
        current = current->next;
    }
}

void printTypedefList()
{
    LIST_T *current = typedefList->listHead;
    while(current != NULL)
    {
        printf("Typedef Data : %s\n", current->string);
        current = current->next;
    }
}

int initCodeTree()
{
    int retVal = 0;
    if(codeTree != NULL)
        codeTreeDestroy();
    codeTree = (CODE_TREE*) calloc(1, sizeof(CODE_TREE));
    if(codeTree == NULL)
        retVal = -1;
    return retVal;
}

void codeTreeDestroy()
{
    if (codeTree != NULL)
    {
        CODE_T *item = codeTree->head;
        while(item != NULL)
        {
            CODE_T *next = item->next;
            free(item);
            item = next;
        }
        free(codeTree);
        codeTree = NULL;
    }
}

int insertToken(int flag, char *token)
{
    if(codeTree == NULL)
        return -1;
    else
    {
        CODE_T *tail = codeTree->tail;
        CODE_T *newNode = (CODE_T*) calloc(1, sizeof(CODE_T));
        if(newNode == NULL)
            return -1;
        else
        {
            newNode->type = flag;
            if((token != NULL) && (strlen(token) != 0))
                newNode->value = mystrdup(token);
            else
                return -1;
        }
        
        if(tail == NULL)
            codeTree->head = newNode;
        else
            tail->next = newNode;
        codeTree->tail = newNode;
        codeTree->current = newNode;
        
    }
    return 0;
}

void testPrintCode()
{
    CODE_T *currentCode = codeTree->head;
    int i = 0;

    printf("\nTEST PRINT\n");
    while(currentCode != NULL)
    {
        printf("==============\n");
        printf("CODE TYPE : %d\n", currentCode->type);
        printf("CODE VALUE : \"%s\"\n", currentCode->value);
        printf("==============\n\n");

        currentCode = currentCode->next;
    }
}

///////////////////////////////////////////// 658
int isException(int flag)
{
    if((flag == SPACE) || (flag == TAB) || (flag == NEWLINE) || 
        (flag == B_COMMENT) || (flag == SL_COMMENT))
        return 1;
    return 0;
}

int pushChild(int line,int type,int flag,char* value)
{
    int retVal = 1;
    
    CODE_NODE_T *node = (CODE_NODE_T *) calloc(1, sizeof(CODE_T));
    CODE_T *data = (CODE_T *) calloc(1, sizeof(CODE_T));
    if((data == NULL) || (node == NULL))
        retVal = -1;
    else
    {
        data->type = type;
        data->value = mystrdup(value);
        data->fLine = line;
        data->lLine = line;
        node->next = codeStackHead;
        node->flag = flag;
        node->data = data;
        codeStackHead = node;
    }
    return retVal;
}

int pushParent(CODE_NODE_T *parent)
{
    int retVal = 1;
    
    if(parent == NULL)
        retVal = 0;
    else
    {
        parent->next = codeStackHead;
        codeStackHead = parent;
    }
  return retVal;
}

int pushFlag(int flag)
{
    int retVal = 1;
    
    FLAG_T *data = (FLAG_T *)calloc(1, sizeof(FLAG_T));
    if(data == NULL)
        retVal = 0;
    else
    {
        data->flag = flag;
        data->next = flagStackHead;
        flagStackHead = data;
    }
    return retVal;
}

CODE_NODE_T *popCode()
{
    CODE_NODE_T *retCode = codeStackHead;
    if(retCode != NULL)
    {
        codeStackHead = retCode->next;
        retCode->next = NULL;
    }
    return retCode;
}

int popFlag()
{
    FLAG_T *retFlag = flagStackHead;
    int flag = -1;
    if(retFlag != NULL)
    {
        flag = retFlag->flag;
        flagStackHead = retFlag->next;
        free(retFlag);
    }
    return flag;
}

int pack()
{
    int retVal = -1;
    int bLastChild = 1;

    CODE_NODE_T *programNode = (CODE_NODE_T *) calloc(1, sizeof(CODE_NODE_T));
    CODE_T *program = (CODE_T*) calloc(1, sizeof(CODE_T));
    if(program != NULL || programNode != NULL)
    {
        retVal = 1;
        program->type = programNode->flag = PROGRAM;
        while(codeStackHead != NULL)
        {
            CODE_NODE_T *node = popCode();
            if(bLastChild)
            {
                program->lLine = node->data->lLine;
                bLastChild = 0;
            }
            // First Child
            if(flagStackHead == NULL)
            {
                program->fLine = node->data->fLine;
                program->count = program->lLine - program->fLine + 1;
            }
            // Insert to child list
            if(program->firstChild == NULL)
                program->lastChild = node->data;
            else
                node->data->next = program->firstChild;
            program->firstChild = node->data;
            node->data->parent = program;
            // free node
            node->data = NULL;
            free(node);

        }
        programNode->data = program;
        pushParent(programNode);
        if (codeStackHead == NULL)
        {
            printf("Debug : Error pushing packed node to stack\n");
            retVal = -1;
        }
    }
    return retVal;
}

int combine(int acseFlag, int yaccFlag)
{
    int retVal = 1;
    int bLastChild = 1;
    int bTry = 0;
    CODE_NODE_T *temp = NULL;
    
    CODE_NODE_T *parentNode = (CODE_NODE_T *) calloc(1, sizeof(CODE_NODE_T));
    CODE_T *parent = (CODE_T *) calloc(1, sizeof(CODE_T));
    if(parent == NULL || parentNode == NULL)
        retVal = -1;
    else
    {
        printf("Debug : Combining to block %d %d\n",acseFlag,yaccFlag);
        parent->type = acseFlag;
        parentNode->flag = yaccFlag;
        //printf("eiei1\n");
        while(flagStackHead != NULL)
        {
            CODE_NODE_T *node = popCode();
            //printf("eiei2 %d %d\n\t\"%s\"\n",node->flag, flagStackHead->flag, node->data->value);
            if(node == NULL && bLastChild) // Error in storing code node in the list
            {
                printf("Error : Something wrong in storing data\n");
                retVal = -1;
                break;
            }
            // if any problem occur try avoid BLOCK NODE (value == NULL)
            else if((node->flag == flagStackHead->flag) || isException(node->flag))
            {
                // Last Child
                if(bLastChild)
                {
                    parent->lLine = node->data->lLine;
                    bLastChild = 0;
                }
                // First Child
                if(flagStackHead == NULL)
                {
                    //printf("eiei3\n");
                    parent->fLine = node->data->fLine;
                    parent->count = parent->lLine - parent->fLine + 1;
                }
                // Flag matching
                //printf("eiei4\n");
                if(node->flag == flagStackHead->flag)
                    popFlag();
                // assign node to its parent
                //printf("eiei5\n");
                if(parent->firstChild == NULL)
                    parent->lastChild = node->data;
                else
                    node->data->next = parent->firstChild;
                //printf("eiei6\n");
                parent->firstChild = node->data;
                node->data->parent = parent;
                // free node
                //printf("eiei7\n");
                node->data = NULL;
                free(node);
                /*if (bTry) // if any problem occur try uncomment here
                {
                    //printf("eiei8\n");
                    pushParent(temp);
                    bTry = 0;
                    temp = NULL;
                } */           
            }
            else 
            {
                if(bTry)
                {
                    printf("Error : Uncorrect data.\n");
                    printStack();
                    exit(0);
                }
                //printf("eiei9\n");
                bTry = 1;
                temp = node;
            }
        }
        printf("eiei\n");
        // push back to stack
        parentNode->data = parent;
        pushParent(parentNode);
        if (bTry) // if any problem occur try comment here
        {
            //printf("eiei8\n");
            pushParent(temp);
            bTry = 0;
            temp = NULL;
        }  
        if (codeStackHead == NULL)
        {
            printf("Debug : Error pushing node to stack\n");
        }
    }
    return retVal;
}

int flagTransform(int oldFlag, int newFlag)
{
    int retVal = 0;
    CODE_NODE_T *current = codeStackHead;
    //printf("Transform %d to %d\n", oldFlag, newFlag);
    while(current != NULL)
    {
        if(oldFlag == current->flag)
        {
            current->flag = newFlag;
            retVal = 1;
            break;
        }
        current = current->next;
    }

    if(retVal == 0)
    {
        printf("Error : Invalid head stack flag\n");
        printf("Find flag %d but found flag %d\n", oldFlag, codeStackHead->flag);
        printStack();
        exit(0);
        retVal = -1;
    }
    return retVal;
}

void codeStackClear()
{
    while(codeStackHead != NULL)
    {
        CODE_NODE_T *dump = codeStackHead->next;
        free(codeStackHead);
        codeStackHead = dump;
    }
}

void flagStackClear()
{
    while(flagStackHead != NULL)
    {
        FLAG_T *dump = flagStackHead->next;
        free(flagStackHead);
        flagStackHead = dump;
    }
}

void printStack()
{
    CODE_NODE_T *current = codeStackHead;
    while(current != NULL)
    {
        printf("Flag %d\n\t\"%s\"\n", current->flag, current->data->value);
        current = current->next;
    }
    printf("End printf stack\n\n");
}

int tmp = 1;
int mode = 0;
int level = 0;
void printProgram(CODE_T *node)
{
    CODE_T *current = node;
    level++;
    if ((current == NULL) && (tmp))
    {
        tmp = 0;
        printf("PROGRAM IS NULL\n");
    }
    while(current != NULL)
    {
        if(tmp)
        {
            tmp = 0;
            printf("Start Printing Code from line %d to %d\n", current->fLine, current->lLine);
        }
        if(current->firstChild != NULL)    // if parent
        {
            //printf("Child in node %d\n", current->type);
            printProgram(current->firstChild);
            level--;
        }
        /*if(current->value == NULL)
            printf("%d",current->type);
        else
            printf("%s", current->value);*/

        if(current->value != NULL)
        {
            if(mode)
                printf("%s", current->value);
            else
                printf("\"%s\" [%d]\n", current->value, level);
        }
        current = current->next;
    }
}

// void printProgram(CODE_T *node)
// {
//     CODE_T *current = node->firstChild;
//     printf("\n%d\n\n", node->type);
//     while(current != NULL)
//     {
//         //printf("%d\n", current->type);
//         //printf("\"%s\"\n", current->value);
//         if(current->value == NULL)
//         {
//             CODE_T *tmp = current->firstChild;
//             while(tmp != NULL)
//             {
//                 printf("tmp: %d\n", tmp->type);

//                 if(tmp->value == NULL)
//                 {
//                     if(tmp->firstChild->value == NULL)
//                     {
//                         CODE_T *tmp2 = tmp->firstChild;
//                         while(tmp2 != NULL)
//                         {
//                             printf("\t%d\n",tmp2->type);
//                             if(tmp2->firstChild != NULL)
//                             {
//                                 CODE_T *tmp3 = tmp2->firstChild;
//                                 while(tmp3 != NULL)
//                                 {
//                                     printf("\t : %s\n",tmp3->value);
//                                     tmp3 = tmp3->next;
//                                 }
//                             }
//                             tmp2 = tmp2->next;
//                         }
//                     }
//                     else
//                     {
//                         printf("tmp child: %d\n", tmp->firstChild->type);
//                         printf("tmp child:\"%s\"\n", tmp->firstChild->value);
//                     }
                    
//                 }
//                 else
//                     printf("tmp: \"%s\"\n", tmp->value);
//                 tmp = tmp->next;
//             }
//         }
//         current = current->next;
//     }

//     // CODE_T *current = node;
//     // if(node->next != NULL)
//     //     printf("%s\n", node->next->value);
//     // else
//     //     printf("eiei\n");
// }

CODE_T *getStackHeadData()
{
    return codeStackHead->data;
}