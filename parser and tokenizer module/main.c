//  listProject.c
//  Code
//  To compile the code do as the same when compiling lex and Yacc file
//  Finally compile with: gcc -o main main.c y.tab.c lex.yy.c -ly -ll
//  Created by Siva on 4/8/2557 BE.
//  Copyright (c) 2557 Siva. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "LexYacc.h"


int main(int argc,char* argv[])
    {
    char *codes[5000];
    extern int result[20];
    int i = 0;
    FILE* fileInput;
    char inputBuffer[5000];
    char lineData[5000];
    int retVal = 0;
    
    codeStackClear();
    flagStackClear();

    if(structListInit() == -1)
    {
        printf("Error : Cannot initial structList\n");
        exit(0);
    }
    if(typedefListInit() == -1)
    {
        printf("Error : Cannot initial typedefList\n");
        exit(0);
    }
    if((fileInput=fopen(argv[1],"r"))==NULL)
        {
        printf("Error reading files, the program terminates immediately\n");
        exit(0);
        }
    else
    {
        while(fgets(inputBuffer,sizeof(inputBuffer),fileInput) != NULL)
        {
            sscanf(inputBuffer,"%[^\n]s",lineData);
            if(i < 5000)
            {
                codes[i] = mystrdup(lineData);
                i++;
            }
            else
            {
                printf("The input file's size is too large\n");
                exit(0);
            }

        }
    }

    rewind(fileInput);
    retVal = parse(fileInput);
    if(retVal) // error occur
    {
        printf("Syntax error line %d\n",getLine());
        printf("\t%s\n",codes[getLine()-1]);
    }
    else
    {
        pack();
        printProgram(getStackHeadData());
        printf("\n");
    }
    codeStackClear();
    flagStackClear();
    printStructList();
    printTypedefList();
    structlistDestroy();
    typedeflistDestroy();
    }