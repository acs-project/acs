typedef enum
{
	NAME,
	FORMAT,
	DOCUMENT
} TYPE;

typedef enum 
{
	ALLVAR = 0,
	GLOBAL,
	LOCAL,
	STATIC,
	EXTERN,
	CONSTANT,
	DEFINEDCONST,
	ENUM,
	FUNC,
	STRUCT,
	TYPEDEF,
	BRACEPOS = 0,
	IFELSE,
	SWITCH,
	VARDECLAREPOS,
	OPSPACE,
	BLOCKCOMM = 0,
	SINGLELINECOMM,
	TRAILCOMM
} SUB_TYPE;

typedef struct _rule
{
	int id;
	char name[64];
	int type;
	int subtype;
	void *data;
	struct _rule *pNext;
} RULE_T;

typedef struct
{
	RULE_T *rHead;
	RULE_T *rTail;
	RULE_T *rCurrent;
} LIST_T;

typedef struct
{
	int maxLength;			// 
	int minLength;
	int style;
	char prefix[5];
	char suffix[5];
	char **exception;
} NAME_T;

typedef struct
{
	int indentAmount;
	int bracePlacement;
	int position;
	int maxLine;
} FORMAT_T;

typedef struct
{

} DOCUMENT_T;