%token	IDENTIFIER I_CONSTANT F_CONSTANT STRING_LITERAL FUNC_NAME SIZEOF
%token	PTR_OP INC_OP DEC_OP LEFT_OP RIGHT_OP LE_OP GE_OP EQ_OP NE_OP
%token	AND_OP OR_OP MUL_ASSIGN DIV_ASSIGN MOD_ASSIGN ADD_ASSIGN
%token	SUB_ASSIGN LEFT_ASSIGN RIGHT_ASSIGN AND_ASSIGN
%token	XOR_ASSIGN OR_ASSIGN
%token	TYPEDEF_NAME ENUMERATION_CONSTANT

%token	TYPEDEF EXTERN STATIC AUTO REGISTER INLINE
%token	CONST RESTRICT VOLATILE
%token	BOOL CHAR SHORT INT LONG SIGNED UNSIGNED FLOAT DOUBLE VOID
%token	COMPLEX IMAGINARY
%token	STRUCT UNION ENUM ELLIPSIS

%token	CASE DEFAULT IF ELSE SWITCH WHILE DO FOR GOTO CONTINUE BREAK RETURN

%token	ALIGNAS ALIGNOF ATOMIC GENERIC NORETURN STATIC_ASSERT THREAD_LOCAL

%token INCLUDE DATATYPE STRUCT_NAME DEFINE LIBRARY CHAR_LITERAL


%start translation_unit
%{
    
    #include<stdio.h>
    #include<string.h>
    #include"LexYacc.h"
    extern FILE* yyin;
int yylex();
void yyerror(const char *s);
    
%}



%%

define_declaration
: '#' DEFINE IDENTIFIER define_constant
	{
		printf("906\n");
		pushFlag(HASH); 
		pushFlag(DEFINE); 
		pushFlag(IDENTIFIER); 
		pushFlag(define_constant); 
		combine(DEFINE_STMT,define_declaration);
	}
;

define_constant
: I_CONSTANT 				
	{
		flagTransform(I_CONSTANT,define_constant);
	}
| F_CONSTANT 				
	{
		flagTransform(F_CONSTANT,define_constant);
	}
| STRING_LITERAL			
	{
		flagTransform(STRING_LITERAL,define_constant);
	}
| CHAR_LITERAL				
	{
		flagTransform(CHAR_LITERAL,define_constant);
	}
;


include 
: '#' INCLUDE '<' LIBRARY '>'  
	{
		printf("903\n");
		pushFlag(HASH); 
		pushFlag(INCLUDE); 
		pushFlag(REL_OP_1); 
		pushFlag(LIBRARY); 
		pushFlag(REL_OP_2); 
		combine(INCLUDE_STMT,include);
	}// library
| '#' INCLUDE  STRING_LITERAL  
	{
		printf("904\n");
		pushFlag(HASH); 
		pushFlag(INCLUDE); 
		pushFlag(STRING_LITERAL); 
		combine(INCLUDE_STMT,include);
	}// header file
;

primary_expression
: IDENTIFIER   			
	{
		printf("1\n");
		flagTransform(IDENTIFIER,primary_expression);
	}
| constant  			
	{
		printf("2\n");
		flagTransform(constant,primary_expression);
	}
| string				
	{
		printf("3\n");
		flagTransform(string,primary_expression);
	}
| '(' expression ')' 	
	{
		printf("4\n");
		pushFlag(OP_PAR_BRACKET);
		pushFlag(expression); 
		pushFlag(ED_PAR_BRACKET);
		combine(PRI_EXPR,primary_expression);
	}
| generic_selection 	
	{
		printf("5\n");
		flagTransform(PRI_EXPR,primary_expression);
	}
;

constant
: I_CONSTANT		/* includes character_constant */ 	
	{
		printf("6\n");
		flagTransform(I_CONSTANT,constant);
	}
| CHAR_LITERAL 											
	{
		printf("11\n");
		flagTransform(CHAR_LITERAL,constant);
	}
| F_CONSTANT 											
	{
		printf("7\n");
		flagTransform(F_CONSTANT,constant);
	}
| ENUMERATION_CONSTANT									
	{
		printf("8\n");
		flagTransform(ENUMERATION_CONSTANT,constant);
	}/* after it has been defined as such */
;

enumeration_constant		/* before it has been defined as such */
: IDENTIFIER 		
	{
		printf("9\n");
		flagTransform(IDENTIFIER,enumeration_constant);
	}
;

string
: STRING_LITERAL 	
	{
		printf("10\n");
		flagTransform(STRING_LITERAL,string);
	}
| FUNC_NAME 		
	{
		printf("12\n");
		flagTransform(FUNC_NAME,string);
	}
;

generic_selection
: GENERIC '(' assignment_expression ',' generic_assoc_list ')' 
	{
		printf("13\n");
		pushFlag(GENERIC); 
		pushFlag(OP_PAR_BRACKET); 
		pushFlag(assignment_expression); 
		pushFlag(COMM); 
		pushFlag(generic_assoc_list);
		pushFlag(ED_PAR_BRACKET); 
		combine(GENERIC_ASSOC,generic_selection);
	}
;

generic_assoc_list
: generic_association 							
	{
		printf("14\n");
		flagTransform(GENERIC_ASSOC,generic_assoc_list);
	}
| generic_assoc_list ',' generic_association 	
	{
		printf("15\n");
		pushFlag(generic_assoc_list); 
		pushFlag(COMM); 
		pushFlag(generic_association);
		combine(GENERIC_ASSOC,generic_assoc_list);
	}
;

generic_association
: type_name ':' assignment_expression 
	{
		printf("16\n");
		pushFlag(type_name); 
		pushFlag(COL); 
		pushFlag(assignment_expression);
		combine(GENERIC_ASSOC,generic_association);
	}
| DEFAULT ':' assignment_expression 
	{
		printf("17\n");
		pushFlag(DEFAULT); 
		pushFlag(COL); 
		pushFlag(assignment_expression);
		combine(GENERIC_ASSOC,generic_association);
	}
;

postfix_expression //////////////////////// pHead
: primary_expression
	{
		printf("18\n");
		flagTransform(primary_expression,postfix_expression);
	}
| postfix_expression '[' expression ']' 				
	{
		printf("19\n");
		pushFlag(postfix_expression); 
		pushFlag(OP_SQ_BRACKET); 
		pushFlag(expression); 
		pushFlag(ED_SQ_BRACKET); 
		combine(MEM_ACS_EXPR,postfix_expression);
	} // member-access
| postfix_expression '(' ')' 							
	{
		printf("20\n");
		pushFlag(postfix_expression); 
		pushFlag(OP_PAR_BRACKET); 
		pushFlag(ED_PAR_BRACKET); 
		combine(FUNC_CALL_EXPR,postfix_expression);
	}// FUNC_CALL_EXPR
| postfix_expression '(' argument_expression_list ')' 	
	{
		printf("21\n");
		pushFlag(postfix_expression); 
		pushFlag(OP_PAR_BRACKET); 
		pushFlag(argument_expression_list); 
		pushFlag(ED_PAR_BRACKET); 
		combine(FUNC_CALL_EXPR,postfix_expression);
	} //FUNC_CALL_EXPR
| postfix_expression '.' IDENTIFIER 					
	{
		printf("22\n");
		pushFlag(postfix_expression); 
		pushFlag(FULL_STOP); 
		pushFlag(IDENTIFIER); 
		combine(MEM_ACS_EXPR,postfix_expression);
	} // member-access
| postfix_expression PTR_OP IDENTIFIER 					
	{
		printf("23\n");
		pushFlag(postfix_expression); 
		pushFlag(PTR_OP); 
		pushFlag(IDENTIFIER);  
		combine(MEM_ACS_EXPR,postfix_expression);
	} //member-access
| postfix_expression INC_OP 							
	{
		printf("24\n");
		pushFlag(postfix_expression); 
		pushFlag(INC_OP); 
		combine(INC_DEC_EXPR,postfix_expression);
	}
| postfix_expression DEC_OP 							
	{
		printf("25\n");
		pushFlag(postfix_expression); 
		pushFlag(DEC_OP); 
		combine(INC_DEC_EXPR,postfix_expression);
	}
| '(' type_name ')' '{' initializer_list '}' 			
	{
		printf("26\n");
		pushFlag(OP_PAR_BRACKET);
		pushFlag(type_name); 
		pushFlag(ED_PAR_BRACKET); 
		pushFlag(OP_CUR_BRACKET); 
		pushFlag(initializer_list);
		pushFlag(ED_CUR_BRACKET); 
		combine(PRI_EXPR,postfix_expression);
	}
| '(' type_name ')' '{' initializer_list ',' '}' 		
	{
		printf("27\n");
		pushFlag(OP_PAR_BRACKET);
		pushFlag(type_name); 
		pushFlag(ED_PAR_BRACKET); 
		pushFlag(OP_CUR_BRACKET); 
		pushFlag(initializer_list);
		pushFlag(COMM); 
		pushFlag(ED_CUR_BRACKET); 
		combine(PRI_EXPR,postfix_expression);
	}
;

argument_expression_list
: assignment_expression 								
	{
		printf("28\n");
		flagTransform(assignment_expression,argument_expression_list);
	}
| argument_expression_list ',' assignment_expression 	
	{
		printf("29\n");
		pushFlag(argument_expression_list); 
		pushFlag(COMM); 
		pushFlag(assignment_expression); 
		combine(ASS_EXPR,argument_expression_list);
	}
;

unary_expression
: postfix_expression 				
	{
		printf("30\n");
		flagTransform(postfix_expression,unary_expression);
	}
| INC_OP unary_expression 			
	{
		printf("31\n");
		pushFlag(INC_OP); 
		pushFlag(unary_expression); 
		combine(INC_DEC_EXPR,unary_expression);
	}
| DEC_OP unary_expression 			
	{
		printf("32\n");
		pushFlag(DEC_OP); 
		pushFlag(unary_expression); 
		combine(INC_DEC_EXPR,unary_expression);
	}
| unary_operator cast_expression 	
	{
		printf("33\n");
		pushFlag(unary_operator); 
		pushFlag(cast_expression); 
		combine(MEM_ACS_EXPR,unary_expression);
	} // member-access
| SIZEOF unary_expression 			
	{
		printf("34\n");
		pushFlag(SIZEOF); 
		pushFlag(unary_expression);
		combine(UNARY_EXPR,unary_expression);
	}
| SIZEOF '(' type_name ')' 			
	{
		printf("35\n");
		pushFlag(SIZEOF); 
		pushFlag(OP_PAR_BRACKET);
		pushFlag(type_name); 
		pushFlag(ED_PAR_BRACKET); 
		combine(UNARY_EXPR,unary_expression);
	}
| ALIGNOF '(' type_name ')' 		
	{
		printf("36\n");
		pushFlag(ALIGNOF); 
		pushFlag(OP_PAR_BRACKET);
		pushFlag(type_name); 
		pushFlag(ED_PAR_BRACKET); 
		combine(UNARY_EXPR,unary_expression);
	}
;

unary_operator
: '&' 	
	{
		printf("37\n");
		flagTransform(BIT_OP,unary_operator);
	}
| '*' 	
	{
		printf("38\n");
		flagTransform(ARITH_OP_1,unary_operator);
	}
| '+' 	
	{
		printf("39\n");
		flagTransform(ARITH_OP_1,unary_operator);
	}
| '-' 	
	{
		printf("40\n");
		flagTransform(ARITH_OP_1,unary_operator);
	}
| '~' 	
	{
		printf("41\n");
		flagTransform(BIT_OP,unary_operator);
	}
| '!' 	
	{
		printf("42\n");
		flagTransform(LOGIC_OP,unary_operator);
	}
;

cast_expression
: unary_expression 					
	{
		printf("43\n");
		flagTransform(unary_expression,cast_expression);
	}
| '(' type_name ')' cast_expression 
	{
		printf("44\n");
		pushFlag(OP_PAR_BRACKET);
		pushFlag(type_name); 
		pushFlag(ED_PAR_BRACKET);
		pushFlag(cast_expression); 
		combine(CAST_EXPR,cast_expression); 
	}
;

multiplicative_expression /////////////////////////////////////////// STUDENT_T*
: cast_expression 								
	{
		printf("45\n");
		flagTransform(cast_expression,multiplicative_expression);
	}
| multiplicative_expression '*' cast_expression 
	{
		printf("46\n");
		pushFlag(multiplicative_expression); 
		pushFlag(ARITH_OP_1); 
		pushFlag(cast_expression); 
		combine(ARTH_EXPR,multiplicative_expression);
	}
| multiplicative_expression '/' cast_expression 
	{
		printf("47\n");
		pushFlag(multiplicative_expression); 
		pushFlag(ARITH_OP_1); 
		pushFlag(cast_expression); 
		combine(ARTH_EXPR,multiplicative_expression);
	}
| multiplicative_expression '%' cast_expression 
	{
		printf("48\n");
		pushFlag(multiplicative_expression); 
		pushFlag(ARITH_OP_1); 
		pushFlag(cast_expression); 
		combine(ARTH_EXPR,multiplicative_expression);
	}
;

additive_expression
: multiplicative_expression 						
	{
		printf("49\n");
		flagTransform(multiplicative_expression,additive_expression);
	}
| additive_expression '+' multiplicative_expression	
	{
		printf("50\n");
		pushFlag(additive_expression); 
		pushFlag(ARITH_OP_1); 
		pushFlag(multiplicative_expression); 
		combine(ARTH_EXPR,additive_expression);
	}
| additive_expression '-' multiplicative_expression 
	{
		printf("51\n");
		pushFlag(additive_expression); 
		pushFlag(ARITH_OP_1); 
		pushFlag(multiplicative_expression); 
		combine(ARTH_EXPR,additive_expression);
	}
;

shift_expression
: additive_expression 								
	{
		printf("53\n");
		flagTransform(additive_expression,shift_expression);
	}
| shift_expression LEFT_OP additive_expression 		
	{
		printf("54\n");
		pushFlag(shift_expression); 
		pushFlag(LEFT_OP);
		pushFlag(additive_expression); 
		combine(ASS_EXPR,shift_expression); // shift to BIT
	}
| shift_expression RIGHT_OP additive_expression 	
	{
		printf("55\n");
		pushFlag(shift_expression); 
		pushFlag(RIGHT_OP);
		pushFlag(additive_expression); 
		combine(ASS_EXPR,shift_expression); //  shift to BIT
	}
;

relational_expression
: shift_expression 								
	{		
		printf("56\n");
		flagTransform(shift_expression,relational_expression);
	}
| relational_expression '<' shift_expression 	
	{
		printf("57\n");
		pushFlag(relational_expression); 
		pushFlag(REL_OP_1); 
		pushFlag(shift_expression); 
		combine(COMP_EXPR,relational_expression);
	}
| relational_expression '>' shift_expression 	
	{
		printf("58\n");
		pushFlag(relational_expression); 
		pushFlag(REL_OP_2); 
		pushFlag(shift_expression); 
		combine(COMP_EXPR,relational_expression);
	}
| relational_expression LE_OP shift_expression 	
	{
		printf("59\n");
		pushFlag(relational_expression); 
		pushFlag(LE_OP); 
		pushFlag(shift_expression); 
		combine(COMP_EXPR,relational_expression);
	}
| relational_expression GE_OP shift_expression 	
	{
		printf("60\n");
		pushFlag(relational_expression); 
		pushFlag(GE_OP); 
		pushFlag(shift_expression);
		combine(COMP_EXPR,relational_expression);
	}
;

equality_expression
: relational_expression 							
	{
		printf("61\n");
		flagTransform(relational_expression,equality_expression);
	}
| equality_expression EQ_OP relational_expression 	
	{
		printf("62\n");
		pushFlag(equality_expression); 
		pushFlag(EQ_OP); 
		pushFlag(relational_expression); 
		combine(LOG_EXPR,equality_expression);
	}
| equality_expression NE_OP relational_expression 	
	{
		printf("63\n");
		pushFlag(equality_expression); 
		pushFlag(NE_OP); 
		pushFlag(relational_expression); 
		combine(LOG_EXPR,equality_expression);
	}
;

and_expression
: equality_expression 						
	{											
		printf("64\n");
		flagTransform(equality_expression,and_expression);
	}
| and_expression '&' equality_expression 	
	{
		printf("65\n");
		pushFlag(and_expression); 
		pushFlag(BIT_OP); 
		pushFlag(equality_expression); 
		combine(LOG_EXPR,and_expression);
	}
;

exclusive_or_expression
: and_expression 								
	{
		printf("66\n");
		flagTransform(and_expression,exclusive_or_expression);
	}
| exclusive_or_expression '^' and_expression 	
	{
		printf("67\n");
		pushFlag(exclusive_or_expression); 
		pushFlag(BIT_OP); 
		pushFlag(and_expression); 
		combine(LOG_EXPR,exclusive_or_expression);
	}
;

inclusive_or_expression
: exclusive_or_expression 								
	{
		printf("68\n");
		flagTransform(exclusive_or_expression,inclusive_or_expression);
	}
| inclusive_or_expression '|' exclusive_or_expression	
	{
		printf("69\n");
		pushFlag(inclusive_or_expression); 
		pushFlag(BIT_OP); 
		pushFlag(exclusive_or_expression); 
		combine(LOG_EXPR,inclusive_or_expression);
	}
;

logical_and_expression
: inclusive_or_expression 								
	{
		printf("70\n");
		flagTransform(inclusive_or_expression,logical_and_expression);
	}
| logical_and_expression AND_OP inclusive_or_expression 
	{
		printf("71\n");
		pushFlag(logical_and_expression); 
		pushFlag(AND_OP); 
		pushFlag(inclusive_or_expression); 
		combine(LOG_EXPR,logical_and_expression);
	}
;

logical_or_expression
: logical_and_expression 								
	{
		printf("72\n");
		flagTransform(logical_and_expression,logical_or_expression);
	}
| logical_or_expression OR_OP logical_and_expression	
	{
		printf("73\n");
		pushFlag(logical_or_expression); 
		pushFlag(OR_OP); 
		pushFlag(logical_and_expression); 
		combine(LOG_EXPR,logical_or_expression);
	}
;

conditional_expression
: logical_or_expression 											
	{
		printf("74\n");
		flagTransform(logical_or_expression,conditional_expression);
	}
| logical_or_expression '?' expression ':' conditional_expression 	
	{
		printf("75\n");
		pushFlag(logical_or_expression); 
		pushFlag(Q_MARK); 
		pushFlag(expression); 
		pushFlag(COL); 
		pushFlag(conditional_expression); 
		combine(TERN_COND_EXPR,conditional_expression);
	}
;

assignment_expression
: conditional_expression 										
	{
		printf("76\n");
		flagTransform(conditional_expression,assignment_expression);
	} //TERN_COND_EXPR
| unary_expression assignment_operator assignment_expression 	
	{
		printf("77\n");
		pushFlag(unary_expression); 
		pushFlag(assignment_operator); 
		pushFlag(assignment_expression); 
		combine(ASS_EXPR,assignment_expression);
	}
;

assignment_operator
: '=' 			
	{
		printf("78\n");
		flagTransform(ASS_OP,assignment_operator);
	}
| MUL_ASSIGN 	
	{
		printf("79\n");
		flagTransform(MUL_ASSIGN,assignment_operator);
	}
| DIV_ASSIGN 	
	{
		printf("80\n");
		flagTransform(DIV_ASSIGN,assignment_operator);
	}
| MOD_ASSIGN 	
	{
		printf("81\n");
		flagTransform(MOD_ASSIGN,assignment_operator);
	}
| ADD_ASSIGN 	
	{
		printf("82\n");
		flagTransform(ADD_ASSIGN,assignment_operator);
	}
| SUB_ASSIGN 	
	{
		printf("83\n");
		flagTransform(SUB_ASSIGN,assignment_operator);
	}
| LEFT_ASSIGN 	
	{
		printf("84\n");
		flagTransform(LEFT_ASSIGN,assignment_operator);
	}
| RIGHT_ASSIGN 	
	{
		printf("85\n");
		flagTransform(RIGHT_ASSIGN,assignment_operator);
	}
| AND_ASSIGN 	
	{
		printf("86\n");
		flagTransform(AND_ASSIGN,assignment_operator);
	}
| XOR_ASSIGN 	
	{
		printf("87\n");
		flagTransform(XOR_ASSIGN,assignment_operator);
	}
| OR_ASSIGN 	
	{
		printf("88\n");
		flagTransform(OR_ASSIGN,assignment_operator);
	}
;

expression
: assignment_expression 				
	{
		printf("89\n");
		flagTransform(assignment_expression,expression);
	}
| expression ',' assignment_expression 	
	{
		printf("90\n");
		pushFlag(expression); 
		pushFlag(COMM); 
		pushFlag(assignment_expression);
		combine(EXPR_STMT,expression);
	}
;

constant_expression
: conditional_expression				
	{
		printf("91\n");
		flagTransform(conditional_expression,constant_expression);
	}/* with constraints */
;

/*typedef_abstract_declaration
: type_specifier declaration_specifiers 
	{
		printf("typedef abstract\n");
		pushFlag(type_specifier); 
		pushFlag(declaration_specifiers); 
		combine(DECL_STMT,typedef_abstract_declaration);
	}
;*/

declaration
: declaration_specifiers ';'    								
	{
		printf("92\n");
		pushFlag(declaration_specifiers); 
		pushFlag(SEMICOMM); 
		combine(DECL_STMT,declaration);
	}
| declaration_specifiers init_declarator_list ';' /* int a;*/ 	
	{
		printf("93 %d %d\n", declaration_specifiers, init_declarator_list);
		pushFlag(declaration_specifiers); 
		pushFlag(init_declarator_list); 
		pushFlag(SEMICOMM); 
		combine(DECL_STMT,declaration);
	}
/*| typedef_abstract_declaration									
	{
		printf("300\n");
		flagTransform(typedef_abstract_declaration,declaration);
	}*/
//| struct_abstract_declaration ';'
| static_assert_declaration 									
	{
		printf("94\n");
		flagTransform(static_assert_declaration,declaration);
	}
;

declaration_specifiers
: storage_class_specifier declaration    		
	{
		printf("95\n");	
		pushFlag(storage_class_specifier); 
		pushFlag(declaration); 
		combine(DECL_STMT,declaration_specifiers);
	}
| storage_class_specifier               		
	{
		printf("96\n");
		flagTransform(storage_class_specifier,declaration_specifiers);
	}
| type_specifier declaration_specifiers 		
	{
		printf("97\n");
		pushFlag(type_specifier); 
		pushFlag(declaration_specifiers); 
		combine(DECL_STMT,declaration_specifiers);
	}
| type_specifier 								
	{
		printf("98\n");
		flagTransform(type_specifier,declaration_specifiers);
	}
| type_qualifier declaration_specifiers			
	{
		printf("99\n");
		pushFlag(type_qualifier); 
		pushFlag(declaration_specifiers); 
		combine(DECL_STMT,declaration_specifiers);									
	}
| type_qualifier 								
	{
		printf("100\n");
		flagTransform(type_qualifier,declaration_specifiers);
	}
| function_specifier declaration_specifiers 	
	{
		printf("101\n");
		pushFlag(function_specifier); 
		pushFlag(declaration_specifiers); 
		combine(DECL_STMT,declaration_specifiers);
	}
| function_specifier 							
	{
		printf("102\n");
		flagTransform(function_specifier,declaration_specifiers);
	}
| alignment_specifier declaration_specifiers 	
	{
		printf("103\n");
		pushFlag(alignment_specifier); 
		pushFlag(declaration_specifiers); 
		combine(DECL_STMT,declaration_specifiers);
	}
| alignment_specifier 							
	{
		printf("104\n");
		flagTransform(alignment_specifier,declaration_specifiers);
	}
;

init_declarator_list
: init_declarator 							
	{
		printf("105\n");
		flagTransform(init_declarator,init_declarator_list);
	}
| init_declarator_list ',' init_declarator 	
	{
		printf("106\n");
		pushFlag(init_declarator_list); 
		pushFlag(COMM); 
		pushFlag(init_declarator); 
		combine(INIT_DECL,init_declarator_list);
	}
;

init_declarator
: declarator '=' initializer 	
	{
		printf("107\n");
		pushFlag(declarator); 
		pushFlag(ASS_OP); 
		pushFlag(initializer); 
		combine(INIT_DECL,init_declarator);
	}
| declarator 					
	{
		printf("108\n");
		flagTransform(declarator,init_declarator);
	}
;

storage_class_specifier
: TYPEDEF		
	{
		printf("109\n");
		flagTransform(TYPEDEF,storage_class_specifier);
	} // identifiers must be flagged as TYPEDEF_NAME 
| EXTERN 		
	{
		printf("110\n");
		flagTransform(EXTERN,storage_class_specifier);
	}
| STATIC 		
	{
		printf("111\n");
		flagTransform(STATIC,storage_class_specifier);
	}
| THREAD_LOCAL 	
	{
		printf("112\n");
		flagTransform(THREAD_LOCAL,storage_class_specifier);
	}
| AUTO 			
	{
		printf("113\n");
		flagTransform(AUTO,storage_class_specifier);
	}
| REGISTER 		
	{
		printf("114\n");
		flagTransform(REGISTER,storage_class_specifier);
	}
; 


type_specifier
: VOID 							
	{
		printf("115\n");
		flagTransform(TYPE_SPEC,type_specifier);
	}
| CHAR 							
	{
		printf("116\n");
		flagTransform(TYPE_SPEC,type_specifier);
	}
| SHORT 						
	{
		printf("117\n");
		flagTransform(TYPE_SPEC,type_specifier);
	}
| INT 							
	{
		printf("118\n");
		flagTransform(TYPE_SPEC,type_specifier);
	}
| LONG 	
	{
		printf("119\n");
		flagTransform(TYPE_SPEC,type_specifier);
	}
| FLOAT 						
	{
		printf("120\n");
		flagTransform(TYPE_SPEC,type_specifier);
	}
| DOUBLE 						
	{
		printf("121\n");
		flagTransform(TYPE_SPEC,type_specifier);
	}
| SIGNED 						
	{	
		printf("122\n");
		flagTransform(TYPE_SPEC,type_specifier);
	}
| UNSIGNED 						
	{
		printf("123\n");
		flagTransform(TYPE_SPEC,type_specifier);
	}
| BOOL 							
	{
		printf("124\n");
		flagTransform(TYPE_SPEC,type_specifier);
	}
| COMPLEX 						
	{
		printf("125\n");
		flagTransform(TYPE_SPEC,type_specifier);
	}
| IMAGINARY	  					
	{
		printf("126\n");
		flagTransform(TYPE_SPEC,type_specifier);
	}// non-mandated extension 
| atomic_type_specifier 		
	{
		printf("127\n");
		flagTransform(TYPE_SPEC,type_specifier);
	}
| struct_or_union_specifier 	
	{
		printf("129\n");
		flagTransform(TYPE_SPEC,type_specifier);
	}
| enum_specifier 				
	{
		printf("130\n");
		flagTransform(TYPE_SPEC,type_specifier);
	}
| TYPEDEF_NAME					
	{
		printf("131\n");
		flagTransform(TYPE_SPEC,type_specifier);
	}	// after it has been defined as such 
; 

/*type_specifier
: type_qualifier 
| struct_definition_title {printf("128\n");}
| struct_or_union_specifier {printf("129\n");}
| enum_specifier {printf("130\n");}
| type_qualifier	{printf("131\n");}	// after it has been defined as such 
; */

typedef_specifier
: TYPEDEF struct_or_union_specifier TYPEDEF_NAME ';'
	{
		printf("131.5\n");
		pushFlag(TYPEDEF);
		pushFlag(struct_or_union_specifier);
		pushFlag(TYPEDEF_NAME);
		pushFlag(SEMICOMM);
		combine(TYPE_SPEC, typedef_specifier); // TYPE SPEC
	}
; 

struct_or_union_specifier
: struct_or_union '{' struct_declaration_list '}' 
	{
		printf("132\n");
		pushFlag(struct_or_union); 
		pushFlag(OP_CUR_BRACKET); 
		pushFlag(struct_declaration_list); 
		pushFlag(ED_CUR_BRACKET); 
		combine(TYPE_SPEC,struct_or_union_specifier);
	}
| struct_or_union IDENTIFIER '{' struct_declaration_list '}' 
	{
		printf("133\n");
		pushFlag(struct_or_union); 
		pushFlag(IDENTIFIER); 
		pushFlag(OP_CUR_BRACKET); 
		pushFlag(struct_declaration_list); 
		pushFlag(ED_CUR_BRACKET); 
		combine(TYPE_SPEC,struct_or_union_specifier);
	}
| struct_or_union IDENTIFIER 
	{
		printf("134\n");
		pushFlag(struct_or_union); 
		pushFlag(IDENTIFIER);
		combine(TYPE_SPEC,struct_or_union_specifier);
	}
;

struct_or_union
: STRUCT 	
	{
		printf("135\n");
		flagTransform(STRUCT,struct_or_union);
	}
| UNION 	
	{
		printf("136\n");
		flagTransform(UNION,struct_or_union);
	}
;

struct_declaration_list
: struct_declaration 							
	{
		printf("137\n");
		flagTransform(struct_declaration,struct_declaration_list);
	}
| struct_declaration_list struct_declaration 	
	{
		printf("138\n");
		pushFlag(struct_declaration_list); 
		pushFlag(struct_declaration); 
		combine(DECL_STMT,struct_declaration_list);
	}
;

struct_declaration
: specifier_qualifier_list ';'							
	{
		printf("139\n");
		pushFlag(specifier_qualifier_list); 
		pushFlag(SEMICOMM); 
		combine(DECL_STMT,struct_declaration);
	}/* for anonymous struct/union */ 
| specifier_qualifier_list struct_declarator_list ';' 	
	{
		printf("140\n");
		pushFlag(specifier_qualifier_list); 
		pushFlag(struct_declarator_list); 
		pushFlag(SEMICOMM); 
		combine(DECL_STMT,struct_declaration);
	}
| static_assert_declaration 							
	{
		printf("142\n");
		flagTransform(static_assert_declaration,struct_declaration);
	}
;

specifier_qualifier_list
: type_specifier specifier_qualifier_list 	
	{
		printf("143\n"); 
		pushFlag(type_specifier);
		pushFlag(specifier_qualifier_list); 
		combine(specifier_qualifier_list,specifier_qualifier_list);
		
	}
| type_specifier 							
	{
		printf("144\n");
		flagTransform(type_specifier,specifier_qualifier_list);
	}
| type_qualifier specifier_qualifier_list 	
	{
		printf("145\n");
		pushFlag(type_qualifier);
		pushFlag(specifier_qualifier_list); 
		combine(specifier_qualifier_list,specifier_qualifier_list);
	}
| type_qualifier 							
	{
		printf("146\n");
		flagTransform(type_qualifier,specifier_qualifier_list);
	}
;

struct_declarator_list
: struct_declarator 							
	{
		printf("147\n");
		flagTransform(struct_declarator,struct_declarator_list);
	}
| struct_declarator_list ',' struct_declarator 	
	{
		printf("148\n");
		pushFlag(struct_declarator_list); 
		pushFlag(COMM); 
		pushFlag(struct_declarator); 
		combine(DECL_STMT,struct_declarator_list);
	}
;

struct_declarator
: ':' constant_expression 				
	{
		printf("149\n");
		pushFlag(COL); 
		pushFlag(constant_expression); 
		combine(DECL_STMT,struct_declarator);
	}
| declarator ':' constant_expression 	
	{
		printf("150\n");
		pushFlag(declarator); 
		pushFlag(COL); 
		pushFlag(constant_expression); 
		combine(DECL_STMT,struct_declarator);
	}
| declarator 							
	{	
		printf("151\n");
		flagTransform(declarator,struct_declarator);
	}
;

enum_specifier
: ENUM '{' enumerator_list '}' 
	{
		printf("152\n");
		pushFlag(ENUM); 
		pushFlag(OP_CUR_BRACKET);
		pushFlag(enumerator_list); 
		pushFlag(ED_CUR_BRACKET); 
		combine(TYPE_SPEC,enum_specifier);
	}
| ENUM '{' enumerator_list ',' '}' 
	{
		printf("153\n");
		pushFlag(ENUM); 
		pushFlag(OP_CUR_BRACKET);
		pushFlag(enumerator_list); 
		pushFlag(COMM);
		pushFlag(ED_CUR_BRACKET); 
		combine(TYPE_SPEC,enum_specifier);
	}
| ENUM IDENTIFIER '{' enumerator_list '}' 
	{
		printf("154\n");
		pushFlag(ENUM); 
		pushFlag(IDENTIFIER); 
		pushFlag(OP_CUR_BRACKET);
		pushFlag(enumerator_list); 
		pushFlag(ED_CUR_BRACKET); 
		combine(TYPE_SPEC,enum_specifier);
	}
| ENUM IDENTIFIER '{' enumerator_list ',' '}' 
	{
		printf("155\n");
		pushFlag(ENUM); 
		pushFlag(IDENTIFIER); 
		pushFlag(OP_CUR_BRACKET);
		pushFlag(enumerator_list); 
		pushFlag(COMM);
		pushFlag(ED_CUR_BRACKET); 
		combine(TYPE_SPEC,enum_specifier);
	}
| ENUM IDENTIFIER 
	{
		printf("156\n");
		pushFlag(ENUM); 
		pushFlag(IDENTIFIER); 
		combine(TYPE_SPEC,enum_specifier);
	}
;

enumerator_list
: enumerator 		
	{
		printf("157\n");
		flagTransform(enumerator,enumerator_list);
	}
| enumerator_list ',' enumerator 	
	{
		printf("158\n");
		pushFlag(enumerator_list); 
		pushFlag(COMM); 
		pushFlag(enumerator);
		combine(ENUMERATOR,enumerator_list);
	}
;

enumerator	/* identifiers must be flagged as ENUMERATION_CONSTANT */
: enumeration_constant '=' constant_expression 	
	{
		printf("159\n");
		pushFlag(enumeration_constant); 
		pushFlag(ASS_OP); 
		pushFlag(constant_expression);
		combine(ENUMERATOR,enumerator);
	}
| enumeration_constant 							
	{
		printf("160\n");
		flagTransform(enumeration_constant,enumerator);
	}
;

atomic_type_specifier
: ATOMIC '(' type_name ')' 
	{
		printf("161\n");
		pushFlag(ATOMIC); 
		pushFlag(OP_PAR_BRACKET); 
		pushFlag(type_name);
		pushFlag(ED_PAR_BRACKET); 
		combine(TYPE_SPEC,type_specifier);
	}
;

type_qualifier
: CONST 	
	{
		printf("162\n");
		flagTransform(TYPE_QUAL,type_qualifier);
	}
| RESTRICT 	
	{
		printf("163\n");
		flagTransform(TYPE_QUAL,type_qualifier);
	}
| VOLATILE 	
	{
		printf("164\n");
		flagTransform(TYPE_QUAL,type_qualifier);
	}
| ATOMIC 	
	{	
		printf("165\n");
		flagTransform(TYPE_QUAL,type_qualifier);
	}
;


function_specifier
: INLINE 	
	{
		printf("166\n");
		flagTransform(INLINE,function_specifier);
	}
| NORETURN 	
	{
		printf("167\n");
		flagTransform(NORETURN,function_specifier);
	}
;

alignment_specifier
: ALIGNAS '(' type_name ')' 
	{
		printf("168\n");
		pushFlag(ALIGNAS); 
		pushFlag(OP_PAR_BRACKET); 
		pushFlag(type_name);
		pushFlag(ED_PAR_BRACKET); 
		combine(ALIGNMENT,alignment_specifier);
	}
| ALIGNAS '(' constant_expression ')' 
	{
		printf("169\n");
		pushFlag(ALIGNAS); 
		pushFlag(OP_PAR_BRACKET); 
		pushFlag(constant_expression);
		pushFlag(ED_PAR_BRACKET); 
		combine(ALIGNMENT,alignment_specifier);
	}
;

declarator
: pointer direct_declarator 	
	{
		printf("170\n");
		pushFlag(pointer); 
		pushFlag(direct_declarator); 
		combine(INIT_DECL,declarator);
	} 
| direct_declarator 			
	{
		printf("171\n");
		flagTransform(direct_declarator,declarator);
	} 
;

/////////////////////////////////////// for typedef declaration
direct_declarator
: IDENTIFIER  	
	{
		printf("172\n");
		flagTransform(IDENTIFIER,direct_declarator);
	}
| '(' declarator ')' 															
	{
		printf("173\n");
		pushFlag(OP_PAR_BRACKET); 
		pushFlag(declarator); 
		pushFlag(ED_PAR_BRACKET); 
		combine(INIT_DECL,direct_declarator);
	}
| direct_declarator '[' ']' 													
	{
		printf("174\n");
		pushFlag(direct_declarator); 
		pushFlag(OP_SQ_BRACKET); 
		pushFlag(ED_SQ_BRACKET); 
		combine(INIT_DECL,direct_declarator);
	}
| direct_declarator '[' '*' ']' 												
	{
		printf("175\n");
		pushFlag(direct_declarator); 
		pushFlag(OP_SQ_BRACKET);
		pushFlag(ARITH_OP_1); 
		pushFlag(ED_SQ_BRACKET); 
		combine(INIT_DECL,direct_declarator);
	}
| direct_declarator '[' STATIC type_qualifier_list assignment_expression ']' 	
	{
		printf("176\n");
		pushFlag(direct_declarator); 
		pushFlag(OP_SQ_BRACKET);
		pushFlag(STATIC);
		pushFlag(type_qualifier_list);
		pushFlag(assignment_expression);
		pushFlag(ED_SQ_BRACKET); 
		combine(INIT_DECL,direct_declarator);
	}
| direct_declarator '[' STATIC assignment_expression ']' 						
	{
		printf("177\n");
		pushFlag(direct_declarator); 
		pushFlag(OP_SQ_BRACKET);
		pushFlag(STATIC);
		pushFlag(assignment_expression);
		pushFlag(ED_SQ_BRACKET); 
		combine(INIT_DECL,direct_declarator);
	}
| direct_declarator '[' type_qualifier_list '*' ']' 							
	{
		printf("178\n");
		pushFlag(direct_declarator); 
		pushFlag(OP_SQ_BRACKET);
		pushFlag(type_qualifier_list);
		pushFlag(ARITH_OP_1); 
		pushFlag(ED_SQ_BRACKET); 
		combine(INIT_DECL,direct_declarator);
	}
| direct_declarator '[' type_qualifier_list STATIC assignment_expression ']' 	
	{
		printf("179\n");
		pushFlag(direct_declarator); 
		pushFlag(OP_SQ_BRACKET);
		pushFlag(type_qualifier_list);
		pushFlag(STATIC);
		pushFlag(assignment_expression);
		pushFlag(ED_SQ_BRACKET); 
		combine(INIT_DECL,direct_declarator);
	}
| direct_declarator '[' type_qualifier_list assignment_expression ']' 			
	{
		printf("180\n");
		pushFlag(direct_declarator); 
		pushFlag(OP_SQ_BRACKET);
		pushFlag(type_qualifier_list);
		pushFlag(assignment_expression);
		pushFlag(ED_SQ_BRACKET); 
		combine(INIT_DECL,direct_declarator);
	}
| direct_declarator '[' type_qualifier_list ']' 								
	{
		printf("181\n");
		pushFlag(direct_declarator); 
		pushFlag(OP_SQ_BRACKET);
		pushFlag(type_qualifier_list);
		pushFlag(ED_SQ_BRACKET); 
		combine(INIT_DECL,direct_declarator);
	}
| direct_declarator '[' assignment_expression ']' 								
	{
		printf("182\n");
		pushFlag(direct_declarator); 
		pushFlag(OP_SQ_BRACKET);
		pushFlag(assignment_expression);
		pushFlag(ED_SQ_BRACKET); 
		combine(INIT_DECL,direct_declarator);
	}
| direct_declarator '(' parameter_type_list ')' 								
	{
		printf("183\n");
		pushFlag(direct_declarator); 
		pushFlag(OP_PAR_BRACKET); 
		pushFlag(parameter_type_list); 
		pushFlag(ED_PAR_BRACKET); 
		combine(INIT_DECL,direct_declarator);
	}
| direct_declarator '(' ')' 													
	{
		printf("184\n");
		pushFlag(direct_declarator); 
		pushFlag(OP_PAR_BRACKET); 
		pushFlag(ED_PAR_BRACKET); 
		combine(INIT_DECL,direct_declarator);
	}
| direct_declarator '(' identifier_list ')' 									
	{
		printf("185\n");
		pushFlag(direct_declarator); 
		pushFlag(OP_PAR_BRACKET); 
		pushFlag(identifier_list); 
		pushFlag(ED_PAR_BRACKET); 
		combine(INIT_DECL,direct_declarator);
	}	
;

pointer
: '*' type_qualifier_list pointer 	
	{
		printf("186\n");
		pushFlag(ARITH_OP_1); 
		pushFlag(type_qualifier_list); 
		pushFlag(pointer); 
		combine(DECL_STMT,pointer);
	}
| '*' type_qualifier_list 			
	{
		printf("187\n");
		pushFlag(ARITH_OP_1); 
		pushFlag(type_qualifier_list); 
		combine(DECL_STMT,pointer);
	}
| '*' pointer 						
	{
		printf("188\n");
		pushFlag(ARITH_OP_1); 
		pushFlag(pointer); 
		combine(DECL_STMT,pointer);
	}
| '*' 								
	{
		printf("189\n");
		flagTransform(ARITH_OP_1,pointer);
	}
;

type_qualifier_list
: type_qualifier 						
	{
		printf("190\n");
		flagTransform(type_qualifier,type_qualifier_list);
	}
| type_qualifier_list type_qualifier 	
	{
		printf("191\n");
		pushFlag(type_qualifier_list); 
		pushFlag(type_qualifier); 
		combine(type_qualifier_list,type_qualifier_list);
	}
;


parameter_type_list
: parameter_list ',' ELLIPSIS 	
	{
		printf("192\n");
		pushFlag(parameter_list); 
		pushFlag(COMM); 
		pushFlag(ELLIPSIS);
		combine(PARAMETER,parameter_type_list);
	}
| parameter_list 				
	{
		printf("193\n");
		flagTransform(parameter_list,parameter_type_list);
	}
;

parameter_list
: parameter_declaration 					
	{
		printf("194\n");
		flagTransform(parameter_declaration,parameter_list);
	}
| parameter_list ',' parameter_declaration 	
	{
		printf("195\n");
		pushFlag(parameter_list); 
		pushFlag(COMM); 
		pushFlag(parameter_declaration);
		combine(PARAMETER,parameter_list);
	}
;

parameter_declaration
: declaration_specifiers declarator 			
	{
		printf("196\n");
		pushFlag(declaration_specifiers); 
		pushFlag(declarator); 
		combine(DECL_STMT,parameter_declaration);
	}
| declaration_specifiers abstract_declarator 	
	{
		printf("197\n");
		pushFlag(declaration_specifiers); 
		pushFlag(abstract_declarator); 
		combine(DECL_STMT,parameter_declaration);
	}
| declaration_specifiers 						
	{
		printf("198\n");
		flagTransform(declaration_specifiers,parameter_declaration);											
	}
;

identifier_list
: IDENTIFIER 						
	{
		printf("199\n");
		flagTransform(IDENTIFIER,identifier_list);
	}
| identifier_list ',' IDENTIFIER 	
	{
		printf("200\n");
		pushFlag(identifier_list); 
		pushFlag(COMM);
		pushFlag(identifier_list); 
		combine(DECL_STMT,identifier_list);
	}
;

type_name
: specifier_qualifier_list abstract_declarator 	
	{
		printf("201\n");
		pushFlag(specifier_qualifier_list); 
		pushFlag(abstract_declarator);
		combine(DECL_STMT,type_name);
	}
| specifier_qualifier_list 						
	{
		printf("202\n");
		flagTransform(specifier_qualifier_list,type_name);
	}
;

abstract_declarator
: pointer direct_abstract_declarator 	
	{
		printf("203\n");
		pushFlag(pointer); 
		pushFlag(direct_abstract_declarator);
		combine(DECL_STMT,abstract_declarator);
	}
| pointer 								
	{
		printf("204\n");
		flagTransform(pointer,abstract_declarator);
	}
| direct_abstract_declarator 			
	{
		printf("205\n");
		flagTransform(direct_abstract_declarator,abstract_declarator);
	}
;

direct_abstract_declarator
: '(' abstract_declarator ')' 
	{
		printf("206\n");
		pushFlag(OP_PAR_BRACKET); 
		pushFlag(abstract_declarator);
		pushFlag(ED_PAR_BRACKET); 
		combine(DECL_STMT,direct_abstract_declarator);
	}
| '[' ']' 
	{
		printf("207\n");
		pushFlag(OP_SQ_BRACKET); 
		pushFlag(ED_SQ_BRACKET); 
		combine(DECL_STMT,direct_abstract_declarator);
	}
| '[' '*' ']' 
	{
		printf("208\n");
		pushFlag(OP_SQ_BRACKET); 
		pushFlag(ARITH_OP_1);
		pushFlag(ED_SQ_BRACKET); 
		combine(DECL_STMT,direct_abstract_declarator);
	}
| '[' STATIC type_qualifier_list assignment_expression ']' 
	{
		printf("209\n");
		pushFlag(OP_SQ_BRACKET); 
		pushFlag(STATIC);
		pushFlag(type_qualifier_list);
		pushFlag(assignment_expression);
		pushFlag(ED_SQ_BRACKET); 
		combine(DECL_STMT,direct_abstract_declarator);
	}
| '[' STATIC assignment_expression ']' 
	{
		printf("210\n");
		pushFlag(OP_SQ_BRACKET); 
		pushFlag(STATIC);
		pushFlag(assignment_expression);
		pushFlag(ED_SQ_BRACKET); 
		combine(DECL_STMT,direct_abstract_declarator);
	}
| '[' type_qualifier_list STATIC assignment_expression ']' 
	{
		printf("211\n");
		pushFlag(OP_SQ_BRACKET); 
		pushFlag(type_qualifier_list);
		pushFlag(STATIC);
		pushFlag(assignment_expression);
		pushFlag(ED_SQ_BRACKET); 
		combine(DECL_STMT,direct_abstract_declarator);
	}
| '[' type_qualifier_list assignment_expression ']' 
	{
		printf("212\n");
		pushFlag(OP_SQ_BRACKET);
		pushFlag(type_qualifier_list);
		pushFlag(assignment_expression);
		pushFlag(ED_SQ_BRACKET);
		combine(DECL_STMT,direct_abstract_declarator);
	}
| '[' type_qualifier_list ']' 
	{
		printf("213\n");
		pushFlag(OP_SQ_BRACKET);
		pushFlag(type_qualifier_list);
		pushFlag(ED_SQ_BRACKET);
		combine(DECL_STMT,direct_abstract_declarator);
	}
| '[' assignment_expression ']' 
	{
		printf("214\n");
		pushFlag(OP_SQ_BRACKET);
		pushFlag(assignment_expression);
		pushFlag(ED_SQ_BRACKET);
		combine(DECL_STMT,direct_abstract_declarator);
	}
| direct_abstract_declarator '[' ']' 
	{
		printf("215\n");
		pushFlag(direct_abstract_declarator);
		pushFlag(OP_SQ_BRACKET);
		pushFlag(ED_SQ_BRACKET);
		combine(DECL_STMT,direct_abstract_declarator);
	}
| direct_abstract_declarator '[' '*' ']' 
	{
		printf("216\n");
		pushFlag(direct_abstract_declarator);
		pushFlag(OP_SQ_BRACKET); 
		pushFlag(ARITH_OP_1);
		pushFlag(ED_SQ_BRACKET); 
		combine(DECL_STMT,direct_abstract_declarator);
	}
| direct_abstract_declarator '[' STATIC type_qualifier_list assignment_expression ']' 
	{
		printf("217\n");
		pushFlag(direct_abstract_declarator);
		pushFlag(OP_SQ_BRACKET); 
		pushFlag(STATIC);
		pushFlag(type_qualifier_list);
		pushFlag(assignment_expression);
		pushFlag(ED_SQ_BRACKET); 
		combine(DECL_STMT,direct_abstract_declarator);
	}
| direct_abstract_declarator '[' STATIC assignment_expression ']' 
	{
		printf("218\n");
		pushFlag(direct_abstract_declarator);
		pushFlag(OP_SQ_BRACKET);
		pushFlag(STATIC);
		pushFlag(assignment_expression);
		pushFlag(ED_SQ_BRACKET); 
		combine(DECL_STMT,direct_abstract_declarator);
	}
| direct_abstract_declarator '[' type_qualifier_list assignment_expression ']' 
	{
		printf("219\n");
		pushFlag(direct_abstract_declarator);
		pushFlag(OP_SQ_BRACKET); 
		pushFlag(type_qualifier_list);
		pushFlag(assignment_expression);
		pushFlag(ED_SQ_BRACKET);
		combine(DECL_STMT,direct_abstract_declarator);
	}
| direct_abstract_declarator '[' type_qualifier_list STATIC assignment_expression ']' 
	{
		printf("220\n");
		pushFlag(direct_abstract_declarator);
		pushFlag(OP_SQ_BRACKET); 
		pushFlag(type_qualifier_list);
		pushFlag(STATIC);
		pushFlag(assignment_expression);
		pushFlag(ED_SQ_BRACKET); 
		combine(DECL_STMT,direct_abstract_declarator);
	}
| direct_abstract_declarator '[' type_qualifier_list ']' 
	{
		printf("221\n");
		pushFlag(direct_abstract_declarator);
		pushFlag(OP_SQ_BRACKET); 
		pushFlag(type_qualifier_list);
		pushFlag(ED_SQ_BRACKET); 
		combine(DECL_STMT,direct_abstract_declarator);
	}
| direct_abstract_declarator '[' assignment_expression ']' 
	{
		printf("222\n");
		pushFlag(direct_abstract_declarator);
		pushFlag(OP_SQ_BRACKET); 
		pushFlag(assignment_expression);
		pushFlag(ED_SQ_BRACKET); 
		combine(DECL_STMT,direct_abstract_declarator);
	}
| '(' ')' 
	{
		printf("223\n");
		pushFlag(OP_PAR_BRACKET); 
		pushFlag(ED_PAR_BRACKET); 
		combine(DECL_STMT,direct_abstract_declarator);
	}
| '(' parameter_type_list ')' 
	{
		printf("224\n");
		pushFlag(OP_PAR_BRACKET); 
		pushFlag(parameter_type_list);
		pushFlag(ED_PAR_BRACKET); 
		combine(DECL_STMT,direct_abstract_declarator);
	}
| direct_abstract_declarator '(' ')' 
	{
		printf("225\n");
		pushFlag(direct_abstract_declarator);
		pushFlag(OP_PAR_BRACKET); 
		pushFlag(ED_PAR_BRACKET); 
		combine(DECL_STMT,direct_abstract_declarator);
	}
| direct_abstract_declarator '(' parameter_type_list ')' 
	{
		printf("226\n");
		pushFlag(direct_abstract_declarator);
		pushFlag(OP_PAR_BRACKET); 
		pushFlag(parameter_type_list);
		pushFlag(ED_PAR_BRACKET);
		combine(DECL_STMT,direct_abstract_declarator);
	}
;

initializer
: '{' initializer_list '}' 		
	{
		printf("227\n");
		pushFlag(OP_CUR_BRACKET);
		pushFlag(initializer_list);
		pushFlag(ED_CUR_BRACKET);
		combine(INITIALIZER,initializer);
	}
| '{' initializer_list ',' '}' 	
	{
		printf("228\n");
		pushFlag(OP_CUR_BRACKET);
		pushFlag(initializer_list);
		pushFlag(COMM);
		pushFlag(ED_CUR_BRACKET);
		combine(INITIALIZER,initializer);
	}
| assignment_expression 		
	{
		printf("229\n");
		flagTransform(assignment_expression,initializer);
	}
;

initializer_list
: designation initializer 						
	{
		printf("230\n");
		pushFlag(designation);
		pushFlag(initializer);
		combine(INITIALIZER,initializer_list);	
	}
| initializer 									
	{
		printf("231\n");
		flagTransform(initializer,initializer_list);
	}
| initializer_list ',' designation initializer 	
	{
		printf("232\n");
		pushFlag(initializer_list);
		pushFlag(COMM);
		pushFlag(designation);
		pushFlag(initializer);
		combine(INITIALIZER,initializer_list);	
	}

| initializer_list ',' initializer 				
	{
		printf("233\n");
		pushFlag(initializer_list);
		pushFlag(COMM);
		pushFlag(initializer);
		combine(INITIALIZER,initializer_list);	
	}
;

designation
: designator_list '=' 
	{
		printf("234\n");
		pushFlag(designator_list);
		pushFlag(ASS_OP);
		combine(DESIGNATOR,designation);
	}
;

designator_list
: designator 					
	{
		printf("235\n");
		flagTransform(DESIGNATOR,designator_list);
	}
| designator_list designator 	
	{
		printf("236\n");
		pushFlag(designator_list);
		pushFlag(designator);
		combine(DESIGNATOR,designator_list);
	}
;

designator
: '[' constant_expression ']' 
	{
		printf("237\n");
		pushFlag(OP_SQ_BRACKET);
		pushFlag(constant_expression);
		pushFlag(ED_SQ_BRACKET);
		combine(DESIGNATOR,designator);	
	}
| '.' IDENTIFIER 
	{
		printf("238\n");
		pushFlag(FULL_STOP);
		pushFlag(IDENTIFIER);
		combine(DESIGNATOR,designator);	
	}
;

static_assert_declaration
: STATIC_ASSERT '(' constant_expression ',' STRING_LITERAL ')' ';' 
	{
		printf("239\n");
		pushFlag(STATIC_ASSERT);
		pushFlag(OP_PAR_BRACKET);
		pushFlag(constant_expression);
		pushFlag(COMM);
		pushFlag(STRING_LITERAL);
		pushFlag(ED_PAR_BRACKET);
		pushFlag(SEMICOMM);
		combine(STA_ASS_DECL,static_assert_declaration);	
	}
;

statement
: labeled_statement 	
	{
		printf("240\n");
		flagTransform(labeled_statement,statement);
	} 
| compound_statement 	
	{
		printf("241\n");
		flagTransform(compound_statement,statement);
	}
| expression_statement 	
	{
		printf("242\n");
		flagTransform(expression_statement,statement);
	}
| selection_statement 	
	{
		printf("243\n");
		flagTransform(selection_statement,statement);
	}
| iteration_statement 	
	{
		printf("244\n");
		flagTransform(iteration_statement,statement);
	}
| jump_statement 		
	{
		printf("245\n");
		flagTransform(jump_statement,statement);
	}
;

labeled_statement
: IDENTIFIER ':' statement 
	{
		printf("246\n");
		pushFlag(IDENTIFIER); 
		pushFlag(COL); 
		pushFlag(statement); 
		combine(LABEL_STMT,labeled_statement);
	} // Label statement
| CASE constant_expression ':' statement 
	{
		printf("247\n");
		pushFlag(CASE); 
		pushFlag(constant_expression); 
		pushFlag(COL); 
		pushFlag(statement); 
		combine(LABEL_STMT,labeled_statement);
	} // Label statement
| DEFAULT ':' statement 
	{
		printf("248\n");
		pushFlag(DEFAULT); 
		pushFlag(COL); 
		pushFlag(statement); 
		combine(LABEL_STMT,labeled_statement);
	} // Label statement
;

compound_statement
: '{' '}' 
	{
		printf("249\n");
		pushFlag(OP_CUR_BRACKET); 
		pushFlag(ED_CUR_BRACKET); 
		combine(COMP_STMT,compound_statement);
	}
| '{'  block_item '}' 
	{
		printf("250 eiei\n");
		pushFlag(OP_CUR_BRACKET); 
		pushFlag(block_item);
		pushFlag(ED_CUR_BRACKET); 
		combine(COMP_STMT,compound_statement);
	}
| '{'  block_item_list '}' 
	{
		printf("250\n");
		pushFlag(OP_CUR_BRACKET); 
		pushFlag(block_item_list);
		pushFlag(ED_CUR_BRACKET); 
		combine(COMP_STMT,compound_statement);
	}
;

block_item_list /////////////////////// ;
: block_item 					
	{
		printf("251\n");
		flagTransform(block_item,block_item_list);
	}
| block_item_list block_item 	
	{
		printf("252\n");
		pushFlag(block_item_list);
		pushFlag(block_item);
		combine(block_item_list,block_item_list); // EDIT ACSE FLAG!!
	}
;

block_item
: declaration 	
	{
		printf("253\n");
		flagTransform(declaration,block_item);
	}
| statement 	
	{	
		printf("254\n");
		flagTransform(statement,block_item);
	}
;

expression_statement
: expression ';' 
	{
		printf("256\n");
		pushFlag(expression);
		pushFlag(SEMICOMM);
		combine(EXPR_STMT,expression_statement); // EDIT ACSE FLAG!!
	}
;

/*selection_statement 
: ifelse_statement {printf("257\n");}
| switch_statement {printf("258\n");}

switch_statement
: SWITCH '(' expression ')' '{' statement '}' {printf("259\n");}

ifelse_statement
: IF '(' expression ')' single_or_block  {printf("260\n");}
| ifelse_statement ELSE IF '(' expression ')' single_or_block {printf("261\n");}
| ifelse_statement ELSE single_or_block {printf("262\n");}

single_or_block
: expression_statement {printf("263\n");} // single
| compound_statement  {printf("264\n");}//block
;
*/

//iteration_statement
//: while_statement {printf("288\n");} 
//| dowhile_statement {printf("289\n");} 
//| for_statement {printf("290\n");} 
//;

//while_statement
//: WHILE '(' expression ')' single_or_block {printf("291\n");} 
//;

//dowhile_statement
//: DO compound_statement WHILE '(' expression ')' ';' {printf("292\n");} 
//;

//for_statement
//: FOR '(' expression_statement expression_statement ')' statement {printf("293\n");} 
//| FOR '(' expression_statement expression_statement expression ')' statement {printf("294\n");} 
//| FOR '(' declaration expression_statement ')' statement {printf("295\n");} 
//| FOR '(' declaration expression_statement expression ')' statement {printf("296\n");} 


selection_statement
: IF '(' expression ')' statement  					
	{
		printf("300\n");
		pushFlag(IF); 
		pushFlag(OP_PAR_BRACKET);
		pushFlag(expression); 
		pushFlag(ED_PAR_BRACKET);
		pushFlag(statement);
		combine(IF_STMT,selection_statement); 
	} // IF statement
| IF '(' expression ')' statement ELSE statement 	
	{
		printf("301\n");
		pushFlag(IF); 
		pushFlag(OP_PAR_BRACKET);
		pushFlag(expression); 
		pushFlag(ED_PAR_BRACKET);
		pushFlag(statement);
		pushFlag(ELSE);
		pushFlag(statement);
		combine(IF_STMT,selection_statement); 
	} // IF statement
| SWITCH '(' expression ')' statement  				
	{
		printf("303\n");
		pushFlag(SWITCH); 
		pushFlag(OP_PAR_BRACKET);
		pushFlag(expression); 
		pushFlag(ED_PAR_BRACKET);
		pushFlag(statement);
		combine(SWITCH_STMT,selection_statement); 
	} // switch-statement
;

iteration_statement
: WHILE '(' expression ')' statement	
	{
		printf("265\n");
		pushFlag(WHILE); 
		pushFlag(OP_PAR_BRACKET);
		pushFlag(expression); 
		pushFlag(ED_PAR_BRACKET);
		pushFlag(statement);
		combine(WHILE_STMT,iteration_statement); 
	} // While statement
| DO statement WHILE '(' expression ')' ';' 									
	{
		printf("266\n");
		pushFlag(DO); 
		pushFlag(statement);
		pushFlag(WHILE); 
		pushFlag(OP_PAR_BRACKET);
		pushFlag(expression);
		pushFlag(ED_PAR_BRACKET);
		pushFlag(SEMICOMM);
		combine(DO_WHILE_STMT,iteration_statement); 
	}// do while statement
| FOR '(' expression ';' expression ';' ')' statement 
	{
		printf("267\n");
		pushFlag(FOR); 
		pushFlag(OP_PAR_BRACKET);
		pushFlag(expression);
		pushFlag(SEMICOMM);
		pushFlag(expression);
		pushFlag(SEMICOMM);
		pushFlag(ED_PAR_BRACKET);
		pushFlag(statement);
		combine(FOR_STMT,iteration_statement); 
	} // for statement
| FOR '(' expression ';' expression ';' expression ')' statement 				
	{
		printf("268\n");
		pushFlag(FOR); 
		pushFlag(OP_PAR_BRACKET);
		pushFlag(expression);
		pushFlag(SEMICOMM);
		pushFlag(expression);
		pushFlag(SEMICOMM);
		pushFlag(expression);
		pushFlag(ED_PAR_BRACKET);
		pushFlag(statement);
		combine(FOR_STMT,iteration_statement); 
	} // for statement
| FOR '(' declaration_specifiers ';' expression ';' ')' statement 				
	{
		printf("269\n");
		pushFlag(FOR); 
		pushFlag(OP_PAR_BRACKET);
		pushFlag(declaration_specifiers);
		pushFlag(SEMICOMM);
		pushFlag(expression);
		pushFlag(SEMICOMM);
		pushFlag(ED_PAR_BRACKET);
		pushFlag(statement);
		combine(FOR_STMT,iteration_statement);
	} // for statement
| FOR '(' declaration_specifiers ';' expression ';' expression ')' statement 	
	{
		printf("270\n");
		pushFlag(FOR); 
		pushFlag(OP_PAR_BRACKET);
		pushFlag(declaration_specifiers);
		pushFlag(SEMICOMM);
		pushFlag(expression);
		pushFlag(SEMICOMM);
		pushFlag(expression);
		pushFlag(ED_PAR_BRACKET);
		pushFlag(statement);
		combine(FOR_STMT,iteration_statement);
	} // for statement
;

jump_statement
: GOTO IDENTIFIER ';' 	
	{
		printf("271\n");
		pushFlag(GOTO);
		pushFlag(IDENTIFIER);
		pushFlag(SEMICOMM);
		combine(JUMP_STMT,jump_statement);
	}
| CONTINUE ';' 			
	{
		printf("272\n");
		pushFlag(CONTINUE);
		pushFlag(SEMICOMM);
		combine(JUMP_STMT,jump_statement);
	}
| BREAK ';' 			
	{
		printf("273\n");
		pushFlag(BREAK);
		pushFlag(SEMICOMM);
		combine(JUMP_STMT,jump_statement);
	}
| RETURN ';' 			
	{
		printf("274\n");
		pushFlag(RETURN);
		pushFlag(SEMICOMM);
		combine(JUMP_STMT,jump_statement);
	}
| RETURN expression ';' 
	{
		printf("275\n");
		pushFlag(RETURN);
		pushFlag(expression);
		pushFlag(SEMICOMM);
		combine(JUMP_STMT,jump_statement);
	}
;

translation_unit
: external_declaration  				
	{	
		printf("276\n");
		flagTransform(external_declaration,translation_unit);
	}
| translation_unit external_declaration 
	{	
		printf("277\n");
		pushFlag(translation_unit); 
		pushFlag(external_declaration);
		combine(translation_unit,translation_unit);
	}
;

external_declaration
: function_definition  	
	{	
		printf("278\n");
		flagTransform(function_definition,external_declaration);
	}
| declaration  			
	{	
		printf("279\n");
		flagTransform(declaration,external_declaration);
	}
| include 				
	{
		printf("300 \n");
		flagTransform(include,external_declaration);
	}
| define_declaration  	
	{
		printf("283\n");
		flagTransform(define_declaration,external_declaration);
	}
| typedef_specifier
	{
		printf("283.5\n");
		flagTransform(typedef_specifier,external_declaration);
	}
;

function_definition
: declaration_specifiers declarator declaration_list compound_statement  	
	{
		printf("284\n");
		pushFlag(declaration_specifiers); 
		pushFlag(declarator);
		pushFlag(declaration_list); 
		pushFlag(compound_statement);
		combine(FUNC_DEF_STMT,function_definition); 
	}
| declaration_specifiers declarator compound_statement 						
	{
		printf("285\n");
		pushFlag(declaration_specifiers); 
		pushFlag(declarator);
		pushFlag(compound_statement);
		combine(FUNC_DEF_STMT,function_definition);
	}
// declaration_specifiers= int char
// declarator = IDENTIFIER '(' parameter list')'
// compoud statement= '{' what are declared inside the block such as variable declaration'}'
;

declaration_list
: declaration  					
	{
		printf("286\n");
		flagTransform(declaration,declaration_list);
	}
| declaration_list declaration 	
	{
		printf("287\n"); 
		pushFlag(declaration_list); 
		pushFlag(declaration); 
		combine(DECL_STMT,declaration_list); 
	}
;


%%


void yyerror(const char *s)
{
    fflush(stdout);
    fprintf(stderr, "*** %s\n", s);
}
int parse(FILE* fileInput)
{
	int a;
    yyin= fileInput;
    if(feof(yyin)==0)
    {
        a = yyparse();// To read file, while() and yyparse() is necessary because we need to read until the content is run out
    }
    return a;
}