#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "acse.h"
#include "y.tab.h"

int enforcement()
{
	RULE_T *currentRule = getRuleHead();
	int lineError = 0;
	int attrCount = 0;
	int isName = 0;
	int i = 0;

	while(currentRule != NULL)
	{
		i = 0;
		isName = 0;
		ATTR_T *currentAttr = currentRule->attrHead;
		if(currentRule->type == NAME)
		{
			isName = 1;
			while(currentAttr != NULL)
			{
				if(strcmp("EXCEPTIONS", currentAttr->name) == 0)
				{
					currentAttr->check(getStackHeadData(), currentAttr->value, currentRule);
				}
				attrCount++;
				currentAttr = currentAttr->next;
			}
		}

		currentAttr = currentRule->attrHead;
		if(isName)
		{
			for(i = 0; (i < attrCount) && (currentAttr != NULL); i++)
			{
				currentAttr->check(getStackHeadData(), currentAttr->value, currentRule);
				currentAttr = currentAttr->next;
			}
		}
		else
		{
			while(currentAttr != NULL)
			{
				currentAttr->check(getStackHeadData(), currentAttr->value, currentRule);
				currentAttr = currentAttr->next;
			}
		}

		currentRule = currentRule->next;
	}
}

NAME_LIST_T *nameListDestroy(NAME_LIST_T *list)
{
	LIST_T *destroy = list->listHead;

	if(list == NULL)
		return NULL;
	while(destroy != NULL)
	{
		LIST_T *next = destroy->next;
		free(destroy);
		destroy = next;
	}
	free(list);
	list = NULL;
	return list;
}

NAME_LIST_T *initNameList(NAME_LIST_T *list)
{
	if(list != NULL)
	{
		list = nameListDestroy(list);
	}
	
	NAME_LIST_T *newList = (NAME_LIST_T*) calloc(1,sizeof(NAME_LIST_T));
	return newList;
}

int insertNameList(NAME_LIST_T *list, char *data)
{
	if(list == NULL)
        return -1;
    else
    {
        LIST_T *tail = list->listTail;
        LIST_T *new = (LIST_T*) calloc(1, sizeof(LIST_T));
        if(new != NULL)
        {
            new->string = mystrdup(data);
            if(new->string != NULL)
            {
            	// printf("I : %s\n", new->string);
                if(tail == NULL)
                {
                    list->listHead = new;
                }
                else
                {
                    tail->next = new;
                }
                list->listTail = new;
                list->listCurrent = new;
                return 1;
            }
        }
        return -1;
    }
}